from io import BytesIO
import os, copy
import logging
import json
import datetime
import argparse
import tarfile

from functools import wraps
import zipfile
from flask import Flask, Response, request, send_file, send_from_directory, jsonify
from flask_cors import CORS
from flask_swagger_ui import get_swaggerui_blueprint
from flask_jwt_extended import jwt_required, create_access_token, JWTManager, current_user, verify_jwt_in_request
from flask_jwt_extended import get_jwt, get_jwt_identity, set_access_cookies, unset_jwt_cookies, get_csrf_token
from metaboserv_database import MetaboservDatabase
from metaboserv_es_data import FORBIDDEN_IDS
from metaboserv_input import dictify_vis_ranges, get_metadata_from_file, get_phenotypes_from_file, read_config, hmdb_extract_file, ResultObject, parse_metadata_file, parse_phenotype_file, combine_dicts
from metaboserv_input import get_conc_data_from_file
from metaboserv_permissions import dict2obj
from werkzeug.utils import secure_filename

from metaboserv_plotter import MetaboservPlotter


# Configuration is read from here
CONFIG_PATH = 'metaboserv_conf.yml'
DOCKER_CONFIG_PATH = '/app/metaboserv_conf_docker.yml'

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Control the MetaboSERV backend service.')
    parser.add_argument("--cache", help="Create a concentration data cache on startup", action="store_true")
    parser.add_argument("--eslog", help="Whether or not to show Elasticsearch logs", action="store_true")
    parser.add_argument("--docker", help="Set this flag if the backend is running in a docker container.", action="store_true")
    parser_args = parser.parse_args()
    
    DOCKER_BOOL = parser_args.docker
    config = read_config(DOCKER_CONFIG_PATH) if DOCKER_BOOL else read_config(CONFIG_PATH)

    flask_config = config['flask']
    elasticsearch_config = config['elasticsearch']
    mariadb_config = config['mariadb']
    general_config = config['backend']
    smtp_config = config['smtp']
    raw_config = config['raw']

    api_path = flask_config['app_path']
    api_host = flask_config['app_host']
    api_port = flask_config['app_port']
    elasticsearch_host = f"http://{elasticsearch_config['path']}:{elasticsearch_config['port']}"
    mariadb_host = f"{mariadb_config['path']}:{mariadb_config['port']}"

    data_folder = general_config['data_folder'] # For HMDB files etc.
    source_folder = general_config['source_location'] # For uploaded raw and annotated data
    
    # Create Flask Object
    app = Flask(__name__, template_folder="templates", static_folder='static')
    CORS(app)
    app.config['UPLOAD_FOLDER'] = general_config['upload_folder'] # Temp storage for conc. file parsing etc
    logging.basicConfig(level=logging.INFO)

    if not parser_args.eslog:
        tracer = logging.getLogger('elastic_transport')
        tracer.setLevel(logging.WARNING)

    # Create database connection
    metaboserv = MetaboservDatabase(elasticsearch_config, elasticsearch_host, mariadb_config, mariadb_host,
        DOCKER_BOOL, general_config['admin_password'], source_folder, app.config['UPLOAD_FOLDER'], smtp_config)
    metaboserv_plotter = MetaboservPlotter()

    # Initialize JWT Manager
    jwt = JWTManager(app)
    app.config["JWT_SECRET_KEY"] = flask_config['jwt_key']
    app.config["JWT_TOKEN_LOCATION"] = ["headers", "cookies"]
    app.config["JWT_COOKIE_SECURE"] = flask_config['jwt_https'] # This should be True for production!
    app.config["JWT_ACCESS_TOKEN_EXPIRES"] = datetime.timedelta(hours=int(flask_config['jwt_expiration']))
    app.config["JWT_COOKIE_CSRF_PROTECT"] = True
    app.config["JWT_CSRF_IN_COOKIES"] = True

    # Set up SwaggerUI
    SWAGGER_URL = f'{api_path}doc'
    API_URL = '/static/openapi.json'
    SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
        SWAGGER_URL,
        API_URL,
        config={
            'app_name': flask_config['app_name']
        }
    )
    app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)

    if parser_args.cache:
        pass
        #metaboserv.createInitialCache()

    ################### Auth Decorators + Response functions ###################

    def create_response(payload_data: ResultObject) -> Response:
        """
        Creates a Flask Response object containing the payload of a function.

        :param payload_data: The tuple in question (data, status, content type)
        :returns Response object
        """
        return Response(json.dumps({'body': payload_data.body, 'msg': str(payload_data.msg), 'status': payload_data.status}), status=int(payload_data.status), content_type='application/json')
    
    def create_error_response(e) -> Response:
        """
        Creates an error response that is sent whenever an API error occurs.

        :param e: Exception message
        :returns Response object
        """
        logging.error(f"Error message: {e}")
        return jsonify(msg="An API error occurred. We are sorry for the inconvenience."), 500


    def admin_required():
        """
        Creates a decorator that specifies necessary admin rights for a protected route.
        Used for most of the "administrative" endpoints that revolve around managing the database.
        :returns Unauthorized response (401) or decorated function
        """
        def wrapper(fn):
            @wraps(fn)
            def decorator(*args, **kwargs):
                verify_jwt_in_request()
                claims = get_jwt()
                if claims["is_admin"]:
                    return fn(*args, **kwargs)
                else:
                    return jsonify(msg="This action requires administrator permissions.\n"), 401
            return decorator
        return wrapper

    def require_params(params, parameter_type="GET"):
        """
        Denotes that a function expects a set of parameters.
        Only applies to mandatory parameters. Optional parameters are not affected.
        :param params -- List of expected parameter names (in any order)
        :param parameter_type -- Type of parameters (GET, POST, anything else is JSON)
        :returns Bad request response (400) or decorated function
        """
        def decorator(fn):
            def param_wrapper(*args, **kwargs):
                try:
                    parameter_dict = request.args if parameter_type == "GET" else (request.form if parameter_type == "POST" else request.json)
                    for required_parameter in params:
                        if required_parameter not in parameter_dict.keys():
                            return jsonify(msg=f"Missing at least one required parameter: {required_parameter}"), 400
                    return fn(*args, **kwargs)
                except (NameError, IndexError, KeyError):
                    return jsonify(msg="Oops. You encountered a bug! We are sorry for the inconvenience."), 500
            param_wrapper.__name__ = fn.__name__
            return param_wrapper
        return decorator
    
    def allowed_file(filename) -> bool:
        """
        Returns False if a filename does not fit the accepted scheme or has a wrong extension.
        Currently just returns True for the public instance, feel free to adapt for self-hosted instances.
        """
        return True
        #return '.' in filename and filename.rsplit('.', 1)[1].lower() in app.config['UPLOAD_EXTENSIONS']

    ################### Elasticsearch Database management ###################

    @app.route(f"{api_path}ESIndexCreate", methods=["POST"])
    @require_params(["index"], parameter_type="POST")
    @admin_required()
    def es_index_create():
        """
        Creates an Elasticsearch index.
        """
        index = request.form.get('index')
        try:
            return create_response(metaboserv.elasticsearch_create_index(index))
        except Exception as e:
            return create_error_response(e)

    @app.route(f"{api_path}ESIndexDelete", methods=["POST"])
    @require_params(["index"], parameter_type="POST")
    @admin_required()
    def es_index_delete():
        """
        Deletes an Elasticsearch index.
        """
        index = request.form.get('index')
        try:
            return create_response(metaboserv.elasticsearch_delete_index(index))
        except Exception as e:
            return create_error_response(e)

    @app.route(f"{api_path}ESIndexGet", methods=["GET"])
    @require_params(["index"])
    @admin_required()
    def es_index_get():
        """
        Retrieves information about an Elasticsearch index.
        """
        index = request.form.get('index')
        try:
            return create_response(metaboserv.elasticsearch_get_index_information(index))
        except Exception as e:
            return create_error_response(e)

    @app.route(f"{api_path}ESRecordGet", methods=["GET"])
    @require_params(["index, record_id"])
    @admin_required()
    def es_record_get():
        """
        Retrieves information about any Elasticsearch record.
        """
        index = request.form.get('index')
        record = request.form.get('record_id')
        try:
            return create_response(metaboserv.elasticsearch_get_any_record(index, record))
        except Exception as e:
            return create_error_response(e)

    @app.route(f"{api_path}ESRecordCheckExists", methods=["GET"])
    @require_params(["index", "record_id"])
    @admin_required()
    def es_record_check_exists():
        """
        Checks whether a record exists in any given Elasticsearch index.
        """
        index = request.form.get('index')
        record = request.form.get('record_id')
        try:
            result = metaboserv.record_exists_in_es(index, record)
            return create_response(ResultObject(msg="true" if result else "false", status=200 if result else 404))
        except Exception as e:
            return create_error_response(e)

    @app.route(f"{api_path}ESRecordDelete", methods=["POST"])
    @require_params(["index", "record_id"], parameter_type="POST")
    @admin_required()
    def es_record_delete():
        """
        Deletes a particular Elasticsearch record.
        """
        index = request.form.get('index')
        record = request.form.get('record_id')
        try:
            return create_response(metaboserv.elasticsearch_record_delete(index, record))
        except Exception as e:
            return create_error_response(e)

    @app.route(f"{api_path}ESRecordDeleteByIndex", methods=["POST"])
    @require_params(["index"], parameter_type="POST")
    @admin_required()
    def es_record_delete_by_index():
        """
        Deletes all records in a particular Elasticsearch index.
        """
        index = request.form.get('index')
        try:
            return create_response(metaboserv.elasticsearch_record_delete_by_index(index))
        except Exception as e:
            return create_error_response(e)
        
    @app.route(f"{api_path}ESDatabaseInitialize", methods=["POST"])
    @admin_required()
    def es_database_initialize():
        """
        Initializes the Elasticsearch database, creating the according indices.
        """
        try:
            return create_response(metaboserv.elasticsearch_initialize_db(general_config['admin_password']))
        except Exception as e:
            return create_error_response(e)

    @app.route(f"{api_path}ESDatabaseReset", methods=["POST"])
    @admin_required()
    def es_database_reset():
        """
        Resets the database, removing all records but maintaining indices and their schema.
        """
        try:
            return create_response(metaboserv.elasticsearch_reset_db(general_config['admin_password']))
        except Exception as e:
            return create_error_response(e)

    @app.route(f"{api_path}ESDatabaseDestroy", methods=["POST"])
    @admin_required()
    def es_database_destroy():
        """
        Completely clears the Elasticsearch database.
        """
        try:
            return create_response(metaboserv.elasticsearch_destroy_db())
        except Exception as e:
            return create_error_response(e)

    ################### MariaDB Database management ###################

    @app.route(f"{api_path}MariaDBDropTable", methods=["POST"])
    @require_params(["table_id"], parameter_type="POST")
    @admin_required()
    def mariadb_drop_table():
        """
        Drops a particular table from the MariaDB database.
        """
        try:
            table_id = request.form.get('table_id')
            return create_response(metaboserv.mariadb_drop_table(table_id))
        except Exception as e:
            return create_error_response(e)
        
    @app.route(f"{api_path}MariaDBTruncateTable", methods=["POST"])
    @require_params(["table_id"], parameter_type="POST")
    @admin_required()
    def mariadb_truncate_table():
        """
        Drops a particular table from the MariaDB database.
        """
        try:
            table_id = request.form.get('table_id')
            return create_response(metaboserv.mariadb_truncate_table(table_id))
        except Exception as e:
            return create_error_response(e)
        
    @app.route(f"{api_path}MariaDBReset", methods=["POST"])
    @admin_required()
    def mariadb_reset():
        """
        Resets the MariaDB instance.
        """
        try:
            return create_response(metaboserv.mariadb_reset_db())
        except Exception as e:
            return create_error_response(e)
        
    ################### Other management related endpoints ###################

    @app.route(f"{api_path}IsHMDBEnforced", methods=["GET"])
    def is_hmdb_enforced():
        """
        Returns whether HMDB is enforced for this instance or not.
        """
        return create_response(ResultObject(body=general_config["enforce_hmdb"]))
    
    ################### Study-related endpoints ###################

    @app.route(f"{api_path}StudyCreate", methods=["POST"])
    @require_params(["study_id", "study_name"], parameter_type="POST")
    @jwt_required()
    def study_create():
        try:
            study_id = request.form.get('study_id')
            study_name = request.form.get('study_name')
            study_type = request.form.get('study_type', "unknown")
            study_specimen = request.form.get('study_specimen', "unknown")
            study_date = request.form.get('study_date', "unknown")
            study_author = request.form.get('study_author', "unknown")
            study_visibility = request.form.get('study_visibility', 'private')
            
            concentration_file = request.files.get('concentration_file', None)
            metadata_file = request.files.get('metadata_file', None)
            phenotype_file = request.files.get('phenotype_file', None)

            conc_transpose = True if request.form.get('conc_transpose', "false") == "true" else False
            pheno_transpose = True if request.form.get('pheno_transpose', "false") == "true" else False

            # Concentration file parsing
            concentration_data = {}
            concentration_metadata = {}
            unit_map = {}
            compounds = []
            if not os.path.isdir(os.path.join(source_folder, study_id)):
                os.mkdir(os.path.join(source_folder, study_id))
            if concentration_file:
                if not concentration_file.filename or concentration_file.filename == '':
                    return jsonify(msg="Received an empty file!", status=400), 400
                if not allowed_file(concentration_file.filename):
                    return jsonify(msg="Received an illegal filename!", status=400), 400
                conc_file_data = get_conc_data_from_file(concentration_file, os.path.join(source_folder, study_id), conc_transpose, general_config['enforce_hmdb'])
                if conc_file_data.status != 200:
                    return create_response(conc_file_data)
                concentration_data = conc_file_data.body[0]
                compounds = conc_file_data.body[1]
                concentration_metadata = conc_file_data.body[2]
                compound_name_map = conc_file_data.body[3]
                unit_map = conc_file_data.body[4]

                if compound_name_map and len(compound_name_map.keys()) > 0:
                    metaboserv.compound_add_namemap(compound_name_map, study_id)


            # Metadata file parsing
            metadata = {}
            if metadata_file:
                metadata_results = get_metadata_from_file(metadata_file, os.path.join(source_folder, study_id))
                if metadata_results.status == 200:
                    metadata = metadata_results.getBody()

            # Metadata from frontend will overwrite that specified in the file:
            study_metadata = request.form.get('study_metadata', None)
            if study_metadata:
                metadata_from_frontend = json.loads(study_metadata)
                for k, v in metadata_from_frontend.items():
                    metadata[k] = v

            # Phenotype file parsing
            phenotypes = {}
            exit_pt = True
            if phenotype_file:
                phenotypes_results = get_phenotypes_from_file(phenotype_file, os.path.join(source_folder, study_id), pheno_transpose)
                if phenotypes_results.status == 200:
                    phenotypes = phenotypes_results.getBody()

            study_type = request.form.get('study_type', None)
            study_specimen = request.form.get('study_specimen', None)
            return create_response(metaboserv.study_create(
                study_id, study_type, study_specimen, study_name, study_date, concentration_data, compounds, 
                concentration_metadata, study_author, study_visibility, metadata=metadata, phenotypes=phenotypes, units=unit_map, auth_user=current_user
            ))
        except Exception as e:
            metaboserv.study_delete(study_id, current_user)
            return create_error_response(e)
        
    @app.route(f"{api_path}AddConcentrationDataFromFile", methods=["POST"])
    @jwt_required()
    @require_params(["study_id"], parameter_type="POST")
    def add_conc_data_from_file() -> Response:
        try:
            study_id = request.form.get('study_id')
            conc_file = request.form.get('filename')
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)
            transpose = True if request.form.get('transpose', "false") == "true" else False
            if not conc_file:
                return create_response(ResultObject(msg="No filename received!", status=400))
            if not auth_token and not current_user:
                return create_response(ResultObject(msg="You lack the permissions to add data to this study!", status=401))
            return create_response(
                metaboserv.add_concentration_data_from_file(conc_file, study_id, os.path.join(source_folder, study_id), transpose, auth_user=current_user, auth_token=auth_token)
            )
        except Exception as e:
            return create_error_response(e)
        
    @app.route(f"{api_path}AddMetadataFromFile", methods=["POST"])
    @jwt_required()
    @require_params(["study_id"], parameter_type="POST")
    def add_metadata_from_file() -> Response:
        try:
            study_id = request.form.get('study_id')
            metadata_file = request.form.get('filename', None)
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)
            if not metadata_file:
                return create_response(ResultObject(msg="No file received!", status=400))
            if not auth_token and not current_user:
                return create_response(ResultObject(msg="You lack the permissions to add data to this study!", status=401))
            return create_response(
                metaboserv.add_metadata_from_file(metadata_file, study_id, os.path.join(source_folder, study_id), auth_user=current_user, auth_token=auth_token)
            )
        except Exception as e:
            return create_error_response(e)
        
    @app.route(f"{api_path}AddPhenotypeDataFromFile", methods=["POST"])
    @jwt_required()
    @require_params(["study_id"], parameter_type="POST")
    def add_pheno_data_from_file() -> Response:
        try:
            study_id = request.form.get('study_id')
            phenotype_file = request.form.get('filename', None)
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)
            transpose = True if request.form.get('transpose', "false") == "true" else False
            if not phenotype_file:
                return create_response(ResultObject(msg="No file received!", status=400))
            if not auth_token and not current_user:
                return create_response(ResultObject(msg="You lack the permissions to add data to this study!", status=401))
            return create_response(
                metaboserv.add_phenotype_data_from_file(phenotype_file, study_id, os.path.join(source_folder, study_id), transpose, auth_user=current_user, auth_token=auth_token)
            )
        except Exception as e:
            return create_error_response(e)
        
        

    @app.route(f"{api_path}StudyDelete", methods=["POST"])
    @jwt_required()
    @require_params(["study_id"], parameter_type="POST")
    def study_delete():
        try:
            study_id = request.form.get('study_id')
            return create_response(metaboserv.study_delete(study_id, current_user))
        except Exception as e:
            return create_error_response(e)

    @app.route(f"{api_path}StudyUpdateMetadata", methods=["POST"])
    @jwt_required(optional=True)
    @require_params(["study_id", "study_metadata"], parameter_type="POST")
    def study_update_metadata():
        try:
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)
            study_id = request.form.get('study_id')
            study_metadata = request.form.get('study_metadata').split('$')
            study_dict = {}
            for key_value_pair in study_metadata:
                key_value_split = key_value_pair.split('=')
                study_dict[key_value_split[0]] = str(key_value_split[1])
            return create_response(metaboserv.study_update_metadata(study_id, study_dict, current_user, auth_token))
        except Exception as e:
            return create_error_response(e)
        
    @app.route(f"{api_path}StudyUpdate", methods=["POST"])
    @jwt_required(optional=True)
    @require_params(["study_id", "data"], parameter_type="POST")
    def study_update():
        try:
            study_id = request.form.get('study_id')
            data = json.loads(request.form.get('data'))
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)
            return create_response(metaboserv.study_update(study_id, data, current_user, auth_token))
        except Exception as e:
            return create_error_response(e)


    @app.route(f"{api_path}StudyListAll", methods=["GET"])
    @jwt_required(optional=True)
    def study_list_all():
        try:
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)
            return create_response(metaboserv.study_list_all(auth_user=current_user, auth_token=auth_token))
        except Exception as e:
            return create_error_response(e)

    @app.route(f"{api_path}StudyListPrivate", methods=["GET"])
    @jwt_required(optional=True)
    def study_list_private():
        try:
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)
            return create_response(metaboserv.study_list_private(auth_user=current_user, auth_token=auth_token))
        except Exception as e:
            return create_error_response(e)

    @app.route(f"{api_path}StudyGet", methods=["GET"])
    @jwt_required(optional=True)
    @require_params(['study_id'])
    def study_get():
        try:
            study_id = request.args.get('study_id', None)
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)
            return create_response(metaboserv.study_get(study_id, auth_user=current_user, auth_token=auth_token))
        except Exception as e:
            return create_error_response(e)
        
    @app.route(f"{api_path}StudyGetConcentrationData", methods=['GET'])
    @jwt_required(optional=True)
    @require_params(['study_id'])
    def study_get_concentration():
        try:
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)
            study_id = request.args.get('study_id', None)
            compounds = request.args.get('compounds', None)
            filters = request.args.get('filter', None)
            compound_split = compounds.split(';') if compounds else None
            filter_list = json.loads(filters) if filters else None
            return create_response(metaboserv.study_get_concentration(study_id, compounds=compound_split, pfilter=filter_list, auth_user=current_user, auth_token=auth_token))
        except Exception as e:
            return create_error_response(e)
        
    @app.route(f"{api_path}StudyGetCompounds", methods=['GET'])
    @jwt_required(optional=True)
    @require_params(['studies'])
    def study_get_compounds():
        try:
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)
            studies = request.args.get('studies').split(';')
            if not studies or len(studies) == 0: return
            return create_response(metaboserv.study_get_compounds(studies, auth_user=current_user, auth_token=auth_token))
        except Exception as e:
            return create_error_response(e)
        
    @app.route(f"{api_path}StudyGetUnits", methods=['GET'])
    @jwt_required(optional=True)
    @require_params(['studies'])
    def study_get_units():
        try:
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)
            studies = request.args.get('studies').split(';')
            return create_response(metaboserv.study_get_units(studies, auth_user=current_user, auth_token=auth_token))
        except Exception as e:
            return create_error_response(e)

    @app.route(f"{api_path}StudyListSources", methods=["GET"])
    @jwt_required(optional=True)
    @require_params(['study_id'])
    def study_list_sources():
        try:
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)
            study_id = request.args.get('study_id')
            return create_response(metaboserv.study_list_sources(study_id, auth_user=current_user, auth_token=auth_token))
        except Exception as e:
            return create_error_response(e)

    @app.route(f"{api_path}StudyListContributors", methods=["GET"])
    @jwt_required(optional=True)
    @require_params(['study_id'])
    def study_list_contributors():
        try:
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)
            study_id = request.args.get('study_id')
            return create_response(metaboserv.study_list_contributors(study_id, auth_user=current_user, auth_token=auth_token))
        except Exception as e:
            return create_error_response(e)

    ################### Compound-related methods ###################

    @app.route(f"{api_path}CompoundAddFromHMDB", methods=["POST"])
    @require_params(["filename"], parameter_type="POST")
    @admin_required()
    def compound_add_from_hmdb_file():
        filename = request.form.get('filename')
        try:
            hmdb_result = hmdb_extract_file(data_folder, filename)
            if hmdb_result.status == 500:
                return create_error_response("Something went wrong during HMDB data extraction.")
            elif hmdb_result.status == 200:
                hmdb_dict = hmdb_result.body
                metaboserv.mariadb_add_nconcs(hmdb_dict)
                metaboserv.compound_add_from_dict(hmdb_dict)
            return create_response(ResultObject(msg=f"Successfully extracted {filename}."))    
        except Exception as e:
            logging.error(f"Encountered an API error during HMDB file parsing: {e}")
            return create_error_response(e)
    
    @app.route(f"{api_path}CompoundGetNconcs", methods=['GET'])
    @require_params(['compound_id'])
    def compound_get_nconcs():
        compound_id = request.args.get('compound_id')
        try:
            return create_response(metaboserv.compound_get_nconcs(compound_id))
        except Exception as e:
            logging.error(f"Encountered an API error during Nconc retrieval: {e}")
            return create_error_response(e)
        
    @app.route(f"{api_path}CompoundsToHMDB", methods=['GET'])
    @require_params(['compounds'])
    def compounds_to_hmdb():
        compound_string = request.args.get('compounds')
        compounds = compound_string.strip().split(';')
        try:
            return create_response(metaboserv.compounds_to_hmdb(compounds))
        except Exception as e:
            logging.error(f"Encountered an API error during HMDB conversion: {e}")
            return create_error_response(e)
        
    @app.route(f"{api_path}NextCompound", methods=['GET'])
    @require_params(['compound', 'hmdb_accession'])
    def next_compound():
        compound = request.args.get('compound')
        hmdb = request.args.get('hmdb_accession')
        try:
            return create_response(metaboserv.compound_next(compound, hmdb=hmdb))
        except Exception as e:
            logging.error(f"Encountered an API error during metabolite retrieval: {e}")
            return create_error_response(e)
        
    @app.route(f"{api_path}PreviousCompound", methods=['GET'])
    @require_params(['compound', 'hmdb_accession'])
    def previous_compound():
        compound = request.args.get('compound')
        hmdb = request.args.get('hmdb_accession')
        try:
            return create_response(metaboserv.compound_previous(compound, hmdb=hmdb))
        except Exception as e:
            logging.error(f"Encountered an API error during metabolite retrieval: {e}")
            return create_error_response(e)
        
    @app.route(f"{api_path}CompoundGetNames", methods=['GET'])
    @require_params(['compounds', 'study_id'])
    def compound_get_names():
        compound_string = request.args.get('compounds')
        study_id = request.args.get('study_id')
        compounds = compound_string.strip().split('$')
        try:
            return create_response(metaboserv.compound_retrieve_names(compounds, study_id))
        except Exception as e:
            logging.error(f"Encountered an API error while retrieving human-readable compound names: {e}")
            return create_error_response(e)
        
    @app.route(f"{api_path}CompoundGetMetabolitesFromFile", methods=['POST'])
    def compound_get_names_from_file():
        conc_file = request.files.get('conc_file', None)
        conc_transpose = True if request.form.get('conc_transpose', "false") == "true" else False
        if not conc_file or conc_file.filename == '': 
            return create_response(ResultObject(msg="No file was provided!", status=400))
        try:
            if not allowed_file(conc_file.filename):
                return create_response(ResultObject(msg="Received an illegal filename!", status=400))
            filename = secure_filename(conc_file.filename)
            filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            conc_file.save(filepath)
            filetype_check = metaboserv.file_validate_type(filepath)
            if filetype_check.status not in [200, 201]:
                return create_error_response("File type validation failed!")
            return create_response(metaboserv.compound_retrieve_metabolites_from_file(filepath, conc_transpose))
        except Exception as e:
            logging.error(f"Encountered an API error while getting metabolite names from file: {e}")
            return create_error_response(e)
        
    @app.route(f"{api_path}CompoundMappedAsCSV", methods=['POST'])
    @require_params(['map_results'], parameter_type='JSON')
    def compound_mapped_as_csv():
        res = request.json.get('map_results')
        try:
            res = json.loads(res)
            file_obj = metaboserv.compound_mapped_as_csv(res)
            if file_obj.status == 200:
                return send_from_directory(app.config['UPLOAD_FOLDER'], file_obj.getBody())
        except Exception as e:
            logging.error(f"Encountered an API error while getting mapped CSV: {e}")
            return create_error_response(e)
        
    @app.route(f"{api_path}CompoundMappedAsJSON", methods=['POST'])
    @require_params(['map_results'], parameter_type='JSON')
    def compound_mapped_as_json():
        res = request.json.get('map_results')
        try:
            res = json.loads(res)
            file_obj = metaboserv.compound_mapped_as_json(res)
            if file_obj.status == 200:
                return send_from_directory(app.config['UPLOAD_FOLDER'], file_obj.getBody())
        except Exception as e:
            logging.error(f"Encountered an API error while getting mapped JSON: {e}")
            return create_error_response(e)
        
    @app.route(f"{api_path}CompoundMappedAsAdaptedFile", methods=['POST'])
    def compound_mapped_adapted_file():
        conc_file = request.files.get('conc_file', None)
        adapted_map = request.form.get('adapted_metabolites', None)
        conc_transpose = True if request.form.get('transpose', 'false') == 'true' else False
        if not conc_file or not adapted_map:
            return create_response(ResultObject(msg="File or metabolites missing!", status=400))
        if conc_file.filename == '':
            return create_response(ResultObject(msg="No file was provided!", status=400))
        try:
            if not allowed_file(conc_file.filename):
                return create_response(ResultObject(msg="Received an illegal filename!", status=400))
            filename = secure_filename(conc_file.filename)
            filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            conc_file.save(filepath)
            filetype_check = metaboserv.file_validate_type(filepath)
            if filetype_check.status not in [200, 201]:
                return create_error_response("File type validation failed!")
            try:
                adapted_map_json = json.loads(adapted_map)
                file_obj = metaboserv.compound_mapped_adapted_file(filepath, adapted_map_json, conc_transpose)
                if file_obj.status == 200:
                    return send_from_directory(app.config['UPLOAD_FOLDER'], path=file_obj.getBody())
            except Exception as e:
                logging.error(f"Encountered an error while performing the adaptation: {e}")
                return create_error_response("Conc. file could not be adapted!")
        except Exception as e:
            logging.error(f"Encountered an API error while adapting conc. file: {e}")
            return create_error_response(e)

    ################### Phenotype-related methods ###################

    @app.route(f"{api_path}PhenotypeGet", methods=["GET"])
    @jwt_required(optional=True)
    @require_params(['study_id'])
    def phenotype_get():
        try:
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)
            study_id = request.args.get('study_id', None)
            return create_response(metaboserv.phenotype_get(study_id, auth_user=current_user, auth_token=auth_token))
        except Exception as e:
            return create_error_response(e)
        
    @app.route(f"{api_path}PhenotypeGetMultiple", methods=["GET"])
    @jwt_required(optional=True)
    @require_params(['studies'])
    def phenotype_get_multiple():
        try:
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)
            studies = request.args.get('studies', None).split(';')
            return create_response(metaboserv.phenotype_get_multiple(studies, auth_user=current_user, auth_token=auth_token))
        except Exception as e:
            return create_error_response(e)
        
    @app.route(f"{api_path}PhenotypeGetInformation", methods=["GET"])
    @jwt_required(optional=True)
    @require_params(['study_id'])
    def phenotype_get_information():
        try:
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)
            study_id = request.args.get('study_id', None)
            return create_response(metaboserv.phenotype_get_information(study_id, auth_user=current_user, auth_token=auth_token))
        except Exception as e:
            return create_error_response(e)
        
    ################### Full frontend queries ###################
        
    @app.route(f"{api_path}MetaboservQuery", methods=['GET'])
    @jwt_required(optional=True)
    @require_params(['studies'])
    def metaboserv_query_v1():
        try:
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)
            studies = json.loads(request.args.get('studies'))
            compounds = request.args.get('compounds', None)
            filters = request.args.get('filters', None)
            vis_ranges = request.args.get('vis_ranges', None)
            phenotype_filters = request.args.get('phenotypes', None)
            plot_create = request.args.get('plots', 'scatterplot;histogram')
            aggregate = False if request.args.get('aggregate', 'true') == "false" else True

            if phenotype_filters: phenotype_filters = json.loads(phenotype_filters)
            conc_dict = {}
            compound_name_map = {}
            unit_map = {}
            for study in studies:
                study_results = metaboserv.study_get_concentration(
                    study,
                    json.loads(compounds) if compounds else None,
                    phenotype_filter=phenotype_filters,
                    auth_user=current_user,
                    auth_token=auth_token
                ).getBody()
                if study_results:
                    conc_dict[study] = study_results[0]
                    compound_name_map = {**compound_name_map, **study_results[1]}
                    unit_map_study = metaboserv.study_get_units([study], current_user, auth_token).getBody()[study]
                    if unit_map_study:
                        unit_map = combine_dicts(unit_map, unit_map_study)
            conc_frame = metaboserv_plotter.query_results_to_dataframe(conc_dict, aggregate=True if aggregate and len(studies) >= 2 else False).getBody() # Also performs some grouping and aggregation
            if aggregate and len(studies) >= 2:
                cd = {}
                for source, row in conc_frame.iterrows():
                    sid = row["study_id"]
                    if sid not in cd.keys():
                        cd[sid] = {source: row.fillna("").to_dict()}
                    else:
                        cd[sid][source] = row.fillna("").to_dict()
                conc_dict = cd
            conc_dict = metaboserv.study_filter_concentration(conc_dict, filter=json.loads(filters) if filters else None)
            phenotypes_in_frame = [header for header in conc_frame.columns.values.tolist() if header.startswith('PHENOTYPE_')] 
            plots = plot_create.split(';')
            if plots:
                # Dirty solution, but the only one I can think of: Remake conc frame
                conc_frame = metaboserv_plotter.query_results_to_dataframe(conc_dict, aggregate=True if aggregate and len(studies) >= 2 else False).getBody()
                compound_name_map = {k: v for k,v in compound_name_map.items() if v in list(conc_frame.columns)}
                conc_dict["METABOSERV_PLOTS"] = {}
                vis_ranges = dictify_vis_ranges(json.loads(vis_ranges)) if vis_ranges else None
                if 'scatterplot' in plots and len(compound_name_map.keys()) >= 2:
                    scatter_id = metaboserv_plotter.generate_scatterplot(
                        conc_frame,
                        metabolites_to_plot=[compound_name_map[k] for k in list(compound_name_map.keys())[:2]],
                        vis_ranges=vis_ranges,
                        phenotype_to_plot=phenotypes_in_frame[0] if len(phenotypes_in_frame) != 0 else None,
                        units=unit_map,
                    )
                    if scatter_id.status == 200:
                        conc_dict["METABOSERV_PLOTS"]['scatterplot'] = scatter_id.getBody()['plot_id']
                    elif scatter_id.status == 416:
                        conc_dict["METABOSERV_PLOTS"]['scatterplot'] = "DIFFERENT_UNITS"
                if 'histogram' in plots:
                    hist_id = metaboserv_plotter.generate_histogram(
                        conc_frame,
                        metabolite_to_plot=compound_name_map[list(compound_name_map.keys())[0]],
                        vis_ranges=vis_ranges,
                        phenotype_to_plot=phenotypes_in_frame[0] if len(phenotypes_in_frame) != 0 else None,
                        units=unit_map,
                    )
                    if hist_id.status == 200:
                        conc_dict["METABOSERV_PLOTS"]['histogram'] = hist_id.getBody()['plot_id']
                    elif hist_id.status == 416:
                        conc_dict["METABOSERV_PLOTS"]['histogram'] = "DIFFERENT_UNITS"
                if 'correlations' in plots:
                    corr_id = metaboserv_plotter.generate_correlation_plot(
                        conc_frame,
                    )
                    if corr_id.status == 200:
                        conc_dict["METABOSERV_PLOTS"]['correlations'] = corr_id.getBody()['plot_id']
            return create_response(ResultObject(body=conc_dict))
        except Exception as e:
            return create_error_response(e)
        
    @app.route(f"{api_path}QueryResultsToCSV", methods=['POST'])
    @require_params(['query_results'], parameter_type='JSON')
    def query_results_to_csv():
        res = request.json.get('query_results')
        try:
            file_obj = metaboserv.query_results_to_csv(res)
            if file_obj.status == 200:
                return send_from_directory(app.config['UPLOAD_FOLDER'], file_obj.getBody())
        except Exception as e:
            logging.error(f"Encountered an API error while getting mapped CSV: {e}")
            return create_error_response(e)
        
    ######################### Plotting ########################
        
    @app.route(f"{api_path}PlotRetrieve", methods=["GET"])
    @require_params(['plot_id'])
    def plot_retrieve():
        try:
            plot_id = request.args.get('plot_id')
            plot_obj = metaboserv_plotter.retrieve_plot(plot_id).getBody()
            if plot_obj:
                return Response(plot_obj.getvalue(), mimetype='image/png', status=200)
            else:
                return create_response(ResultObject(msg=f"The plot {plot_id} could not be found!", status=404))
        except Exception as e:
            return create_error_response(e)
        
    @app.route(f"{api_path}PlotCreateScatter", methods=["POST"])
    @require_params(['plot_id', 'plot_data', 'plot_m1', 'plot_m2'], parameter_type="POST")
    def plot_create_scatter():
        try:
            plot_id = request.form.get('plot_id')
            plot_data = json.loads(request.form.get('plot_data'))
            plot_m1 = request.form.get('plot_m1')
            plot_m2 = request.form.get('plot_m2')

            vis_ranges = request.form.get('vis_ranges', None)

            if vis_ranges and vis_ranges != "": vis_ranges = dictify_vis_ranges(json.loads(vis_ranges))

            plot_phenotype = request.form.get('plot_phenotype', None)
            if plot_phenotype: plot_phenotype = 'PHENOTYPE_' + plot_phenotype

            plot_units = request.form.get('units', None)
            plot_units = json.loads(plot_units) if plot_units else {plot_m1: 'mmol/L', plot_m2: 'mmol/L'}

            query_frame = metaboserv_plotter.query_results_to_dataframe(plot_data).getBody()
            potential_plot = metaboserv_plotter.generate_scatterplot(
                query_frame, 
                metabolites_to_plot=[plot_m1, plot_m2],
                vis_ranges=vis_ranges,
                phenotype_to_plot=plot_phenotype,
                plot_id=plot_id,
                units=plot_units
            )
            return create_response(potential_plot)
        except Exception as e:
            return create_error_response(e)
        
    @app.route(f"{api_path}PlotCreateHistogram", methods=["POST"])
    @require_params(['plot_id', 'plot_data', 'plot_m1'], parameter_type="POST")
    def plot_create_histogram():
        try:
            plot_id = request.form.get('plot_id')
            plot_data = json.loads(request.form.get('plot_data'))
            plot_m1 = request.form.get('plot_m1')

            vis_ranges = request.form.get('vis_ranges', None)
            if vis_ranges: vis_ranges = dictify_vis_ranges(json.loads(vis_ranges))

            plot_phenotype = request.form.get('plot_phenotype', None)
            if plot_phenotype: plot_phenotype = 'PHENOTYPE_' + plot_phenotype

            plot_units = request.form.get('units', None)
            plot_units = json.loads(plot_units) if plot_units else {plot_m1: 'mmol/L'}

            plot_histstyle = request.form.get('plot_histstyle', 'step|fill')
            plot_bins = int(request.form.get('plot_bins', 30))

            query_frame = metaboserv_plotter.query_results_to_dataframe(plot_data).getBody()
            potential_plot = metaboserv_plotter.generate_histogram(
                query_frame,
                metabolite_to_plot=plot_m1, 
                vis_ranges=vis_ranges, 
                phenotype_to_plot=plot_phenotype, 
                plot_id=plot_id, 
                histstyle=plot_histstyle, 
                bins=plot_bins,
                units=plot_units
            )
            return create_response(potential_plot)
        except Exception as e:
            return create_error_response(e)
        
    @app.route(f"{api_path}PlotCreateCorrelations", methods=["POST"])
    @require_params(['plot_id', 'plot_data'], parameter_type="POST")
    def plot_create_correlations():
        try:
            plot_id = request.form.get('plot_id')
            plot_data = json.loads(request.form.get('plot_data'))

            query_frame = metaboserv_plotter.query_results_to_dataframe(plot_data).getBody()
            potential_plot = metaboserv_plotter.generate_correlation_plot(query_frame, plot_id)
            return create_response(potential_plot)
        except Exception as e:
            return create_error_response(e)
        
    @app.route(f"{api_path}PlotCreateCorrelationsFC", methods=["POST"])
    @jwt_required(optional=True)
    @require_params(['plot_id', 'study_id'], parameter_type="POST")
    def plot_create_correlations_fc():
        try:
            plot_id = request.form.get('plot_id')
            study_id = request.form.get('study_id')
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)


            study_results = metaboserv.study_get_concentration(
                study_id=study_id,
                auth_user=current_user,
                auth_token=auth_token,
                )
            conc_dict = {study_id: study_results.getBody()[0]}

            conc_frame = metaboserv_plotter.query_results_to_dataframe(conc_dict).getBody()
            potential_plot = metaboserv_plotter.generate_correlation_plot(conc_frame, plot_id)
            return create_response(potential_plot)
        except Exception as e:
            return create_error_response(e)
        
    @app.route(f"{api_path}QCPlotCreateMetabolite", methods=["POST"])
    @require_params(['plot_id', 'plot_data', 'study_id'], parameter_type="POST")
    def plot_create_qc_metabolite():
        try:
            plot_id = request.form.get('plot_id')
            plot_data = json.loads(request.form.get('plot_data'))
            study_id = request.form.get('study_id')

            over_lod = request.form.get('over_lod', 'true')
            if over_lod:
                over_lod = True if over_lod != 'false' else False

            query_frame = metaboserv_plotter.query_results_to_dataframe(plot_data).getBody()
            potential_plot = metaboserv_plotter.generate_quality_plot_per_metabolite(query_frame, plot_id, over_lod, study_id)
            return create_response(potential_plot)
        except Exception as e:
            return create_error_response(e)
        
    @app.route(f"{api_path}QCPlotCreateMetaboliteFC", methods=["POST"])
    @jwt_required(optional=True)
    @require_params(['plot_id', 'study_id'], parameter_type="POST")
    def plot_create_qc_metabolite_fc():
        try:
            plot_id = request.form.get('plot_id')
            study_id = request.form.get('study_id')
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)


            study_results = metaboserv.study_get_concentration(
                study_id=study_id,
                auth_user=current_user,
                auth_token=auth_token,
                )
            conc_dict = {study_id: study_results.getBody()[0]}
            compound_amount = len(study_results.getBody()[1].keys())

            conc_frame = metaboserv_plotter.query_results_to_dataframe(conc_dict).getBody()

            over_lod = request.form.get('over_lod', 'true')
            if over_lod:
                over_lod = True if over_lod != 'false' else False

            potential_plot = metaboserv_plotter.generate_quality_plot_per_metabolite(conc_frame, plot_id, over_lod, study_id, compound_amount)
            return create_response(potential_plot)
        except Exception as e:
            return create_error_response(e)
        
    @app.route(f"{api_path}QCPlotCreateSource", methods=["POST"])
    @require_params(['plot_id', 'plot_data', 'study_id'], parameter_type="POST")
    def plot_create_qc_source():
        try:
            plot_id = request.form.get('plot_id')
            plot_data = json.loads(request.form.get('plot_data'))
            study_id = request.form.get('study_id')

            over_lod = request.form.get('over_lod', 'true')
            if over_lod:
                over_lod = True if over_lod != 'false' else False

            query_frame = metaboserv_plotter.query_results_to_dataframe(plot_data).getBody()
            potential_plot = metaboserv_plotter.generate_quality_plot_per_source(query_frame, plot_id, over_lod, study_id)
            return create_response(potential_plot)
        except Exception as e:
            return create_error_response(e)
        
    @app.route(f"{api_path}QCPlotCreateSourceFC", methods=["POST"])
    @jwt_required(optional=True)
    @require_params(['plot_id', 'study_id'], parameter_type="POST")
    def plot_create_qc_source_fc():
        try:
            plot_id = request.form.get('plot_id')
            study_id = request.form.get('study_id')
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)


            study_results = metaboserv.study_get_concentration(
                study_id=study_id,
                auth_user=current_user,
                auth_token=auth_token,
                )
            conc_dict = {study_id: study_results.getBody()[0]}

            conc_frame = metaboserv_plotter.query_results_to_dataframe(conc_dict).getBody()

            over_lod = request.form.get('over_lod', 'true')
            if over_lod:
                over_lod = True if over_lod != 'false' else False

            potential_plot = metaboserv_plotter.generate_quality_plot_per_source(conc_frame, plot_id, over_lod, study_id)
            return create_response(potential_plot)
        except Exception as e:
            return create_error_response(e)
        
    @app.route(f"{api_path}PlotBrukerSpectrum", methods=["POST"])
    @jwt_required(optional=True)
    @require_params(['study_id', 'filename', 'plot_id'], parameter_type="POST")
    def plot_raw_spectrum():
        try:
            filename = request.form.get('filename')
            study_id = request.form.get('study_id')
            plot_id = request.form.get('plot_id')
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)

            auth_check_user = metaboserv.user_check_authorization(current_user, study_id, required_permission="viewer")
            auth_check_token = metaboserv.token_check_authorization(auth_token, study_id, required_permission="viewer")
            if not auth_check_user and not auth_check_token:
                return create_response(ResultObject(msg=f"You lack the necessary permissions to grant access to study {study_id}!", status=401))

            potential_plot = metaboserv_plotter.plot_spectrum(
                general_config['source_location'],
                study_id,
                filename,
                (raw_config['path']+":"+str(raw_config['port'])),
                plot_id
            )
            return create_response(potential_plot)
            pass
        except Exception as e:
            return create_error_response(e)

    ################### User authentication ###################
        
    @jwt.user_lookup_loader
    def user_lookup(_jwt_header, jwt_data):
        """
        Callback function that looks up a user in ES database whenever a protected route is queried.

        :param _jwt_header: Header of the JWT
        :param jwt_data: Main data of the JWT
        :returns An object representing the user
        """
        id = jwt_data["sub"]
        return dict2obj(metaboserv.user_internal_get_information(id).getBody())

    @jwt.user_identity_loader
    def user_identity_loader(user) -> str:
        """
        Callback function that returns the identity object in a JSON serializable format.

        :param user: A user object
        :returns The username pertaining to the user object
        """
        return user.username

    @jwt.unauthorized_loader
    def unauthorized_callback(callback_response: str) -> Response:
        """
        Redirects to the login page if unauthorized access occurs.

        :param callback_response - A response to return (currently unused)
        :returns Response given for unauthorized access
        """
        return jsonify({'msg': "You lack the required permissions to perform this action."}), 401

    @app.route(f'{api_path}register', methods=['POST'])
    @require_params(['username', 'password', 'email'], parameter_type="JSON")
    def register() -> Response:
        """
        Open endpoint that allows the creation of user accounts.
        Different from the user_create admin method that is not meant for frontend use.

        Parameters must be given in JSON format:
        :param username - The username for the new account
        :param password - The password for the new account (will be hashed and salted)
        :param email - The E-Mail Address of the user
        :param institution - The institution of the user (optional, defaults to "Unknown" in frontend)
        """
        try:
            username = request.json.get("username", None)
            password = request.json.get("password", None)
            email = request.json.get("email", None)
            institution = request.json.get("institution", "Unknown")

            email_validation = metaboserv.user_email_check_exists(email)
            if email_validation.status != 200:
                return jsonify({'msg': email_validation.msg, 'status': email_validation.status})
            email = email_validation.getBody() # Use normalized version of email address

            if username.lower() in FORBIDDEN_IDS:
                return jsonify({'msg': "Provided username contains a protected keyword and can not be used.", 'status': 401}), 401
            if metaboserv.user_check_exists(username.lower()):
                return jsonify({'msg': "A user with that name exists already.", 'status': 409}), 409
            
            return create_response(metaboserv.user_create(username, password, email, institution))
        except Exception as e:
            return create_error_response(e)

    @app.route(f'{api_path}login', methods=['POST'])
    @require_params(['username', 'password'], parameter_type="JSON")
    def login() -> Response:
        """
        Login function that expects a username and password in JSON data.
        If permission checking was successful, it creates and returns a JWT token.

        :param username - The username in question
        :param password - The password in question
        :returns 200: Access Token (JWT), 400: Username or passowrd missing, 401: Login failed
        :remarks Does not throw a 404 if a user does not exist to add some additional privacy
        """
        try:
            username = request.json.get("username", None)
            password = request.json.get("password", None)
            additional_claims = {}

            user_info = metaboserv.user_internal_get_information(username).getBody()

            user = dict2obj(metaboserv.user_authenticate_and_return(username, password)) #Returns none if user not found or password fails
            if not user:
                metaboserv.user_track_login_attempt(user_info)
                return jsonify(msg="Wrong username or password!", status=401), 401
            
            if user.username == "admin" and user.role == "admin":
                additional_claims["is_admin"] = True
            else:
                additional_claims["is_admin"] = False

            # Check if account is locked
            if 'locked' in user_info.keys() and user_info['locked'] != "NO":
                if datetime.datetime.today() < datetime.datetime.strptime(user_info['locked'], '%d/%m/%Y, %H:%M:%S'):
                    return create_response(ResultObject(msg=f"Sorry, this account is locked until {user_info['locked']} because of suspicious login attempts.", status=401))
                else:
                    status = metaboserv.user_clear_lock(username)
                    if status.status != 200:
                        logging.info(f"Warning: Account unlocking failed for account {username}")

            access_token = create_access_token(identity = user, additional_claims=additional_claims)
            untracked_so_far = user_info["untracked"] if "untracked" in user_info.keys() else 0
            metaboserv.user_track_login_attempt(user_info, mode="reset")
            return jsonify(access_token = access_token, status=200,
                msg=f'Login successful! Unsuccessful login attempts since your last login: {untracked_so_far}.',
                username=username, verified=True if user_info['verified'] == "YES" else False), 200 
        except Exception as e:
            logging.error(f"Login failed for {username} with error {e}.")
            return jsonify(msg=f"Login failed for {username} because of a server error."), 500

    @app.route(f'{api_path}UserGet', methods=['GET'])
    @jwt_required()
    @require_params(['username'])
    def user_get() -> Response:
        """
        Gets user data.
        """
        try:
            username = request.args.get('username', None)
            if current_user.username != "admin" and current_user.username != username:
                return jsonify(msg="You are not authorized to retrieve this user's data!"), 401
            if not metaboserv.user_check_exists(username):
                return jsonify(msg=f"User {username} does not exist!"), 404
            return create_response(metaboserv.user_get_information(username, current_user))
        except Exception as e:
            return create_error_response(e)

    @app.route(f'{api_path}UserUpdate', methods=['POST'])
    @jwt_required()
    @require_params(['username', 'study_id'], parameter_type="POST")
    def user_update() -> Response:
        """
        Updates permissions for a user. Can both add and revoke permissions.
        Takes form parameters:
        :param username: The name of the user to add permissions to
        :param study_id: The study to add permissions for
        :param level: Level to grant (viewer/contributor). When passing "revoke" any permissions will be revoked.
        :returns 200: Success, 400: Missing parameters, 401: Unauthorized, 403: Forbidden, 404: User to update or study not found, 409: User already has permission
        """
        try:
            username = request.form.get('username', None)
            study = request.form.get('study_id', None)
            level = request.form.get('level', "viewer")
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)

            if not metaboserv.user_check_exists(username):
                return create_response(ResultObject(msg="You must provide a username!", status=400))

            if level == "viewer":
                return create_response(metaboserv.user_make_viewer(username, study, auth_user=current_user, auth_token=auth_token))
            elif level == "contributor":
                return create_response(metaboserv.user_make_contributor(username, study, auth_user=current_user))
            elif level == "revoke":
                return create_response(metaboserv.user_revoke_permissions(username, study, auth_user=current_user))
        except Exception as e:
            logging.error(f"Failed adding permission to user entity with error: {e}.")
            return create_error_response(e)

    @app.after_request
    def refresh_jwts(response) -> Response:
        """
        Called after each request to the service.
        Refreshes soon-expiring JWTs (30 minutes) if that JWT was used for a request.
        """
        try:
            exp_timestamp = get_jwt()["exp"]
            now = datetime.datetime.now()
            target_timestamp = datetime.datetime.timestamp(now + datetime.timedelta(minutes=30))
            if target_timestamp > exp_timestamp:
                access_token = create_access_token(identity=get_jwt_identity())
                set_access_cookies(response, access_token)
            return response
        except (RuntimeError, KeyError):
            return response
        
    @app.route(f'{api_path}UserRequestVerificationMail', methods=['POST'])
    @jwt_required()
    @require_params(['username', 'email'], parameter_type="POST")
    def user_request_verification() -> Response:
        """
        Requests a verification mail for the user. Will only send a mail if the user is not yet verified.
        Takes form parameters:
        :param username: The name of the user to verify
        :param email: The email address to send the mail to
        """
        try:
            username = request.form.get('username', None)
            email = request.form.get('email', None)
            if not smtp_config["verification"]:
                return create_response(ResultObject(msg="Verification is currently turned off for this MetaboSERV instance!", status=200))
            if not current_user or isinstance(current_user, str):
                return create_response(ResultObject(msg="You must be logged in to request a verification mail!", status=401))
            if current_user.username != username and current_user.username != 'admin':
                return create_response(ResultObject(msg="You can't request verification mails for another user!", status=401))
            return create_response(metaboserv.user_send_verification_email(username, email))
        except Exception as e:
            logging.error(f"Failed sending a verification mail to {email} with error: {e}.")
            return create_error_response(e)
        
    @app.route(f'{api_path}UserVerify', methods=['POST'])
    @jwt_required()
    @require_params(['username', 'code'], parameter_type="POST")
    def user_verify() -> Response:
        """
        Attempts to verify a user account using the code they received in their email.
        Verification codes are valid for 24h.
        Takes form parameters:
        :param username: The name of the user to verify
        :param code: The verification code
        """
        try:
            username = request.form.get('username', None)
            code = request.form.get('code', None)
            if not current_user or isinstance(current_user, str):
                return create_response(ResultObject(msg="You must be logged in verify this account!", status=401))
            if not current_user.username == username and current_user.username != 'admin':
                return create_response(ResultObject(msg="You can't verify other users!", status=401))
            return create_response(metaboserv.user_verify(username, code))
        except Exception as e:
            logging.error(f"Failed verifying user with error: {e}.")
            return create_error_response(e)
        
    @app.route(f'{api_path}UserDelete', methods=['POST'])
    @jwt_required()
    @require_params(['username'], parameter_type="POST")
    def user_delete() -> Response:
        """
        Deletes a user account. Only the user themselves or the admin account can do this.
        Will also remove all studies uploaded by the user, and remove the user from any other studies' permission lists.
        Takes form parameters:
        :param username: The name of the user to delete
        """
        try:
            username = request.form.get('username', None)
            if not current_user or isinstance(current_user, str):
                return create_response(ResultObject(msg="You must be admin, or logged in to delete this account!", status=401))
            if not current_user.username == username and current_user.username != 'admin':
                return create_response(ResultObject(msg="You can't delete other users!", status=401))
            return create_response(metaboserv.user_delete(username, auth_user=current_user))
        except Exception as e:
            logging.error(f"Failed deleting user with error: {e}.")
            return create_error_response(e)
        
    @app.route(f'{api_path}UserChangePassword', methods=['POST'])
    @jwt_required()
    @require_params(['username', 'old_password', 'new_password'], parameter_type="POST")
    def user_change_password() -> Response:
        """
        Changes the password for a user account.
        Takes form parameters:
        :param username: The name of the user to change the password for
        :param new_password: The new password to set for the user
        """
        try:
            username = request.form.get('username', None)
            old_password = request.form.get('old_password', None)
            new_password = request.form.get('new_password', None)
            if not current_user or isinstance(current_user, str):
                return create_response(ResultObject(msg="You must be logged in to change your password!", status=401))
            if not current_user.username == username and current_user.username != 'admin':
                return create_response(ResultObject(msg="You can't change the password for other users!", status=401))
            return create_response(metaboserv.user_change_password(username, new_password, old_password))
        except Exception as e:
            logging.error(f"Failed changing user for password with error: {e}.")
            return create_error_response(e)
        
    @app.route(f'{api_path}UserSendPasswordForgottenCode', methods=['POST'])
    @require_params(['username'], parameter_type="POST")
    def send_recovery_code() -> Response:
        try:
            username = request.form.get('username', None)
            if not username:
                return ResultObject(msg="No username was provided!", status=404)
            if username == "admin":
                return ResultObject(msg="The admin password can not be changed here!", status=401)
            return create_response(metaboserv.user_send_recovery_mail(username))
        except Exception as e:
            logging.error(f"API Error: {e}.")
            return create_error_response(e)
        
    @app.route(f'{api_path}UserEnterPasswordForgottenCode', methods=['POST'])
    @require_params(['username', 'code'], parameter_type="POST")
    def set_recovery_password() -> Response:
        try:
            username = request.form.get('username', None)
            code = request.form.get('code', None)
            if not username:
                return ResultObject(msg="No username or no code was provided!", status=404)
            return create_response(metaboserv.user_recover_password(username, code))
        except Exception as e:
            logging.error(f"API Error: {e}.")
            return create_error_response(e)

    ################### Token authentication ###################

    @app.route(f"{api_path}TokenLogin", methods=['POST'])
    @require_params(['token_id'], parameter_type="JSON")
    def token_login() -> Response:
        try:
            auth_token = request.json.get('token_id')
            if not auth_token: return jsonify(msg='No token was submitted!', status=400), 400
            return create_response(metaboserv.token_check_exists(auth_token))
        except Exception as e:
            logging.error(f"Failed validating token with error: {e}.")
            return create_error_response(e)
        
    @app.route(f"{api_path}TokenListForStudy", methods=['GET'])
    @require_params(['study_id'], parameter_type="GET")
    @jwt_required(optional=True)
    def token_list_for_study() -> Response:
        try:
            study_id = request.args.get('study_id')
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)
            return create_response(metaboserv.study_list_tokens(study_id, current_user, auth_token))
        except Exception as e:
            logging.error(f"Failed supplying token list for study with error: {e}.")
            return create_error_response(e)

    @app.route(f"{api_path}TokenCreate", methods=['POST'])
    @require_params(['study'], parameter_type="POST")
    @jwt_required()
    def token_create() -> Response:
        try:
            study = request.form.get('study').split(';')
            auth_level = request.form.get('auth_level', "viewer")
            comment = request.form.get('comment', "No comment")
            try:
                expiration = int(request.form.get('expiration'), 0)
            except:
                expiration = 0
            created_token = metaboserv.token_create(study=study, auth_level=auth_level, 
                expiration=expiration, comment=comment, auth_user=current_user)
            return create_response(created_token)
        except Exception as e:
            logging.error(f"Failed creating token with error: {e}.")
            return create_error_response(e)

    @app.route(f"{api_path}TokenRevoke", methods=['POST'])
    @require_params(['token_id'], parameter_type="POST")
    @jwt_required()
    def token_revoke() -> Response:
        try:
            token_id = request.form.get('token_id')
            return create_response(metaboserv.token_revoke(token_id, auth_user=current_user))
        except Exception as e:
            logging.error(f"Failed revoking token with error: {e}.")
            return create_error_response(e)
        
    ################### Raw data up/download #######################
        
    @app.route(f"{api_path}UploadExperimentalData", methods=['POST'])
    @jwt_required(optional=True)
    def upload_raw_data() -> Response:
        try:
            raw_data = request.files.get('data', None)
            study_id = request.form.get('study_id', None)
            auth_token = request.form.get('METABOSERV-AUTH-TOKEN', None)
            if not study_id:
                return create_response(ResultObject(msg="No study ID provided!", status=400))
            if (not current_user or isinstance(current_user, str)) and not auth_token: return ResultObject(msg="You must be logged in to upload experimental data!", status=401)
            token_check = metaboserv.token_check_authorization(auth_token, study_id, required_permission="contributor")
            user_check = metaboserv.user_check_authorization(current_user, study_id, required_permission="contributor")
            if not token_check and not user_check:
                return ResultObject(msg="You don't have permissions to upload experimental data to this study!", status=401)
            if raw_data:
                # Grab current chunk ID from HTTP request
                current_chunk = int(request.form.get('dzchunkindex'))

                # Perform some sanity checks
                if current_chunk is None:
                    return create_response(ResultObject(msg="No chunk ID received!", status=400))
                if not raw_data.filename or raw_data.filename == '':
                    return create_response(ResultObject(msg="Received an empty file/chunk!", status=400))
                if not allowed_file(raw_data.filename):
                    return create_response(ResultObject(msg="No chunk ID received!", status=400))
                filename = secure_filename(raw_data.filename)
                filepath = os.path.join(source_folder, study_id, filename)

                # Create the according folder if it does not exist
                if not (os.path.isdir(os.path.join(source_folder, study_id))):
                    os.mkdir(os.path.join(source_folder, study_id))

                # If a file exists and chunk is 0, the file already exists
                if (os.path.exists(filepath)) and current_chunk == 0:
                    return create_response(ResultObject(msg="File already exists!", status=409))
                
                # Filetype checks are deactivated for now.
                #filetype_check = metaboserv.file_validate_type(filepath)
                #if filetype_check.status not in [200, 201]:
                #    os.remove(os.path.join(source_folder, filename))
                #    return create_error_response("File type validation failed!")
                
                # Otherwise, add chunk to the file
                try:
                    with open(filepath, 'ab') as f:
                        f.seek(int(request.form.get('dzchunkbyteoffset')))
                        f.write(raw_data.stream.read())
                except OSError:
                    return create_response(ResultObject(msg="Sorry, we could not write to the file! Possibly the storage is full?", status=500))
                
                total_chunks = int(request.form.get('dztotalchunkcount'))
                if current_chunk + 1 == total_chunks:
                    # This was the last chunk, the file should be complete and the size we expect
                    if os.path.getsize(filepath) != int(request.form['dztotalfilesize']):
                        return create_response(ResultObject(msg="There is a size mismatch between the expected and uploaded file size!", status=500))
                    else:
                        return create_response(ResultObject(msg="File uploaded successfully!"))

                return create_response(ResultObject(msg="Chunk uploaded successfully!"))
            else:
                return create_response(ResultObject(msg="Provided file seems to be empty!", status=400))
        except Exception as e:
            logging.error(request.form)
            logging.error(int(request.form.get('dzchunkbyteoffset')))
            logging.error(int(request.form.get('dztotalchunkcount')))
            logging.error(f"Failed uploading raw data with error: {e}.")
            return create_error_response(e)
        
    @app.route(f"{api_path}DownloadExperimentalData", methods=['GET'])
    @jwt_required(optional=True)
    def download_raw_data() -> Response:
        try:
            study_id = request.args.get('study_id', None)
            filename = request.args.get('filename', None)
            auth_token = request.args.get('METABOSERV-AUTH-TOKEN', None)
            if not study_id or not filename:
                return create_response(ResultObject(msg="Filename and study ID are required!", status=400))
            token_check = metaboserv.token_check_authorization(auth_token, study_id, required_permission="viewer")
            user_check = metaboserv.user_check_authorization(current_user, study_id, required_permission="viewer")
            if not token_check and not user_check:
                return create_response(ResultObject(msg="You don't have permissions to download this file!", status=401))
            if not metaboserv.study_check_exists(study_id):
                return create_response(ResultObject(msg="Study does not exist!", status=404))
            upload_path = os.path.join(source_folder, study_id)
            if not os.path.isfile(os.path.join(upload_path, filename)):
                return create_response(ResultObject(msg="File does not exist!", status=404))
            return send_file(os.path.join(upload_path, filename))
        except Exception as e:
            logging.error(f"Failed downloading raw data with error: {e}.")
            return create_error_response(e)

    @app.route(f"{api_path}GetExperimentalDataFilenames", methods=['GET'])
    @jwt_required(optional=True)
    def get_exp_filenames() -> Response:
        try:
            study_id = request.args.get('study_id', None)
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)
            if not study_id:
                return create_response(ResultObject(msg="No study ID provided!", status=400))
            token_check = metaboserv.token_check_authorization(auth_token, study_id, required_permission="viewer")
            user_check = metaboserv.user_check_authorization(current_user, study_id, required_permission="viewer")
            if not token_check and not user_check:
                return create_response(ResultObject(msg="You don't have permissions to retrieve the available files for this study!", status=401))
            if not metaboserv.study_check_exists(study_id):
                return create_response(ResultObject(msg="Study does not exist!", status=404))
            return create_response(metaboserv.study_get_exp_data(study_id, current_user, auth_token))
        except Exception as e:
            logging.error(f"Failed getting exp. data filenames: {e}.")
            return create_error_response(e)

    @app.route(f"{api_path}DeleteExperimentalDataFile", methods=['POST'])
    @jwt_required(optional=True)
    def delete_exp_file() -> Response:
        try:
            study_id = request.form.get('study_id', None)
            filename = request.form.get('filename', None)
            auth_token = request.headers.get('METABOSERV-AUTH-TOKEN', None)
            if not study_id or not filename:
                return create_response(ResultObject(msg="No study ID or no filename provided!", status=400))
            if (not current_user or isinstance(current_user, str)) and not auth_token: return ResultObject(msg="You must be logged in to delete experimental data!", status=401)
            token_check = metaboserv.token_check_authorization(auth_token, study_id, required_permission="contributor")
            user_check = metaboserv.user_check_authorization(current_user, study_id, required_permission="contributor")
            if not token_check and not user_check:
                return ResultObject(msg="You don't have permissions to delete this file!", status=401)
            if not metaboserv.study_check_exists(study_id):
                return create_response(ResultObject(msg="Study does not exist!", status=404))
            return create_response(metaboserv.study_delete_exp_data(study_id, filename))
        except Exception as e:
            logging.error(f"Failed deleting experimental data file: {e}.")
            return create_error_response(e)
        
    @app.route(f"{api_path}DeleteTempFile", methods=['POST'])
    @require_params(["filename"], parameter_type="POST")
    def delete_temp_file() -> Response:
        try:
            filename = request.form.get('filename', None)
            if not filename:
                return create_response(ResultObject(msg="No filename provided!", status=400))
            return create_response(metaboserv.delete_temp_file(filename))
        except Exception as e:
            logging.error(f"Failed deleting experimental data file: {e}.")
            return create_error_response(e)

            
    ################################################################

    app.run(host=api_host, port=str(api_port))
