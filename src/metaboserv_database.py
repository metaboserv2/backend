import datetime
import functools
import hashlib
import json
import random
import re
import numpy as np
import secrets
import os
import logging
import shutil
import string
import time
import mariadb
import smtplib
import requests
import pandas as pd
from email.mime.text import MIMEText
from email_validator import validate_email, EmailNotValidError
from elasticsearch import Elasticsearch, NotFoundError, AuthenticationException, AuthorizationException
from elasticsearch.helpers import scan
from metaboserv_es_data import INDICES, FORBIDDEN_IDS, MAPPINGS
from metaboserv_exceptions import InvalidBiospecimenException
from metaboserv_input import adapt_conc_file, get_conc_data_from_file, get_metadata_from_file, get_phenotypes_from_file, parse_concentration_file, ResultObject, validate_file_type, adapt_compound_for_mariadb
from metaboserv_permissions import dict2obj
from werkzeug.datastructures import FileStorage

class MetaboservDatabase:

    es_connection_uri: str  # Path to the Elasticsearch instance
    es_is_connected: bool  # Is the Elasticsearch connection working?
    es_client: Elasticsearch  # Elasticsearch Python client
    md_connection_uri: str # Path to the MariaDB instance
    md_username: str # MariaDB username
    md_password: str # MariaDB password
    md_database: str # MariaDB database
    biospecimen: list # List of contained biospecimen
    smtp_config: dict # SMTP configuration
    source_folder: str # Folder for source data
    upload_folder: str # Folder for temp. data storage
    hmdb_regex: re.Pattern


    """
    Singleton wrapper class for mediation between client and the ElasticSearch and MariaDB databases.
    Parameters are mostly set from configuration file.
    """

    ################### Connection #####################

    def __init__(self, elasticsearch_config: dict, elasticsearch_host: str, mariadb_config: dict, mariadb_host: str,
        docker_bool: bool, admin_pw: str, source_folder: str, upload_folder: str, smtp_config: dict):
        """
        Creates the MetaboservDatabase object.
        Creates a connection to both MariaDB (which holds concentration data) and Elasticsearch (which holds metadata and user data).

        :param elasticsearch_config - Elasticsearch-related configuration from the config file
        :param elasticsearch_host - Holds connection details (host, connection scheme, port) for Elasticsearch - refer to API
        :param mariadb_config - MariaDB-related configuration from the config file
        :param mariadb_host - Path to the MariaDB instance
        :param docker_bool - True if the app runs on docker, False otherwise
        :param admin_pw - Admin password (taken from configuration file)
        :param source_folder: Folder for experimental data
        :param upload_folder: Folder for temp. uploads
        :param smtp_config: Configuration for the functional email associated with MetaboSERV
        """
        self.es_connection_uri = elasticsearch_host
        es_username = elasticsearch_config['username']
        es_password = elasticsearch_config['password']

        self.md_connection_uri = mariadb_host
        self.md_username = mariadb_config['username']
        self.md_password = mariadb_config['password']
        self.md_database = mariadb_config['database']

        self.biospecimen = ['serum', 'blood', 'plasma', 'urine', 'feces', 'csf', 'other', 'multiple', 'unknown']
        self.source_folder = source_folder
        self.upload_folder = upload_folder
        self.smtp_config = smtp_config

        self.hmdb_regex = re.compile("HMDB[0-9]{7}")

        try:
            if docker_bool:
                time.sleep(30)
            self.es_is_connected = self.connect_elasticsearch(es_username, es_password)
            if not self.user_check_exists('admin'):
                self.user_create('admin', admin_pw, email="none", institution="none", admin=True)
        except:
            logging.error(
                f"Failed to connect to ES with username {es_username} and password {es_password} ({self.es_connection_uri})"
            )

    def connect_elasticsearch(self, username: str, password: str) -> bool:
        """
        Connects to ElasticSearch.
        The mode of connection varies slightly depending on whether Docker is used or not.

        :param username: The ES username
        :param password: The ES password
        :returns True if connection established, False otherwise
        """
        try:
            self.es_client = Elasticsearch(
                self.es_connection_uri, basic_auth=(username, password))
            logging.info("Successfully connected to the Elasticsearch instance.")
            return True
        except AuthenticationException:
            logging.error("Failed connecting to the ElasticSearch database due to an authentication error (401).")
            return False
        except AuthorizationException:
            logging.error("Authorization exception occurred somewhere in the login process. (403)")
            return False
        except Exception as e:
            logging.error("Failed connecting to the ElasticSearch database with unknown error.")
            logging.error(e)
            return False
        
    def create_mariadb_connection(self) -> bool:
        """
        Connects to MariaDB.
        The mode of connection varies slightly depending on whether Docker is used or not.

        :returns Connection
        """
        try:
            md_connection = mariadb.connect(
                user = self.md_username,
                password = self.md_password,
                host = self.md_connection_uri.split(':')[0],
                port = int(self.md_connection_uri.split(':')[1]),
                database = self.md_database,
                autocommit=True
            )
            return md_connection
        except mariadb.Error as e:
            logging.error(f"Failed connecting to the MariaDB instance with the following error: {e}")
            return None

    ################### Elasticsearch #####################
                
    def elasticsearch_initialize_db(self, admin_password: str) -> ResultObject:
        """
        Initializes the Elasticsearch database by creating all the necessary indices (if they don't already exist).
        Also creates the 'admin' account that can be used for administrating MetaboSERV.
        :param admin_password - The password to use for the administrator account
        """
        try:
            for index_key in INDICES.keys():
                self.elasticsearch_create_index(INDICES[index_key], MAPPINGS[index_key])
            self.user_create('admin', admin_password, email="none", institution="none", admin=True)
            return ResultObject(msg="Successfully initialized the Elasticsearch database.")
        except Exception as e:
            logging.error(f"Failed initializing the Elasticsearch database, with the following error: {e}")
            return ResultObject(msg=f"Failed initializing the Elasticsearch database, with the following error: {e}", status=500)

    def elasticsearch_reset_db(self, admin_password: str) -> ResultObject:
        """
        Resets the Elasticsearch database by cleaning all records (keeps indices).
        :param admin_password - The password to use for the administrator account
        """
        try:
            for index_key in INDICES.keys():
                self.elasticsearch_delete_index(INDICES[index_key])
            for index_key in INDICES.keys():
                self.elasticsearch_create_index(INDICES[index_key], MAPPINGS[index_key])
            self.user_create('admin', admin_password, email="none", institution="none", admin=True)
            return ResultObject(msg="Successfully reset the Elasticsearch database.")
        except Exception as e:
            logging.error(f"Failed resetting the Elasticsearch database, with the following error: {e}")
            return ResultObject(msg=f"Failed resetting the Elasticsearch database, with the following error: {e}", status=500)

    def elasticsearch_destroy_db(self) -> ResultObject:
        """
        Completely clears the Elasticsearch database by deleting all indices.
        """
        try:
            for index_key in INDICES.keys():
                self.elasticsearch_delete_index(INDICES[index_key])
            return ResultObject(msg="Successfully deleted all indices and data in the Elasticsearch database.")
        except Exception as e:
            logging.error(f"Failed destroyed the Elasticsearch database, with the following error: {e}")
            return ResultObject(msg=f"Failed destroyed the Elasticsearch database, with the following error: {e}", status=500)
                
    def elasticsearch_create_index(self, index: str, mapping: dict) -> ResultObject:
        """
        Creates an Elasticsearch index, if it does not already exist.
        :param index - Index to create
        :param mapping - Mapping for the index
        """
        try:
            if self.es_client.indices.exists(index=index):
                return ResultObject(msg=f"Index {index} already exists!", status=409)
            self.es_client.indices.create(index=index, mappings=mapping)
            self.es_client.indices.put_settings(index=index, body={"index.mapping.total_fields.limit": 10000})
            return ResultObject(msg=f"Successfully created the index {index}.")
        except Exception as e:
            logging.error(f"Failed creating the index {index}, with the following error: {e}")
            return ResultObject(msg=f"Failed creating the index {index}, with the following error: {e}", status=500)

    def elasticsearch_delete_index(self, index: str) -> ResultObject:
        """
        Deletes an Elasticsearch index.
        :param index - Index to delete
        """
        try:
            if not self.es_client.indices.exists(index=index):
                return ResultObject(msg=f"Index {index} does not exist, so it can't be deleted!", status=404)
            self.es_client.indices.delete(index=index)
            return ResultObject(msg=f"Successfully deleted the index {index}.")
        except Exception as e:
            logging.error(f"Failed deleting the index {index}, with the following error: {e}")
            return ResultObject(msg=f"Failed deleting the index {index}, with the following error: {e}", status=500)

    def elasticsearch_get_index_information(self, index: str) -> ResultObject:
        """
        Returns information on an Elasticsearch index.
        :param index - Index to retrieve information on
        """
        try:
            if not self.es_client.indices.exists(index=index):
                return ResultObject(msg=f"Index {index} does not exist!", status=404)
            return ResultObject(body=self.es_client.indices.get(index=index))
        except Exception as e:
            logging.error(f"Failed retrieving Elasticsearch index information, with the following error: {e}")
            return ResultObject(msg=f"Encountered an error during index information retrieval: {e}", status=500)

    def elasticsearch_record_delete(self, index: str, record_id: str) -> ResultObject:
        """
        Deletes any Elasticsearch record in a given index.
        :param index - Index the record lives in
        :param record_id - ID of the record to delete
        """
        try:
            if not self.es_client.exists(index=index, id=record_id):
                return ResultObject(msg=f"The record {index}:{record_id} does not exist, so it can't be deleted!", status=404)
            self.es_client.delete(index=index, id=record_id)
            return ResultObject(msg=f"Successfully deleted the record {index}:{record_id}.")
        except Exception as e:
            logging.error(f"Failed deleting the record {index}:{record_id}, with the following error: {e}")
            return ResultObject(msg=f"Encountered an error during record deletion: {e}", status=500)

    def elasticsearch_record_delete_by_index(self, index: str) -> ResultObject:
        """
        Deletes all records in a particular Elasticsearch index.
        :param index - The index to delete records in.
        """
        try:
            if not self.es_client.indices.exists(index=index):
                return ResultObject(msg=f"The index '{index}' does not exist!", status=404)
            self.es_client.delete_by_query(index=index, body={"query": {"match_all": {}}})
            return ResultObject(msg=f"Successfully deleted all records in index '{index}'.")
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an error during record deletion: {e}", status=500)

    def elasticsearch_get_any_record(self, index: str, record_id: str) -> ResultObject:
        """
        Retrieves an arbitrary Elasticsearch record.
        :param index - The index the record lives in
        :param record_id - ID of the record
        """
        try:
            if not self.es_client.exists(index=index, id=record_id):
                return ResultObject(f"The record {index}:{record_id} does not exist!", status=404)
            return ResultObject(body=self.es_client.get(index=index, id=record_id))
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an error during record retrieval: {e}", status=500)

    ################### MariaDB #####################

    def mariadb_add_nconcs(self, compound_dict: dict) -> ResultObject:
        """
        Extracts normal concentrations from a dictionary and adds them to the MariaDB instance.
        Currently only normal concentrations with a unit of mmol/L are considered.
        Update: Now also adds an entry into the hmdb_map table.
        :param compound_dict - Dictionary containing compound data and normal concentrations
        """
        try:
            md_connection = self.create_mariadb_connection()
            cursor = md_connection.cursor()
            for compound in compound_dict.keys():
                try:
                    cursor.execute("INSERT INTO hmdb_map (hmdb_id, compound_name, compound_id_adapted) VALUES(?, ?, ?)",
                        (compound_dict[compound]['compound_id'], compound_dict[compound]['compound_name'], adapt_compound_for_mariadb(compound_dict[compound]['compound_name'])))
                except mariadb.Error as e:
                        logging.warning(f"Exception while trying to add a name for HMDB: {e}")
                nconcs = compound_dict[compound]['normal_concentrations']
                if not nconcs or len(nconcs) == 0: continue
                for sn in nconcs:
                    try:
                        #if sn['unit'].lower() == 'mmol/l':
                        cursor.execute("INSERT INTO nconcs (compound_id, compound_name, nconc_min, nconc_max, nconc_unit, nconc_state, nconc_source, nconc_age, nconc_sex, nconc_ref, nconc_biospecimen) \
                                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                            (compound_dict[compound]['compound_id'], compound_dict[compound]['compound_name'], sn['min'], sn['max'], sn['unit'], sn['condition'], 'HMDB' if 'source' not in sn.keys() else sn['source'],
                                sn['age'], sn['sex'], sn['ref'], sn['biospecimen'])
                        )
                    except mariadb.Error as e:
                        logging.error(f"Error while trying to add a single normal concentration: {e}")
                        continue
            md_connection.commit()
            cursor.close()
            md_connection.close()
            return ResultObject(msg="Successfully added normal concentrations to the MariaDB instance.")
        except mariadb.Error as e:
            logging.error(f"Encountered an error while adding normal concentrations to MariaDB: {e}")
            return ResultObject(msg=f"Encountered an error while adding normal concentrations to MariaDB: {e}", status=500)                    

    def mariadb_drop_table(self, table_id: str) -> ResultObject:
        """
        Drops a MariaDB table (if it exists).
        :param table_id - ID of the table to drop
        """
        try:
            md_connection = self.create_mariadb_connection()
            cursor = md_connection.cursor()
            cursor.execute(f"DROP TABLE IF EXISTS {table_id}")
            cursor.execute("DELETE FROM studies WHERE study_id=?", (table_id,))
            md_connection.commit()
            cursor.close()
            md_connection.close()
            return ResultObject(msg=f"Table {table_id} dropped successfully.")
        except mariadb.Error as e:
            logging.error(f"Encountered an error while dropping a MariaDB table: {e}")
            return ResultObject(msg=f"Encountered an error while dropping a MariaDB table: {e}", status=500)

    def mariadb_truncate_table(self, table_id: str) -> ResultObject:
        """
        Truncates a MariaDB table (if it exists).
        This translates to deletion of all records inside the table.
        :param table_id - ID of the table to drop
        """
        try:
            md_connection = self.create_mariadb_connection()
            cursor = md_connection.cursor()
            cursor.execute(f"TRUNCATE TABLE {table_id}")
            cursor.close()
            md_connection.commit()
            md_connection.close()
            return ResultObject(msg=f"Table {table_id} truncated successfully.")
        except mariadb.Error as e:
            logging.error(f"Encountered an error while truncating a MariaDB table: {e}")
            return ResultObject(msg=f"Encountered an error while truncating a MariaDB table: {e}", status=500)
    
    def mariadb_reset_db(self) -> ResultObject:
        """
        Resets the MariaDB instance, keeping table schema.
        """
        try:
            md_connection = self.create_mariadb_connection()
            cursor = md_connection.cursor()
            cursor.execute(f"SELECT study_id FROM studies")
            studies = cursor.fetchall()
            for (study,) in studies:
                cursor.execute(f"DROP TABLE IF EXISTS {study}")
            md_connection.commit()
            cursor.close()
            md_connection.close()
            self.mariadb_truncate_table("nconcs")
            self.mariadb_truncate_table("studies")
            return ResultObject(msg="MariaDB reset successfully.")
        except mariadb.Error as e:
            logging.error(f"Encountered an error while resetting MariaDB instance: {e}")
            return ResultObject(msg=f"Encountered an error while resetting MariaDB instance: {e}", status=500)
        
    def mariadb_remove_compound_entries(self, study_id: str) -> ResultObject:
        """
        Removes all compound name entries that are associated with a certain study.
        Generally to be used when a study is deleted.
        :param study_id - The study in question
        """
        try:
            md_connection = self.create_mariadb_connection()
            cursor = md_connection.cursor()
            query = "DELETE FROM compound_map WHERE study_id=?"
            cursor.execute(query, (study_id,))
            md_connection.commit()
            cursor.close()
            md_connection.close()
            self.mariadb_truncate_table("nconcs")
            self.mariadb_truncate_table("studies")
            return ResultObject(msg="MariaDB reset successfully.")
        except mariadb.Error as e:
            logging.error(f"Encountered an error while resetting MariaDB instance: {e}")
            return ResultObject(msg=f"Encountered an error while resetting MariaDB instance: {e}", status=500)


    ################### Study-related #####################
                
    def study_create(self, study_id: str, study_type: str, study_specimen: str, study_name: str, study_date: str, concentration_data: dict,
            compounds: list, concentration_metadata: dict, author: str, visibility='private', metadata=None, phenotypes=None, units=None, auth_user=None) -> ResultObject:
        """
        Creates a new study. Adds a new study document in Elasticsearch, as well as saving the concentration data in MariaDB.
        :param study_id - ID of the study to create
        :param study_type - Type of the study to create (e.g. NMR)
        :param study_specimen - Biospecimen of the study
        :param study_name - Name of the study to create (may be different from ID)
        :param study_date - String indicating the study date
        :param concentration_data - Dictionary of concentration entries (read in API)
        :param compounds - List of compounds in the study
        :param concentration_metadata - Metadata about the singular sources in the study
        :param author - Author(s) of the study
        :param visibility - Either 'public' or 'private'.
        :param metadata - Dictionary with metadata. Should at least contain an author field and a privacy value.
        :param phenotypes - Optional phenotype information on the study.
        :param units - Dictionary containing compound IDs and associated units (if not present, mmol/L is assumed)
        :param auth_user - User uploading the study
        """
        if not auth_user: return ResultObject(msg="You must be logged in to create a study!", status=401)
        if self.study_check_exists(study_id): return ResultObject(msg=f"Study {study_id} already exists!", status=409)
        if study_id.lower() in FORBIDDEN_IDS or '_SOURCE_' in study_id: return ResultObject(msg=f"Sorry - this study name represents a forbidden keyword!", status=400)
        if ';' in study_id: return ResultObject(msg=f"Study identifiers may not contain semicolons!", status=400)
        try:
            compounds = self.validate_compounds(compounds)
            study_specimen = self.validate_biospecimen(study_specimen)
            md_connection = self.create_mariadb_connection()

            phenotype_levels = None
            if phenotypes:
                phenotype_data, phenotype_levels, phenotype_metadata = phenotypes
                self.phenotype_create(study_id, phenotype_data, phenotype_levels, phenotype_metadata)
            
            # Create main study document
            self.es_client.index(index=INDICES["STUDY"], id=study_id, document = {
                "study_id": study_id,
                "study_name": study_name,
                "study_type": study_type,
                "study_date": study_date,
                "study_specimen": study_specimen,
                "study_author": author,
                "study_visibility": visibility,
                "study_compounds": compounds,
                "study_phenotypes": list(phenotype_levels.keys()) if phenotype_levels else [],
                "study_metadata": metadata,
                "study_members": {
                    auth_user.username: "uploader",
                },
                "study_auth_tokens": {},
            }, refresh='true')
            
            # Check if user is verified (no studies can be uploaded otherwise)
            user_info = self.user_internal_get_information(auth_user.username).getBody()
            if user_info['verified'] != "YES":
                return ResultObject(msg="Sorry, your account must be verified in order to upload a study.", status=401)

            # Add study permissions to the uploader
            self.es_client.update(index=INDICES["USER"], id=auth_user.username, body={"script":{
                "source": "ctx._source.studies[params.study_id]='uploader'",
                "lang": "painless",
                "params": {
                    "study_id": study_id,
                }
            }}, refresh='true')

            # Create metadata entry for each source in the study
            if concentration_metadata:
                for source_entry in concentration_metadata.keys():
                    self.es_client.index(index=INDICES["SOURCE"], id=study_id + '_SOURCE_' + str(source_entry), body={
                        "source_id": str(source_entry),
                        "source_study": study_id,
                        "source_metadata": concentration_metadata[source_entry]
                    })
                self.es_client.indices.refresh(index=INDICES["SOURCE"])

            # Create unit map for this study
            if units:
                self.es_client.index(index=INDICES["UNIT"], id=study_id, body={
                    "study_id": study_id,
                    "unit_map": units,
                })

            # Add concentration data to MariaDB
            cursor = md_connection.cursor()
            cursor.execute("INSERT INTO studies (study_id, study_type, study_specimen, study_name) VALUES(?, ?, ?, ?)", (study_id, study_type, study_specimen, study_name))
            if concentration_data and compounds:
                query = f"CREATE TABLE IF NOT EXISTS {study_id} (source_id VARCHAR(255) PRIMARY KEY, {', '.join([f'{compound} FLOAT' for compound in compounds])})"
                cursor.execute(query)
                # Sort compounds alphabetically
                compounds.sort()
                columns = ', '.join(compounds)
                for source_entry in concentration_data.keys():
                    if str(source_entry).startswith('PROCESSING') or source_entry.lower() in ['unit', 'units']: continue
                    values = ', '.join([concentration_data[source_entry][k] for k in compounds])
                    query = f"INSERT INTO {study_id} (source_id, %s) VALUES ('{str(source_entry)}', %s)" % (columns, values)
                    cursor.execute(query)
            cursor.close()
            md_connection.commit()
            md_connection.close()
            return ResultObject(msg=f"Study {study_id} created successfully!")
        except mariadb.Error as e:
            self.study_delete(study_id=study_id, auth_user=auth_user)
            logging.error(f"Encountered a MariaDB error while creating a study: {e}")
            return ResultObject(msg=f"Encountered a MariaDB error while creating a study: {e}", status=500)
        except Exception as e:
            self.study_delete(study_id=study_id, auth_user=auth_user)
            logging.error(f"Encountered a general error while creating a study: {e}")
            return ResultObject(msg=f"Encountered a general error while creating a study: {e}", status=500)

    def study_delete(self, study_id: str, auth_user=None) -> ResultObject:
        """
        Deletes an existing study.
        Also removes any associated experimental data, and any links to this study for other users.
        :param study_id - ID of the study to delete
        :param auth_user - The user deleting the study
        """
        try:
            if not self.study_check_exists(study_id):
                return ResultObject(msg=f"Study {study_id} does not exist!", status=404)
            if not self.user_check_authorization(auth_user, study_id, required_permission="uploader"):
                return ResultObject(msg=f"You lack the necessary permissions to delete the study {study_id}!", status=401)
            self.mariadb_drop_table(study_id)
            self.mariadb_remove_compound_entries(study_id)
            try:
                study_data = self.study_get_internal(study_id).getBody()
                if "study_members" in study_data.keys():
                    for user in study_data["study_members"].keys():
                        self.user_unlink(user, study_id)
                self.es_client.delete(index=INDICES["STUDY"], id=study_id)
                self.es_client.delete(index=INDICES["PHENOTYPE"], id=study_id)
                self.es_client.delete(index=INDICES["UNIT"], id=study_id)
                self.es_client.delete_by_query(index=INDICES["SOURCE"], body={"query": {"match": {"source_study": study_id}}})
                self.es_client.indices.refresh(index=INDICES["STUDY"])
                self.es_client.indices.refresh(index=INDICES["UNIT"])
                self.es_client.indices.refresh(index=INDICES["USER"])
                if (os.path.exists(os.path.join(self.source_folder, study_id)) and os.path.isdir(os.path.join(self.source_folder, study_id))):
                    shutil.rmtree(os.path.join(self.source_folder, study_id))
            except NotFoundError:
                pass
            return ResultObject(msg=f"Study {study_id} successfully deleted.")
        except Exception as e:
            logging.error(f"Encountered an error while deleting a study: {e}")
            return ResultObject(msg=f"Encountered an error while deleting a study: {e}", status=500)

    def study_get(self, study_id: str, auth_user=None, auth_token=None) -> ResultObject:
        """
        Retrieves information about an existing study.
        :param study_id - ID of the study to retrieve information about
        :param auth_user - The user retrieving the data
        :param auth_token - Token that may be used for the action
        """
        try:
            if not self.study_check_exists(study_id): return ResultObject(msg=f"Study {study_id} does not exist!", status=404)
            user_check = self.user_check_authorization(auth_user, study_id, required_permission="viewer")
            token_check = self.token_check_authorization(auth_token, study_id, required_permission="viewer")
            if not user_check and not token_check:
                return ResultObject(msg=f"You are not authorized to access the study {study_id}", status=401)
            return ResultObject(body=self.es_client.get(index=INDICES["STUDY"], id=study_id)['_source'])
        except Exception as e:
            logging.error(f"Encountered an error while retrieving a study: {e}")
            return ResultObject(msg=f"Encountered an error while retrieving a study: {e}", status=500)
        
    def study_get_internal(self, study_id: str) -> ResultObject:
        """
        Retrieves information about an existing study.
        Internal method that is not exposed to the API - used for internal purposes!
        :param study_id - ID of the study to retrieve information about
        """
        try:
            if not self.study_check_exists(study_id): return ResultObject(msg=f"Study {study_id} does not exist!", status=404)
            return ResultObject(body=self.es_client.get(index=INDICES["STUDY"], id=study_id)['_source'])
        except NotFoundError:
            return ResultObject(msg=f"Study {study_id} does not exist!", status=404)
        except Exception as e:
            logging.error(f"Encountered an error while retrieving a study (internal): {e}")
            return ResultObject(msg=f"Encountered an error while retrieving a study (internal): {e}", status=500)
        
    def study_check_exists(self, study_id: str) -> bool:
        """
        Checks if a study exists already (in which case it returns true).
        :param study_id - ID of the study to check
        """
        try:
            if not study_id: return False
            md_connection = self.create_mariadb_connection()
            cursor = md_connection.cursor()
            cursor.execute("SELECT study_id FROM studies WHERE study_id=?", (study_id,))
            row = cursor.fetchone()
            cursor.close()
            md_connection.close()
            study_in_es = self.record_exists_in_es(index=INDICES["STUDY"], record_id=study_id)
            return (study_in_es or (row!=None))
        except Exception as e:
            logging.error(f"Failed checking if a study exists with the following error: {e}")

    def study_update_metadata(self, study_id: str, study_metadata: dict, auth_user=None, auth_token=None) -> ResultObject:
        """
        Updates metadata associated with a study.
        Metadata is solely kept in Elasticsearch.
        :param study_id - ID of the study to update
        :param study_metadata - Metadata dict
        :param auth_user - The user updating metadata for a study
        :param auth_token - Token that may be used to perform the action instead
        """
        try:
            if not self.study_check_exists(study_id): return ResultObject(msg=f"Study {study_id} does not exist!", status=404)
            token_check = self.token_check_authorization(auth_token, study_id, required_permission="contributor")
            user_check = self.user_check_authorization(auth_user, study_id, required_permission="contributor")
            if not token_check and not user_check:
                return ResultObject(msg="You lack the necessary permissions to update metadata for this study!", status=401)
            self.es_client.update(index=INDICES["STUDY"], id=study_id, body={"doc": {"study_metadata": study_metadata}})
            self.es_client.indices.refresh(index=INDICES["STUDY"])
            return ResultObject(msg=f"Successfully updated study {study_id}.")
        except Exception as e:
            logging.error(f"Failed updating study metadata with the following error: {e}")
            return ResultObject(msg=f"Failed updating study metadata with the following error: {e}", status=500)
        
    def study_update(self, study_id: str, data: dict, auth_user=None, auth_token=None) -> ResultObject:
        """
        Catch-all study update method. Update data must be passed in a dict.
        :param study_id - ID of the study to update
        :param data - Update data (key-value pairs)
        :param auth_user - The user updating metadata for a study
        :param auth_token - Token that may be used to perform the action instead
        """
        try:
            if not self.study_check_exists(study_id): return ResultObject(msg=f"Study {study_id} does not exist!", status=404)
            token_check = self.token_check_authorization(auth_token, study_id, required_permission="contributor")
            user_check = self.user_check_authorization(auth_user, study_id, required_permission="contributor")
            if not token_check and not user_check:
                return ResultObject(msg="You lack the necessary permissions to update this study!", status=401)
            self.es_client.update(index=INDICES["STUDY"], id=study_id, body={"doc": data})
            self.es_client.indices.refresh(index=INDICES["STUDY"])
            return ResultObject(msg=f"Successfully updated study {study_id}.")
        except Exception as e:
            logging.error(f"Failed updating study with the following error: {e}")
            return ResultObject(msg=f"Failed updating study with the following error: {e}", status=500)

    def study_list_all(self, auth_user=None, auth_token=None) -> ResultObject:
        """
        Returns a list of all studies, as long as the necessary permissions are present.
        :param auth_user - The user trying to get a list of studies (public and private)
        :param auth_token - Token that may be used to obtain permissions
        """
        try:
            study_scan = scan(self.es_client, index=INDICES["STUDY"], query={"query": {"match_all": {}}})
            study_dict = {}

            for k in study_scan:
                study_details = k["_source"]
                if study_details['study_visibility'] == 'private':
                    if not auth_user and not auth_token: continue
                    auth_check_user = self.user_check_authorization(auth_user, k["_id"], required_permission="viewer")
                    auth_check_token = self.token_check_authorization(auth_token, k["_id"], required_permission="viewer")
                    if not auth_check_user and not auth_check_token: continue
                        
                    study_dict[k["_id"]] = study_details
                else:
                    study_dict[k["_id"]] = study_details
            return ResultObject(body=study_dict)
        except Exception as e:
            logging.error(f"Failed to retrieve study list with error {e}.")
            return ResultObject(msg=f"Failed to retrieve study list with error {e}.", status=500)

    def study_list_private(self, auth_user=None, auth_token=None) -> ResultObject:
        """
        Returns a list of all accessible private studies.
        :param auth_user - The user trying to get a list of private (shared and uploaded) studies
        :param auth_token - Token that may be used to obtain permissions
        """
        try:
            study_scan = scan(self.es_client, index=INDICES["STUDY"], query={"query": {"match_all": {}}})
            study_dict = {}

            for k in study_scan:
                study_details = k["_source"]
                if study_details['study_visibility'] == 'private':
                    if not auth_user and not auth_token: continue
                    auth_check_user = self.user_check_authorization(auth_user, k["_id"], required_permission="viewer")
                    auth_check_token = self.token_check_authorization(auth_token, k["_id"], required_permission="viewer")
                    if not auth_check_token and not auth_check_user: continue
                    study_dict[k["_id"]] = study_details
            return ResultObject(body=study_dict)
        except Exception as e:
            return ResultObject(msg="Failed to retrieve study list.", status=500)

    def study_set_visibility(self, study_id: str, visibility="private", auth_user=None) -> ResultObject:
        """
        Changes visibility for a study. It can be either private or public.
        Only the uploader of a study can perform this action.
        :param study_id - ID of the study to update
        :param visibility - Visibility to set
        :param auth_user - The user performing the update
        """
        try:
            if not self.study_check_exists(study_id): return ResultObject(msg=f"Study {study_id} does not exist!", status=404)
            if not self.user_check_authorization(auth_user, study_id, required_permission="uploader"):
                return ResultObject(msg="You lack the necessary permissions to change visibility of this study!", status=401)
            if visibility not in ["private", "public"]:
                return ResultObject(msg="Invalid visibility value!", status=400)
            self.es_client.update(index=INDICES["STUDY"], id=study_id, body={"doc": {"study_visibility": visibility}})
            self.es_client.indices.refresh(index=INDICES["STUDY"])
            return ResultObject(msg=f"Successfully updated visibility for study {study_id}.")
        except Exception as e:
            logging.error(f"Failed updating study visibility with the following error: {e}")
            return ResultObject(msg=f"Failed updating study visibility with the following error: {e}", status=500)

    def study_get_concentration(self, study_id: str, compounds=None, phenotype_filter=None, auth_user=None, auth_token=None) -> ResultObject:
        """
        Retrieves concentration data about a particular study.
        Also retrieves phenotype data.
        :param study_id - The study in question
        :param compounds - Optional list of compounds for pre-filtering
        :param pfilter - Optional added filters for compound ranges
        :param phenotype_filter - Optional added filters for nominal phenotypes
        :param auth_user - The user trying to retrieve concentration data
        :param auth_token - Authorization token that may be used to retrieve the data
        """
        try:
            user_check = self.user_check_authorization(auth_user, study_id, required_permission="viewer")
            token_check = self.token_check_authorization(auth_token, study_id, required_permission="viewer")
            if not user_check and not token_check:
                return ResultObject(body={}, msg="Necessary permissions to retrieve this data not met!", status=401)
            md_connection = self.create_mariadb_connection()
            cursor = md_connection.cursor()
            if compounds and isinstance(compounds, list):
                study_compounds = self.study_get_compounds([study_id], auth_user=auth_user, auth_token=auth_token).getBody()
                compounds = list(filter(lambda c: c in study_compounds.keys(), compounds))
                if compounds:
                    query = f"SELECT source_id, {', '.join([f'{adapt_compound_for_mariadb(compound)}' for compound in compounds])} FROM {study_id}"
                    cursor.execute(query)
            else:
                cursor.execute(f"SELECT * FROM {study_id}")
            try:
                rows = cursor.fetchall()
            except Exception as e: # Empty cursor result set
                return ResultObject(body={}, msg="Empty result set for this study!", status=404)
            results = {}
            headers = []
            retrieve_names = []
            for desc_tuple in cursor.description:
                if desc_tuple[0].startswith('_') or re.match(self.hmdb_regex, desc_tuple[0]):
                    retrieve_names.append(desc_tuple[0])

            compound_name_map = self.compound_retrieve_names(retrieve_names, study_id).getBody()

            for desc_tuple in cursor.description:
                headers.append(compound_name_map[desc_tuple[0]] if desc_tuple[0] in compound_name_map.keys() else desc_tuple[0])

            for row_tuple in rows:
                temp_dict = {}
                for i, conc_value in enumerate(row_tuple):
                    if i == 0: continue
                    temp_dict[headers[i]] = conc_value
                    if str(headers[i]) not in ['id', 'source_id', 'study_id'] and str(headers[i]) not in retrieve_names:
                        retrieve_names.append(str(headers[i]))
                results[row_tuple[0]] = temp_dict
            cursor.close()
            md_connection.close()
            results = self.study_filter_concentration_by_phenotype(results, study_id, phenotype_filter, auth_user, auth_token)
            return ResultObject(body=[results, compound_name_map])
        except Exception as e:
            logging.error(f"Encountered an error during concentration data retrieval: {e}")
            return ResultObject(msg=f"Encountered an error during concentration data retrieval: {e}", status=500)
        
    def study_filter_concentration_by_phenotype(self, concentration: dict, study_id: str, phenotype_filters=None, auth_user=None, auth_token=None) -> dict:
        """
        Adds phenotype data to the concentration dictionary and also performs filtering based on selected levels.
        Entries that don't satisfy the filter conditions are removed.
        :param concentration - Concentration data dictionary
        :param study_id - ID of the study in question
        :param phenotype_filters - List containing phenotype filters ({phenotype, include, included_levels?})
        """
        try:
            to_delete = []
            phenotype_data = self.phenotype_get(study_id, auth_user, auth_token).getBody()
            if phenotype_data:
                phenotype_data = phenotype_data['phenotype_data']
            else:
                return concentration

            for source in concentration.keys():
                if source in phenotype_data.keys():
                    for phenotype_key in phenotype_data[source].keys():
                        concentration[source]['PHENOTYPE_'+phenotype_key] = phenotype_data[source][phenotype_key]
                    if phenotype_filters:
                        for phen_filter in phenotype_filters:
                            if (phen_filter['phenotype']) in phenotype_data[source].keys():
                                if not phen_filter['include']:
                                    del concentration[source]['PHENOTYPE_'+phen_filter['phenotype']]
                                if 'included_levels' in phen_filter.keys() and isinstance(phen_filter['included_levels'], list):
                                    if phenotype_data[source][phen_filter['phenotype']] not in phen_filter['included_levels']:
                                        to_delete.append(source)
                                        continue
            for source_del in to_delete:
                if source_del in concentration.keys():
                    del concentration[source_del]
            return concentration
        except Exception as e:
            logging.error(f"Encountered an error while filtering a concentration dictionary by phenotypes: {e}")
            return concentration


    def study_filter_concentration(self, concentration: dict, filter=None) -> dict:
        """
        Adds phenotype data.
        Then filters a concentration dictionary by the provided numerical or nominal phenotype filters.
        Entries that don't satisfy the filter conditions are removed.
        :param concentration - Concentration data dictionary
        :param study_id - ID of the study in question
        :param filter: Filter list containing filter objects ({compound_id, filter_type, min, max?})
        """
        try:
            if not filter: return concentration
            
            for st in concentration.keys():
                to_delete = []
                for source in concentration[st].keys():
                    if source not in to_delete and filter is not None:
                        for filter_entry in filter:
                            if source in to_delete: continue
                            compound = filter_entry['compound_id']
                            filter_type = filter_entry['filter_type']
                            min = float(filter_entry['min'])
                            max = float(filter_entry['max']) if 'max' in filter_entry.keys() else None
                            if compound not in concentration[st][source].keys() or concentration[st][source][compound] is None or concentration[st][source][compound] == "":
                                to_delete.append(source)
                                continue
                            if filter_type=='out':
                                if not max:
                                    if concentration[st][source][compound] > min:
                                        to_delete.append(source)
                                        continue
                                if max and concentration[st][source][compound] > min and concentration[st][source][compound] < max:
                                    to_delete.append(source)
                                    continue
                            else:
                                if max and concentration[st][source][compound] > max:
                                    to_delete.append(source)
                                    continue
                                if min and concentration[st][source][compound] < min:
                                    to_delete.append(source)
                                    continue
                for source_del in to_delete:
                    if source_del in concentration[st].keys():
                        del concentration[st][source_del]
            return concentration
        except Exception as e:
            logging.error(f"Encountered an error while filtering a concentration dictionary: {e}")
            return concentration
        
    def study_get_units(self, studies: list, auth_user=None, auth_token=None) -> ResultObject:
        """
        Retrieves units for one or more studies.
        Return format: {study: {compound: unit}}
        :param studies - One or more studies
        :param auth_user - User performing the action
        :param auth_token - Token that may be used to perform this action
        """
        try:
            if not studies or studies[0] == '': return ResultObject(body={})
            results = {}
            for study in studies:
                results[study] = {}
                user_auth_check = self.user_check_authorization(auth_user, study, required_permission="viewer")
                token_auth_check = self.token_check_authorization(auth_token, study, required_permission="viewer")
                if not user_auth_check and not token_auth_check:
                    continue
                if self.es_client.exists(index=INDICES["UNIT"], id=study):
                    unit_map = self.es_client.get(index=INDICES["UNIT"], id=study)["_source"]
                    compound_list = list(unit_map['unit_map'].keys())
                    compounds_human_readable = self.compound_retrieve_names(compound_list, study).getBody()
                    for compound in unit_map['unit_map'].keys():
                        unit_to_add = unit_map['unit_map'][compound]
                        if type(unit_to_add) == "Tuple":
                            unit_to_add = unit_to_add[0]
                        results[study][compounds_human_readable[compound]] = unit_to_add
            return ResultObject(body=results)
        except Exception as e:
            logging.error(f"Encountered an error while retrieving units: {e}")
            return ResultObject(msg=f"Encountered an error while retrieving units: {e}", status=500)


    def study_get_compounds(self, studies: list, auth_user=None, auth_token=None) -> ResultObject:
        """
        Retrieves the compounds (and only the compounds) that are contained in one or more studies.
        After aggregation, it also adds compound data from the database (e.g. from HMDB).
        :param studies - One or more studies
        :param auth_user - User performing the action
        :param auth_token - Token that may be used to perform this action
        """
        try:
            if not studies or studies[0] == '': return ResultObject(body={})
            results = {}
            for study in studies:
                user_auth_check = self.user_check_authorization(auth_user, study, required_permission="viewer")
                token_auth_check = self.token_check_authorization(auth_token, study, required_permission="viewer")
                if not user_auth_check and not token_auth_check:
                    continue
                study_data = self.study_get_internal(study)
                study_data = study_data.getBody()
                compounds_human_readable = self.compound_retrieve_names(study_data['study_compounds'], study).getBody()
                for compound, compound_hr in compounds_human_readable.items():
                    if compound_hr in results.keys(): continue
                    compound_data = self.compound_get(compound)
                    if compound_data.status == 200:
                        results[compound_hr] = compound_data.getBody()
                    else:
                        results[compound_hr] = {}
            return ResultObject(body=results)
        except Exception as e:
            logging.error(f"Encountered an error while retrieving compounds: {e}")
            return ResultObject(msg=f"Encountered an error while retrieving compounds: {e}", status=500)

    def study_list_contributors(self, study_id: str, auth_user=None, auth_token=None) -> ResultObject:
        """
        Lists the contributors (users that are registered) on a study document.
        :param study_id - The study in question
        :param auth_user - The user performing the action
        :param auth_token - Authorization token that may be used instead
        """
        try:
            if not self.study_check_exists(study_id):
                return ResultObject(msg=f"The study {study_id} was not found!", status=404)
            user_check = self.user_check_authorization(auth_user, study_id, required_permission="viewer")
            token_check = self.token_check_authorization(auth_token, study_id, required_permission="viewer")
            if not user_check and not token_check:
                return ResultObject(msg=f"You do not have the necessary permissions to perform this action!", status=401)
            study_data = self.study_get_internal(study_id).getBody()
            return ResultObject(body=study_data["study_members"])
        except Exception as e:
            logging.error(f"Encountered an error while retrieving contributor list: {e}")
            return ResultObject(msg=f"Encountered an error while retrieving contributor list: {e}", status=500)
            
    def study_list_sources(self, study_id: str, auth_user=None, auth_token=None) -> ResultObject:
        """
        Returns a list of all "sources" (e.g. patients) of a study.
        :param study_id - The study in question
        :param auth_user - User performing the action
        :param auth_token - Authorization token that may be used instead
        """
        try:
            if not self.study_check_exists(study_id):
                return ResultObject(msg=f"The study {study_id} was not found!", status=404)
            user_check = self.user_check_authorization(auth_user, study_id, required_permission="viewer")
            token_check = self.token_check_authorization(auth_token, study_id, required_permission="viewer")
            if not user_check and not token_check:
                return ResultObject(msg=f"You do not have the necessary permissions to perform this action!", status=401)
            md_connection = self.create_mariadb_connection()
            cursor = md_connection.cursor()
            query = f"SELECT source_id FROM ?"
            cursor.execute(query, (study_id,))
            sources = []
            rows = cursor.fetchall()
            for row_tuple in rows:
                sources.append(row_tuple[0])
            cursor.close()
            md_connection.close()
            return ResultObject(body=sources)
        except Exception as e:
            logging.error(f"Encountered an error while retrieving source list: {e}")
            return ResultObject(msg=f"Encountered an error while retrieving source list: {e}", status=500)
        
    def study_list_tokens(self, study_id: str, auth_user=None, auth_token=None) -> ResultObject:
        """
        Returns a list of all tokens associated with a particular study.
        :param study_id - The study in question
        :param auth_user - User performing the action
        :param auth_token - Authorization token that may be used instead
        """
        try:
            if not auth_user and not auth_token: return ResultObject(msg="You must be logged in to retrieve a list of tokens!", status=401)
            token_check = self.token_check_authorization(auth_token, study_id, required_permission="contributor")
            user_check = self.user_check_authorization(auth_user, study_id, required_permission="contributor")
            if not token_check and not user_check:
                return ResultObject(msg="You don't have permissions to access the tokens for this study!", status=401)
            token_list = []
            token_query = {"query": {"match": {"studies": study_id}}}
            self.es_client.indices.refresh(index=INDICES["TOKEN"])
            token_results = scan(self.es_client, index=INDICES["TOKEN"], query=token_query)
            for res in token_results:
                for maybe_study in res['_source']['studies']:
                    if study_id and maybe_study != study_id: continue 
                    token_list.append(res['_source'])
            return ResultObject(body=token_list)

        except Exception as e:
            logging.error(f"Encountered an error while retrieving token list for study: {e}")
            return ResultObject(msg=f"Encountered an error while retrieving token list for study: {e}", status=500)
        
    def study_get_exp_data(self, study_id: str, auth_user=None, auth_token=None) -> ResultObject:
        """
        Returns a list of all available files for download (experimental data).
        Does not retrieve the files themselves, only the filenames.
        :param study_id - The study to retrieve files for.
        """
        try:
            if not self.study_check_exists(study_id):
                return ResultObject(msg=f"The study {study_id} was not found!", status=404)
            user_check = self.user_check_authorization(auth_user, study_id, required_permission="viewer")
            token_check = self.token_check_authorization(auth_token, study_id, required_permission="viewer")
            if not user_check and not token_check:
                return ResultObject(msg=f"You do not have the necessary permissions to perform this action!", status=401)
            if not os.path.isdir(os.path.join(self.source_folder, study_id)):
                return ResultObject(msg="No files found for this study!", status=404)
            return ResultObject(body=os.listdir(os.path.join(self.source_folder, study_id)))
        except Exception as e:
            logging.error(f"Encountered an error while retrieving filenames for study: {e}")
            return ResultObject(msg=f"Encountered an error while retrieving filenames for study: {e}", status=500)
        
    def study_delete_exp_data(self, study_id: str, filename: str) -> ResultObject:
        """
        Deletes an experimental data file.
        :param study_id - The study to delete a file for
        :param filename - Name of the file
        """
        try:
            if not study_id or not filename:
                return ResultObject(msg="Filename and/or study identifier are missing!", status=400)
            if (os.path.exists(os.path.join(self.source_folder, study_id)) and os.path.isfile(os.path.join(self.source_folder, study_id, filename))):
                os.remove(os.path.join(self.source_folder, study_id, filename))
                return ResultObject(msg="Successfully deleted exp. data file!")
            else:
                return ResultObject(msg="File was not found, or you don't have the necessary permissions!", status=404)
        except Exception as e:
            logging.error(f"Encountered an error while deleting exp. data file: {e}")
            return ResultObject(msg=f"Encountered an error while deleting exp. data file: {e}", status=500)


    ################### Compound-related #####################

    def compound_add_singular(self, compound_dict: dict) -> ResultObject:
        """
        Adds a single compount to the Elasticsearch database (or updates it).
        :param compound_dict - Dictionary containing data on a singular compound
        """
        try:
            compound_dict_parsed = {ckey: compound_dict[ckey] for ckey in compound_dict.keys() if ckey != 'normal_concentrations'}
            self.es_client.index(index=INDICES["COMPOUND"], id=compound_dict_parsed['compound_id'], document=compound_dict_parsed)
            return ResultObject(msg=f"Successfully added compound {compound_dict_parsed['compound_id']} to the database.")
        except Exception as e:
            logging.error(f"Failed adding compound with the following error: {e}")
            return ResultObject(msg=f"Failed adding compound {compound_dict_parsed['compound_id']} to the database.", status=500)

    def compound_add_from_dict(self, compounds_dict: dict) -> ResultObject:
        """
        Adds an entire dictionary of compounds to the Elasticsearch database (one by one).
        :param compounds_dict - Dictionary containing a list of compounds and data on said compounds.
        """
        try:
            for compound in compounds_dict.keys():
                result = self.compound_add_singular(compounds_dict[compound])
                if result.status == 500:
                    logging.error(result.msg)
                    continue
            return ResultObject(msg="Successfully added the provided list of compounds to the database.")
        except Exception as e:
            logging.error(f"Failed adding compound list with the following error: {e}")
            return ResultObject(msg=f"Failed adding compound list with the following error: {e}", status=500)
        
    def compound_get(self, compound_id: str) -> ResultObject:
        """
        Retrieves data on a compound from Elasticsearch.
        Compound data is public by default, so no auth/auth is required.
        :param compound_id - ID of the compound in question
        """
        try:
            if not re.match(self.hmdb_regex, compound_id):
                compound_id = self.compound_to_hmdb(compound_id) # Try mapping it to HMDB accession
            if not self.compound_check_exists(compound_id): return ResultObject(msg=f"Compound {compound_id} does not exist!", status=404)
            return ResultObject(body=self.es_client.get(index=INDICES["COMPOUND"], id=compound_id)['_source'])
        except Exception as e:
            logging.error(f"Encountered an error while retrieving a compound: {e}")
            return ResultObject(msg=f"Encountered an error while retrieving a compound: {e}", status=500)
        
    def compound_to_hmdb(self, compound_id: str) -> str:
        """
        Tries to find a HMDB accession for a compound.
        :param compound_id - Compound in question (can be a normal name or an adapted metabolite, which is indicated by starting with an underscore)
        """
        try:
            md_connection = self.create_mariadb_connection()
            cursor = md_connection.cursor()
            if compound_id.startswith('_'): # Adapted ID
                query = "SELECT hmdb_id FROM hmdb_map WHERE compound_id_adapted = ?"
                cursor.execute(query, (compound_id,))
            else: # Normal metabolite name
                query = "SELECT hmdb_id FROM hmdb_map WHERE UPPER(compound_name)=?"
                cursor.execute(query, (compound_id.upper(),))
            compound_res = cursor.fetchone()
            if compound_res:
                return compound_res[0]
            return compound_id
        except Exception as e:
            logging.error(f"Encountered an error while trying to find a HMDB accession for {compound_id}: {e}")
            return compound_id

        
    def compound_check_exists(self, compound_id: str) -> bool:
        """
        Checks whether a compound document exists in Elasticsearch.
        :param compound_id - ID of the compound in question
        """
        try:
            does_exist = self.record_exists_in_es(index=INDICES["COMPOUND"], record_id=compound_id)
            return (bool(does_exist))
        except Exception as e:
            logging.error(f"Failed checking if a compound exists with the following error: {e}")
            return ResultObject(msg=f"Encountered an exception during compound check: {e}", status=500)

    def compound_get_nconcs(self, compound_id: str) -> ResultObject:
        """
        Retrieves normal concentrations for a particular compound, should they exist.
        :param compound_id - The compound in question
        """
        try:
            md_connection = self.create_mariadb_connection()
            cursor = md_connection.cursor()
            if re.match(self.hmdb_regex, compound_id):
                query = "SELECT * FROM nconcs WHERE compound_id=?"
                cursor.execute(query, (compound_id,))
            else:
                query1 = "SELECT hmdb_id, compound_name FROM hmdb_map WHERE UPPER(compound_name) = UPPER(?)"
                cursor.execute(query1, (compound_id.upper(),))
                compound_res = cursor.fetchone()
                if compound_res:
                    query = "SELECT * FROM nconcs WHERE compound_id=? OR UPPER(compound_name)=?"
                    cursor.execute(query, (compound_res[0], compound_res[1].upper(),))
                else:
                    query = "SELECT * FROM nconcs WHERE UPPER(compound_name)=?"
                    cursor.execute(query, (compound_id.upper(),))

            rows = cursor.fetchall()
            results = []
            headers = []
            for desc_tuple in cursor.description:
                headers.append(desc_tuple[0])
            for row_tuple in rows:
                temp_dict = {}
                for i, value in enumerate(row_tuple):
                    temp_dict[headers[i]] = value
                results.append(temp_dict)
            cursor.close()
            md_connection.close()
            return ResultObject(body=results)
        except Exception as e:
            logging.error(f"Encountered an error while retrieving normal concentrations for {compound_id}: {e}")
            return ResultObject(msg=f"Encountered an error while retrieving normal concentrations for {compound_id}: {e}", status=500)
        
    def compound_add_namemap(self, compound_map: dict, study_id: str) -> ResultObject:
        """
        Adds a compound map to MariaDB, which maps internal compound identifiers to more human-readable names.
        Only one compound map is possible for each study, ergo old entries are deleted if this function is called.
        Will later be integrated into an approach that can deal with synonyms better.
        :param compound_map - Map of internal compound names to human-readable names
        :param study_id - Identifier of the study the compound was collected in
        """
        try:
            md_connection = self.create_mariadb_connection()
            cursor = md_connection.cursor()
            cursor.execute("DELETE FROM compound_map WHERE study_id=?", (study_id,))
            for compound_id in compound_map.keys():
                cursor.execute("INSERT INTO compound_map(compound_id, compound_name, study_id) VALUES(?,?,?)", (compound_id, compound_map[compound_id], study_id))
            md_connection.commit()
            cursor.close()
            md_connection.close()
            return ResultObject(msg="Successfully added compound map to database.")
        except Exception as e:
            logging.error(f"Encountered an error while adding compound map to MariaDB: {e}")
            return ResultObject(msg=f"Encountered an error while adding compound map to MariaDB: {e}", status=500)
    
    def compounds_to_hmdb(self, compounds: list) -> ResultObject:
        """
        Attempts to convert a compound or number of compounds into their respective HMDB accession.
        Uses the CTS - The Chemical Translation Service, by Fiehn labs.
        Requires internet access to https://cts.fiehnlab.ucdavis.edu.
        If conversion fails for one or more metabolites, the user will get an error response.
        :param compounds - List of compounds to convert.
        """
        if not compounds: return ResultObject(msg="No compounds were provided!", status=400)
        try:
            results = {}
            md_connection = self.create_mariadb_connection()
            cursor = md_connection.cursor()
            for compound in compounds:

                # Try finding it in database first
                
                query = "SELECT hmdb_id, compound_name FROM hmdb_map WHERE UPPER(compound_name) = UPPER(?)"
                cursor.execute(query, (compound,))
                compound_res = cursor.fetchone()
                if compound_res:
                    results[compound] = {"conf": "database-exact", "res": compound_res[0], "original": compound_res[1]}
                    continue

                # Fuzzy matching
                query = f"SELECT hmdb_id, compound_name FROM hmdb_map WHERE UPPER(compound_name) LIKE UPPER('%%{compound}')"   
                cursor.execute(query)
                compound_res = cursor.fetchall()
                if compound_res:
                    lowest = None
                    chosen = None
                    for (hmdb_id, compound_name) in compound_res:
                        hmdb_num = int(hmdb_id.replace("HMDB",""))
                        if not lowest or lowest > hmdb_num:
                            lowest = hmdb_num
                            chosen = (hmdb_id, compound_name)
                    results[compound] = {"conf": "database-fuzzy", "res": chosen[0], "original": chosen[1]}
                    continue


                # If not in DB, try online
                cmp_response = requests.get(f'https://cts.fiehnlab.ucdavis.edu/rest/convert/Chemical%20Name/Human%20Metabolome%20Database/{str(compound).title()}')
                if cmp_response.status_code >= 200 and cmp_response.status_code <= 299:
                    lowest = None
                    chosen = None
                    for res in cmp_response.json()[0]["results"]:
                        res_num = int(res.replace("HMDB",""))
                        if not lowest or lowest > res_num:
                            lowest = res_num
                            chosen = res
                    results[compound] = {"conf": "online", "res": res}
                else:
                    results[compound] = {"failed": True}
            cursor.close()
            return ResultObject(body=results)
        except Exception as e:
            logging.error(f"Encountered an error while converting a compound to HMDB: {e}")
            return ResultObject(msg=f"Encountered an error while converting a compound to HMDB: {e}", status=500)
        
    def compound_next(self, compound: str, hmdb: str) -> ResultObject:
        try:
            results = {}
            md_connection = self.create_mariadb_connection()
            cursor = md_connection.cursor()
            query = f"SELECT hmdb_id, compound_name FROM hmdb_map WHERE UPPER(compound_name) LIKE UPPER('%%{compound}')"   
            cursor.execute(query)
            compound_res = cursor.fetchall()
            hmdb_higher_than = int(hmdb.replace("HMDB",""))
            if compound_res:
                lowest = None
                chosen = None
                for (hmdb_id, compound_name) in compound_res:
                    hmdb_num = int(hmdb_id.replace("HMDB",""))
                    if hmdb_num > hmdb_higher_than and (not lowest or lowest > hmdb_num):
                        lowest = hmdb_num
                        chosen = (hmdb_id, compound_name)
                results[compound] = {"conf": "database-fuzzy", "res": chosen[0], "original": chosen[1]}
                return ResultObject(body=results)
            else:
                return ResultObject(msg="No next metabolite!", status=404)
        except Exception as e:
            logging.error(f"Encountered an error while retrieving next HMDB metabolite from DB: {e}")
            return ResultObject(msg=f"Encountered an error while retrieving next HMDB metabolite from DB: {e}", status=500)
        
    def compound_previous(self, compound: str, hmdb: str) -> ResultObject:
        try:
            results = {}
            md_connection = self.create_mariadb_connection()
            cursor = md_connection.cursor()
            query = f"SELECT hmdb_id, compound_name FROM hmdb_map WHERE UPPER(compound_name) LIKE UPPER('%%{compound}')"   
            cursor.execute(query)
            compound_res = cursor.fetchall()
            hmdb_lower_than = int(hmdb.replace("HMDB",""))
            if compound_res:
                highest = None
                chosen = None
                for (hmdb_id, compound_name) in compound_res:
                    hmdb_num = int(hmdb_id.replace("HMDB",""))
                    if hmdb_num < hmdb_lower_than and (not highest or highest < hmdb_num):
                        highest = hmdb_num
                        chosen = (hmdb_id, compound_name)
                results[compound] = {"conf": "database-fuzzy", "res": chosen[0], "original": chosen[1]}
                return ResultObject(body=results)
            else:
                return ResultObject(msg="No next metabolite!", status=404)
        except Exception as e:
            logging.error(f"Encountered an error while retrieving next HMDB metabolite from DB: {e}")
            return ResultObject(msg=f"Encountered an error while retrieving next HMDB metabolite from DB: {e}", status=500)

        
    def compound_retrieve_names(self, compound_list: list, study_id: str) -> ResultObject:
        """
        Retrieves a list of human-readable compound names from a study.
        :param compound_list - List of compounds to retrieve names for
        :param study_id - Identifier of the study to retrieve names from
        """
        try:
            if not compound_list or len(compound_list) == 0: return ResultObject(body={})
            md_connection = self.create_mariadb_connection()
            cursor = md_connection.cursor()
            result_dict = {}

            # Distribute compounds into internal + HMDB identifiers
            hmdb_list = []
            int_list = []
            for compound in compound_list:
                if re.match(self.hmdb_regex, compound):
                    hmdb_list.append(compound)
                else:
                    int_list.append(compound)

            # Fetch for internal
            if len(int_list) > 0:
                ln_params = ', '.join(['%s'] * len(int_list))
                query = f"SELECT compound_id, compound_name FROM compound_map WHERE compound_id IN (%s) AND study_id='{study_id}'" % ln_params
                cursor.execute(query, int_list)
                compounds = cursor.fetchall()
                for (compound_id, compound_name) in compounds:
                    result_dict[compound_id] = compound_name

            # Fetch for HMDB identifiers
            if len(hmdb_list) > 0:
                ln_params = ', '.join(['%s'] * len(hmdb_list))
                query = f"SELECT hmdb_id, compound_name FROM hmdb_map WHERE hmdb_id IN (%s)" % ln_params
                cursor.execute(query, hmdb_list)
                compounds = cursor.fetchall()
                for (hmdb_id, compound_name) in compounds:
                    result_dict[hmdb_id] = compound_name
            cursor.close()
            md_connection.close()
            for met in compound_list:
                if met not in result_dict.keys():
                    result_dict[met] = met
            return ResultObject(body=result_dict)
        except Exception as e:
            logging.error(f"Encountered an error while retrieving compound map: {e}")
            return ResultObject(msg=f"Encountered an error while retrieving compound map: {e}", status=500)
        
    def compound_retrieve_metabolites_from_file(self, conc_file: str, transpose: bool = False) -> ResultObject:
        try:
            filetype = 'xlsx' if (conc_file.endswith('.xlsx') or conc_file.endswith('xls')) else ('csv' if conc_file.endswith('.csv') else 'tsv')
            cf_results = parse_concentration_file(conc_file, filetype, transpose)
            metabolites = cf_results.getBody()[3].values()
            return ResultObject(body=list(metabolites))
        except Exception as e:
            logging.error(f"Encountered an error while retrieving metabolites from file: {e}")
            return ResultObject(msg=f"Encountered an error while retrieving metabolites from file: {e}", status=500)
        
    def compound_mapped_as_csv(self, meta_dict: dict) -> ResultObject:
        try:
            if not meta_dict: return ResultObject(msg="No metabolite dict received!", status=400)
            meta_dict_filtered = {metabolite: {'compound_name': metabolite, 'hmdb_id': meta_dict[metabolite]['res']} for metabolite in meta_dict.keys()}
            df = pd.DataFrame.from_dict(meta_dict_filtered, orient='index')
            temp_filename = secrets.token_hex(16) + '.csv'
            df.to_csv(os.path.join(self.upload_folder, temp_filename), index=False, sep=";")
            return ResultObject(body=temp_filename)
        except Exception as e:
            logging.error(f"Encountered an error while converting mapped metabolites to CSV: {e}")
            return ResultObject(msg=f"Encountered an error while converting mapped metabolites to CSV: {e}", status=500)
        
    def compound_mapped_as_json(self, meta_dict: dict) -> ResultObject:
        try:
            if not meta_dict: return ResultObject(msg="No metabolite dict received!", status=400)
            meta_dict_filtered = {metabolite: meta_dict[metabolite]['res'] for metabolite in meta_dict.keys()}
            json_string = json.dumps(meta_dict_filtered)
            temp_filename = secrets.token_hex(16) + '.csv'
            with open(os.path.join(self.upload_folder, temp_filename), 'w') as fw:
                fw.write(json_string)
            return ResultObject(body=temp_filename)
        except Exception as e:
            logging.error(f"Encountered an error while converting mapped metabolites to CSV: {e}")
            return ResultObject(msg=f"Encountered an error while converting mapped metabolites to CSV: {e}", status=500)
        
    def compound_mapped_adapted_file(self, conc_file: str, meta_dict: dict, transpose: bool = False) -> ResultObject:
        try:
            filetype = 'xlsx' if conc_file.endswith('.xlsx') else 'xls' if conc_file.endswith('xls') else 'csv' if conc_file.endswith('.csv') else 'tsv'
            adapted = adapt_conc_file(conc_file, filetype, meta_dict, self.upload_folder, transpose)
            if adapted.status == 200:
                return ResultObject(body=adapted.getBody()) # Filename
            else:
                return ResultObject(msg="Error during adaptation of conc. file!", status=500)
        except Exception as e:
            logging.error(f"Encountered an error while retrieving metabolites from file: {e}")
            return ResultObject(msg=f"Encountered an error while retrieving metabolites from file: {e}", status=500)

    ################### Phenotype-related #####################
        
    def phenotype_create(self, study_id: str, phenotype_data: dict, phenotype_levels: dict, phenotype_metadata: dict) -> ResultObject:
        """
        Creates a phenotype record in Elasticsearch.
        Phenotype-records are *study-specific* to avoid any kind of ID conflict which might easily happen otherwise.
        :param phenotype_data - Data on how the phenotype is distributed across the sources (patients) in the study
        :param phenotype_levels - Levels of each phenotype
        :param phenotype_metadata - Metadata for the sources (patients) regarding the phenotype's collection
        """
        try:
            phenotype_dict = {
                "phenotype_data": phenotype_data,
                "phenotype_metadata": {}, 
                "phenotype_levels": phenotype_levels,
                "phenotype_study": study_id,
            }
            self.es_client.index(index=INDICES["PHENOTYPE"], id=study_id, body=phenotype_dict)
            return ResultObject(msg=f"Successfully created {len(phenotype_levels.keys())} phenotype(s) for study {study_id}!")
        except Exception as e:
            logging.error(f"Encountered an error during phenotype creation: {e}")
            return ResultObject(msg=f"Encountered an error during phenotype creation: {e}", status=500)

    def phenotype_check_exists(self, study_id: str) -> bool:
        """
        Checks if a phenotype record for a particular study already exists.
        :param study_id - The ID in question.
        """
        try:
            does_exist = self.record_exists_in_es(index=INDICES["PHENOTYPE"], record_id=study_id)
            return (bool(does_exist))
        except Exception as e:
            logging.error(f"Failed checking if a phenotype exists with the following error: {e}")
            return ResultObject(msg=f"Encountered an exception during phenotype check: {e}", status=500)

    def phenotype_get(self, study_id: list, auth_user=None, auth_token=None) -> ResultObject:
        """
        Retrieves phenotype information about one or more existing studies.
        :param study_id - ID of the study to retrieve phenotypes about
        :param auth_user - The user retrieving the data
        :param auth_token - Auth. Token that may be used instead of a user account
        """
        try:
            if not self.study_check_exists(study_id): return ResultObject(msg="Study does not exist!", status=404)
            if not self.phenotype_check_exists(study_id): return ResultObject(msg="No phenotypes found for the study.", status=404)
            if not self.user_check_authorization(auth_user, study_id, required_permission="viewer") \
                and not self.token_check_authorization(auth_token, study_id, required_permission="viewer"):
                return ResultObject(msg="You are not authorized to access this phenotype data!", status=401)
            return ResultObject(body=self.es_client.get(index=INDICES["PHENOTYPE"], id=study_id)['_source'])
        except Exception as e:
            logging.error(f"Encountered an error while retrieving a phenotype: {e}")
            return ResultObject(msg=f"Encountered an error while retrieving a phenotype: {e}", status=500)

    def phenotype_get_multiple(self, studies: list, auth_user=None, auth_token=None) -> ResultObject:
        """
        Retrieves multiple phenotypes records at once (one per study) for a list of studies.
        :param studies - List of studies
        :param auth_user - User performing the action
        :param auth_token - Token that may be used instead of a user account
        """
        try:
            if not studies or studies[0] == '': return ResultObject(body={})
            results = {}
            for study in studies:
                results[study] = self.phenotype_get(study, auth_user=auth_user, auth_token=auth_token).getBody()
            return ResultObject(body=results)
        except Exception as e:
            logging.error(f"Encountered an error while retrieving a phenotype: {e}")
            return ResultObject(msg=f"Encountered an error while retrieving a phenotype: {e}", status=500)

    def phenotype_get_information(self, study_id: str, auth_user=None, auth_token=None) -> ResultObject:
        """
        Retrieves phenotype meta-information about an existing study (Names, levels).
        :param study_id - ID of the study to retrieve phenotypes about
        :param auth_user - The user retrieving the data
        :param auth_token - Auth. Token that may be used instead of a user account
        """
        try:
            if not self.study_check_exists(study_id): return ResultObject(msg=f"Study {study_id} does not exist!", status=404)
            if not self.phenotype_check_exists(study_id): return ResultObject(msg=f"Study exists, but no phenotype record can be found!", status=404)
            if not self.user_check_authorization(auth_user, study_id, required_permission="viewer") \
                and not self.token_check_authorization(auth_token, study_id, required_permission="viewer"):
                return ResultObject(msg=f"You are not authorized to access the study {study_id}", status=401)
            phenotype_information = self.es_client.get(index=INDICES["PHENOTYPE"], id=study_id)['_source']['phenotype_levels']
            return ResultObject(body=phenotype_information)
        except Exception as e:
            logging.error(f"Encountered an error while retrieving a phenotype: {e}")
            return ResultObject(msg=f"Encountered an error while retrieving a phenotype: {e}", status=500)

    ################### File handling #####################
            
    def file_validate_type(self, filepath: str) -> ResultObject:
        """
        Validates the type of an uploaded file with pymagic.
        :param filepath - Complete path to the file (generally upload folder + filename)
        """
        try:
            return validate_file_type(filepath)
        except Exception as e:
            logging.error(f"Encountered an error on file validation method handover: {e}")
            return ResultObject(msg=f"Encountered an error on file validation method handover: {e}", status=500)

    def file_read_conc_file(self, filepath: str, filetype: str, transpose=True, enforce_hmdb=False) -> ResultObject:
        """
        Reads a concentration file (given that it conforms to the provided file format).
        :param filepath - Complete path to the file (generally upload folder + filename)
        :param filetype - Filetype as indicated by the extension and validated by pymagic
        :param transpose - Dictates whether to transpose the dataframe
        :param enforce_hmdb - If set to true, only HMDB identifiers will be accepted
        """
        try:
            return parse_concentration_file(filepath, filetype, transpose, unit_parsing="column", enforce_hmdb=enforce_hmdb)
        except Exception as e:
            logging.error(f"Encountered an error on file parsing method handover: {e}")
            return ResultObject(msg=f"Encountered an error on file parsing method handover: {e}", status=500)
        
    def query_results_to_csv(self, query_results: dict) -> ResultObject:
        """
        Turns query results into a CSV file that is then sent back to the endpoint.
        :param query_results - Query results (loaded from JSON representation)
        """
        try:
            if not query_results: return ResultObject(msg="No query result dict received!", status=400)
            final_df = pd.DataFrame
            for study_id in query_results.keys():
                res_dict = {}
                for source_id in query_results[study_id].keys():
                    res_dict[source_id] = {'study_id': study_id}
                    for key in query_results[study_id][source_id].keys():
                        potential_value = query_results[study_id][source_id][key]
                        res_dict[source_id][key] = potential_value if potential_value else np.nan
                if final_df.empty:
                    final_df = pd.DataFrame.from_dict(res_dict).T
                else:
                    final_df = pd.concat([final_df, pd.DataFrame.from_dict(res_dict).T], ignore_index=True)
            temp_filename = secrets.token_hex(16) + '.csv'
            final_df.to_csv(os.path.join(self.upload_folder, temp_filename), index=False, sep=";")
            return ResultObject(body=temp_filename)
        except Exception as e:
            logging.error(f"Encountered an error turning query results into CSV: {e}")
            return ResultObject(msg=f"Encountered an error turning query results into CSV: {e}", status=500)
        
    def delete_temp_file(self, filename: str) -> ResultObject:
        """
        Tells MetaboSERV to delete a temporary file that was e.g. created for downloading purposes.
        :param filename - Name of the file to delete
        """
        try:
            if not filename: return ResultObject(msg="No file provided!", status=400)
            if os.path.isfile(os.path.join(self.upload_folder, filename)):
                os.remove(os.path.join(self.upload_folder, filename))
                return ResultObject(msg="Temporary file removed!")
        except Exception as e:
            logging.error(f"Encountered an error removing temporary file: {e}")
            return ResultObject(msg=f"Encountered an error removing temporary file: {e}", status=500)
        
    ################### User authentication and authorization #######################

    def user_create(self, username: str, password: str, email: str, institution: str, admin=False) -> ResultObject:
        """
        Creates a user entity in ES. Invoked e.g. through "Register" button in the frontend.
        The password is salted and only stored in a hashed form (for verification purposes).
        :param username - The name of the user to create
        :param password - The password of the user
        :param email - The e-mail address of the user
        :param institution - The institution of the user (e.g. university)
        :param admin: - True for creation of admin user (only the very first user account created automatically)
        """
        try:
            ### These are mainly a failsafe, it should already be ensured this can't happen through the API ###
            if self.user_check_exists(username):
                return ResultObject(msg=f"User with that name ('{username}') already exists!", status=409)
            if not admin and username.lower() in FORBIDDEN_IDS:
                return ResultObject(msg=f"Username contains a protected keyword and can not be used.", status=400)
            if not admin and 'admin' in username.lower():
                return ResultObject(msg=f"Usernames can not contain the substring 'admin'.", status=400)

            salt = secrets.token_hex(64)
            password_enc = password.encode()
            digest = hashlib.pbkdf2_hmac('sha256', password_enc, salt.encode(), 10000)
            user_dict = {
                'username': username,
                'password': digest.hex(),
                'salt': salt,
                'email': email,
                'institution': institution,
                'role': "user",
                'verified': "NO" if self.smtp_config["verification"] else "YES",
                'studies': {},
            }
            if username == "admin":
                user_dict['role'] = "admin"
                user_dict['verified'] = 'YES'
            self.es_client.index(index=INDICES["USER"], id=user_dict['username'], body=json.dumps(user_dict), refresh='true')
            self.user_send_verification_email(username, email)
            return ResultObject(msg=f"Successfully created user record '{username}'. You can now log in! Please check your provided e-mail address and take note of the verification code.")
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception during account creation: {e}", status=500)

    def user_delete(self, username: str, auth_user=None) -> ResultObject:
        """
        Deletes a user account by removing the according Elasticsearch document.
        Also deletes all studies they uploaded regardless of contributor status.
        By way of study_delete() it also deletes all links to other users.
        :param username - The user to delete
        :param auth_user - The user performing the deletion. Must be admin or the user themselves.
        """
        try:
            if not self.user_check_exists(username):
                return ResultObject(msg=f"User {username} does not exist!", status=404)
            if not auth_user:
                return ResultObject(msg="You must be logged in to perform this action!", status=401)
            if username != auth_user.username and auth_user.username != 'admin':
                return ResultObject(msg="Only the administrator or the according user can delete user accounts!", status=401)
            try:
                user_data = self.user_internal_get_information(username).getBody()
                if "studies" in user_data.keys() and user_data["studies"]:
                    for (study_key, study_perms) in user_data["studies"].items():
                        if study_perms == "uploader":
                            self.study_delete(study_key, auth_user=auth_user)
                self.es_client.delete(index=INDICES["USER"], id=username)
                self.es_client.indices.refresh(index=INDICES["USER"])
            except NotFoundError:
                pass
            return ResultObject(msg=f"User {username} successfully deleted.")
        except Exception as e:
            logging.error(f"Encountered an error while deleting a user: {e}")
            return ResultObject(msg=f"Encountered an error while deleting a user: {e}", status=500)

    def user_check_exists(self, username: str) -> bool:
        """
        Checks if a user account already exists.
        :param username - The username to check
        """
        try:
            if not username: return False
            does_exist = self.record_exists_in_es(index=INDICES["USER"], record_id=username)
            return (bool(does_exist))
        except Exception as e:
            logging.error(f"Failed checking if a user exists with the following error: {e}")
            return ResultObject(msg=f"Encountered an exception during user check: {e}", status=500)

    def user_get_information(self, username: str, auth_user=None) -> ResultObject:
        """
        Retrieves data on a user. Will not return password and/or salt.
        :param username - The user to retrieve information on
        :param auth_user - Logged in user
        """
        try:
            if not auth_user: return ResultObject(msg="User account is required to use this endpoint!", status=401)
            if auth_user.username != "admin" and auth_user.username != username:
                return ResultObject(msg="You need administrator permissions to retrieve user information about anyone else!", status=401)
            user_dict = self.es_client.get(index=INDICES["USER"], id=username)['_source']
            del user_dict['salt']
            del user_dict['password']
            if 'verification_code' in user_dict.keys(): del user_dict['verification_code']
            if 'verification_expiration' in user_dict.keys(): del user_dict['verification_expiration']
            return ResultObject(body=user_dict)
        except NotFoundError:
            return ResultObject(msg="User with provided name does not exist.", status=404)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception getting user information: {e}", status=500)
        
    def user_internal_get_information(self, username: str) -> ResultObject:
        """
        Gets data about a specific user. Not connected to any open endpoint.
        Strictly intended for internal use, e.g. to check password against what a user provides.
        :param username - Username of the user to retrieve data for
        """
        try:
            user_dict = self.es_client.get(index=INDICES["USER"], id=username)['_source']
            return ResultObject(body=user_dict)
        except NotFoundError:
            return ResultObject(msg="User with provided name does not exist.", status=404)
        except Exception as e:
            logging.error(e)
            return ResultObject(msg=f"Encountered an exception getting user information: {e}", status=500)

    def user_authenticate_and_return(self, username: str, password: str) -> object:
        """
        Authenticates a user by username and password (e.g. used by login endpoint).
        Then returns the according user dictionary on successful authentication.
        :param username - Username of the user
        :param password - Password of the user
        :remark ResultObject can not be used here - flask-jwt requires a user object
        """
        try:
            user_in_question = self.user_internal_get_information(username).getBody()
            encoded_password = password.encode()
            hashed_password = hashlib.pbkdf2_hmac('sha256', encoded_password, user_in_question['salt'].encode(), 10000).hex()
            if (user_in_question['username'] == username and user_in_question['password'] == hashed_password):
                return user_in_question
            else: return
        except Exception as e:
            logging.error(e)
            return
        
    def user_make_contributor(self, username: str, study_id: str, auth_user=None) -> ResultObject:
        """
        Adds a user as a contributor on a particular study. Adapts both study and user documents in Elasticsearch.
        :param username - Name of the user to add as contributor
        :param study_id - ID of the according study
        :param auth_user - User performing the permission grant
        """
        try:
            ## Authorization and parameter checks ##
            check = self.user_auth_params_check(username, study_id, auth_user=auth_user, auth_token=None)
            if not check.status in [200, 201]: return check

            ## Update study document ##
            study_members = self.study_get_internal(study_id).getBody()['study_members']
            if not isinstance(study_members, dict): study_members = {}
            if username in study_members.keys():
                if study_members[username] == "viewer": study_members[username] = "contributor"
            else:
                study_members[username] = "contributor"
            self.es_client.update(index=INDICES["STUDY"], id=study_id, body={"doc": {"study_members": study_members}})

            ## Update user document
            user_studies = self.user_internal_get_information(username).getBody()['studies']
            if not isinstance(user_studies, dict): user_studies = {}
            if study_id in user_studies.keys():
                if user_studies[study_id] == "viewer": user_studies[study_id] = "contributor"
            else:
                user_studies[study_id] = "contributor"
            self.es_client.update(index=INDICES["USER"], id=username, body={"doc": {"studies": user_studies}})

            self.es_client.indices.refresh(index=INDICES["STUDY"])
            self.es_client.indices.refresh(index=INDICES["USER"])
            return ResultObject(msg=f"Successfully made user {username} a contributor on study {study_id} (higher permission levels were retained).")
        except Exception as e:
            logging.error(f"Encountered an error while granting contributor permissions on study {study_id}: {e}")
            return ResultObject(msg=f"Encountered an error while granting contributor permissions on study {study_id}: {e}", status=500)

    def user_make_viewer(self, username: str, study_id:str, auth_user=None, auth_token=None) -> ResultObject:
        """
        Adds a user as a viewer on a particular study. Adapts both study and user documents in Elasticsearch.
        :param username - Name of the user to add as viewer
        :param study_id - ID of the according study
        :param auth_user - User performing the permission grant
        :param auth_token - Token that could be used to obtain the necessary permission to perform the grant
        """
        try:
            ## Authorization and parameter checks ##
            check = self.user_auth_params_check(username, study_id, auth_user=auth_user, auth_token=auth_token)
            if not check.status in [200, 201]: return check

            ## Update study document ##
            study_members = self.study_get_internal(study_id).getBody()['study_members']
            if not isinstance(study_members, dict): study_members = {}
            if username not in study_members.keys():
                study_members[username] = "viewer"
            self.es_client.update(index=INDICES["STUDY"], id=study_id, body={"doc": {"study_members": study_members}})

            ## Update user document
            user_studies = self.user_internal_get_information(username).getBody()['studies']
            if not isinstance(user_studies, dict): user_studies = {}
            if study_id not in user_studies.keys():
                user_studies[study_id] = "viewer"
            self.es_client.update(index=INDICES["USER"], id=username, body={"doc": {"studies": user_studies}})
            self.es_client.indices.refresh(index=INDICES["USER"])
            self.es_client.indices.refresh(index=INDICES["STUDY"])

            return ResultObject(msg=f"Successfully made user {username} a viewer on study {study_id} (higher permission levels were retained).")
        except Exception as e:
            logging.error(f"Encountered an error while granting viewer permissions on study {study_id}: {e}")
            return ResultObject(msg=f"Encountered an error while granting viewer permissions on study {study_id}: {e}", status=500)

    def user_revoke_permissions(self, username: str, study_id: str, auth_user=None) -> ResultObject:
        """
        Removes a user from a study. Only the study uploader can perform this action.
        :param username - Username of the user to revoke
        :param study_id - ID of the study to remove user from
        :param auth_user - User trying to revoke another user's privileges
        """
        try:
            if not auth_user or not self.user_check_authorization(auth_user, study_id, required_permission="uploader"):
                return ResultObject(msg="You don't have the necessary permissions to perform this action!", status=401)
            if username == auth_user.username:
                return ResultObject(msg="Uploader permission can not be revoked!", status=401)
            if not username or not self.user_check_exists(username):
                return ResultObject(msg="Username missing or user does not exist!", status=404)
            if not study_id or not self.study_check_exists(study_id):
                return ResultObject(msg="Study ID missing or study does not exist!", status=404)
            
            study_dict = self.study_get_internal(study_id).getBody()
            if username in study_dict['study_members'].keys():
                del study_dict['study_members'][username]
            user_dict = self.user_internal_get_information(username).getBody()
            if study_id in user_dict['studies'].keys():
                del user_dict['studies'][study_id]
            
            self.es_client.index(index=INDICES["STUDY"], id=study_id, body=study_dict, refresh='true')
            self.es_client.index(index=INDICES["USER"], id=username, body=user_dict, refresh='true')
            return ResultObject(msg=f"Successfully revoked permissions for user {username}.")
        except Exception as e:
            logging.error(f"Encountered an error while revoking permissions from user {username}: {e}")
            return ResultObject(msg=f"Encountered an error while revoking permissions from user {username}: {e}", status=500)
        
    def user_unlink(self, username: str, study_id: str) -> ResultObject:
        """
        Removes a user from a study. Internal method that is only called on study deletion.
        :param username - Username of the user to revoke
        :param study_id - ID of the study to remove user from
        """
        try:
            if not username or not self.user_check_exists(username):
                return ResultObject(msg="Username missing or user does not exist!", status=404)
            if not study_id or not self.study_check_exists(study_id):
                return ResultObject(msg="Study ID missing or study does not exist!", status=404)
            
            study_dict = self.study_get_internal(study_id).getBody()
            if username in study_dict['study_members'].keys():
                del study_dict['study_members'][username]
            
            self.es_client.index(index=INDICES["STUDY"], id=study_id, body=study_dict, refresh='true')
            return ResultObject(msg=f"Successfully unlinked study {study_id} from {username}.")
        except Exception as e:
            logging.error(f"Encountered an error while unlinking study from user {username}: {e}")
            return ResultObject(msg=f"Encountered an error while unlinking study from user {username}: {e}", status=500)
    
    def user_list_all(self) -> ResultObject:
        """
        Lists all user accounts in the database.
        Only accessible behind admin endpoint.

        Returns a list of all studies, as long as the necessary permissions are present.
        :param auth_user - The user trying to get a list of studies (public and private)
        :param auth_token - Token that may be used to obtain permissions
        """
        try:
            user_scan = scan(self.es_client, index=INDICES["USER"], query={"query": {"match_all": {}}})
            user_dict = {}

            for k in user_scan:
                user_dict[k["_id"]] = k["_source"]
                
            return ResultObject(body=user_dict)
        except Exception as e:
            logging.error(f"Failed to retrieve user list with error {e}.")
            return ResultObject(msg=f"Failed to retrieve user list with error {e}.", status=500)
        
    def user_list_for_study(self, study_id, auth_user=None, auth_token=None) -> ResultObject:
        """
        Lists all users that are denoted for a particular study.
        Requires contributor permission.
        :param study_id - ID of the study
        :param auth_user - The user trying to get a list of studies (public and private)
        :param auth_token - Token that may be used to obtain permissions
        """
        try:
            if not auth_user and not auth_token:
                return ResultObject(msg="You need to be logged in or provide an authorization token!", status=401)
            if not self.study_check_exists(study_id):
                return ResultObject(msg=f"Study {study_id} does not exist!", status=404)
            auth_check_user = self.user_check_authorization(auth_user, study_id, required_permission="contributor")
            auth_check_token = self.token_check_authorization(auth_check_token, study_id, required_permission="contributor")
            if not auth_check_token and not auth_check_user:
                return ResultObject(msg=f"You lack the necessary permission to view this user list!", status=401)
            
            study_users = self.study_get_internal(study_id).getBody()['study_permissions']
            return ResultObject(body=study_users)
        except Exception as e:
            logging.error(f"Failed to retrieve user list for study {study_id} with error {e}.")
            return ResultObject(msg=f"Failed to retrieve user list for study {study_id} with error {e}.", status=500)

    def user_check_authorization(self, user, study_id, required_permission="contributor") -> bool:
        """
        Checks whether a particular user has necessary permissions to perform an action.
        :param user - User intending to perform an action
        :param study_id - Study to check authorization for
        :param required_permission - Permission level required for the action. Should be "uploader", "contributor" or "viewer".
            Admin actions should instead be prefixed in the API with @admin_required.
        """
        try:
            if not study_id:
                return False
            study_data = self.study_get_internal(study_id).getBody()
            if not study_data: return False
            if user and user.username:
                user_data = self.user_internal_get_information(user.username).getBody()
                if not isinstance(user_data['studies'], dict) or not isinstance(study_data['study_members'], dict): return False
                # Check whether user is registered on the study
                # In this case, the study must be public to be accessible!
                if study_id not in user_data['studies'].keys() or user.username not in study_data['study_members'].keys():
                    if study_data["study_visibility"] == "public" and required_permission == "viewer":
                        return True
                    else: return False
                
                # Permissions on private studies are invalid unless the account is verified:
                if user_data['verified'] != "YES" and self.smtp_config["verification"]:
                    return False

                # Otherwise, check if the required permissions are met (regardless of visibility)
                if required_permission == "uploader":
                    if study_data["study_members"][user.username] != "uploader" or user_data['studies'][study_id] != "uploader": return False
                elif required_permission == "contributor":
                    if study_data["study_members"][user.username] not in ["contributor", "uploader"] \
                        or user_data["studies"][study_id] not in ["contributor", "uploader"]: return False
                elif required_permission == "viewer":
                    # If the user is listed at all they are guaranteed to have viewer permissions.
                    return True
                else:
                    # Only the 3 keywords viewer, contributor and uploader are accepted.
                    return False
            # If user isn't logged in, the study has to be public.
            else:
                if study_data["study_visibility"] == "public" and required_permission == "viewer":
                        return True
                else: return False
            # If no reason was found to deny access, return true.
            return True
        except NotFoundError:
            logging.error("Authorization was attempted for a nonexistant user or study")
            return False
        except Exception as e:
            logging.error(f"Encountered a general error while checking user authorization: {e}")
            return False

    def user_auth_params_check(self, username: str, study_id: str, auth_user=None, auth_token=None) -> ResultObject:
        """
        Function that combines a lot of common checks which occur when permissions on a specific study are evaluated.
        :param username - Username to check for
        :param study_id - Study to check for
        :param auth_user - User performing a particular action
        :param auth_token - Authorization token which may give necessary permissions for the action
        """
        if not auth_user and not auth_token:
                return ResultObject(msg="You must be logged in or provide an authorization token!", status=401)
        if not self.study_check_exists(study_id):
            return ResultObject(msg=f"The study you want to adapt permissions for ({study_id}) does not exist!", status=404)
        if auth_user.username == username:
            return ResultObject(msg=f"You can not grant permissions to yourself or remove them from yourself!", status=409)
        auth_check_user = self.user_check_authorization(auth_user, study_id, required_permission="contributor")
        auth_check_token = self.token_check_authorization(auth_token, study_id, required_permission="contributor")
        if not auth_check_user and not auth_check_token:
            return ResultObject(msg=f"You lack the necessary permissions to grant access to study {study_id}!", status=401)
        if not self.user_check_exists(username):
            return ResultObject(msg=f"The user you want to grant permissions ({username}) does not exist!", status=404)
        return ResultObject(msg="All checks passed", status=200)
    
    def user_email_check_exists(self, email: str) -> ResultObject:
        """
        Function that leverages the email-validator package to test if an email address actually exists.
        Also returns a normalized version of the e-mail address.
        :param email - Email address to validate
        :return Normalized email address, or None if validation failed
        """
        if not email:
            return ResultObject(msg="No email provided!", status=404)
        try:
            info = validate_email(email)
            return ResultObject(body=info.normalized)
        except EmailNotValidError as e:
            return ResultObject(msg=f"Email address could not be validated! Error: {e}", status=400)
        
    def user_send_verification_email(self, username: str, email: str) -> ResultObject:
        """
        Sends a verification code to a user per email.
        The user can then enter this code (valid for 24h) to verify their account.
        :param username - Username of the user to verify
        :param email - Email address to send the verification code to
        """
        try:
            if not username or not email:
                return ResultObject(msg="No username or no e-mail provided!", status=400)
            if not self.user_check_exists(username):
                return ResultObject(msg="The provided user does not exist!", status=404)
            user_info = self.user_internal_get_information(username).getBody()
            if user_info['verified'] == 'YES':
                return ResultObject(msg="The user is already verified, there is no need to send another email.", status=200)
            
            # Create a 10 character verification code
            code = ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits) for _ in range(10))
            expiration = (datetime.datetime.today() + datetime.timedelta(days=1)).strftime('%d/%m/%Y, %H:%M:%S')

            self.es_client.update(index=INDICES["USER"], id=username, body={"doc": {"verification_code": code, "verification_expiration": expiration}})
            self.es_client.indices.refresh(index=INDICES["USER"])

            subject = "MetaboSERV: Please verify your e-mail address"
            body = f"""\
            Hello {username},
            thank you for your MetaboSERV registration!

            To verify this is your e-mail address, please log in, click on the 'Account' tab and enter the following code in the 'Verify account' section:
            {code}

            Your code will be valid for 24 hours, or until {expiration}.
            This email is auto-generated. Feel free to respond to it regardless if you have any questions.
            """

            message = MIMEText(body, 'plain')
            message["Subject"] = subject
            message["From"] = self.smtp_config['email']
            message["To"] = email

            try:
                with smtplib.SMTP_SSL(self.smtp_config['server'], self.smtp_config['port']) as smtp_server:
                    smtp_server.login(self.smtp_config['email'], self.smtp_config['password'])
                    smtp_server.sendmail(self.smtp_config['email'], email, message.as_string())
                    logging.info(f"Successfully sent a verification code to {email}.")
            except Exception as e:
                return ResultObject(msg=f"Sorry, the verification email could not be sent! Error: {e}", status=500)

            return ResultObject(msg="A verification code has been sent to the provided e-mail address.")
        except Exception as e:
            logging.error(f"Encountered an error while sending a verification mail: {e}")
            return ResultObject(msg=f"Encountered an error while sending a verification mail: {e}", status=500)
        
    def user_send_recovery_mail(self, username: str) -> ResultObject:
        """
        Sends a password recovery code to the user.
        The user can then enter this code (valid for 24h) to receive a new auto-generated password.
        :param username - Username of the user to verify
        :param email - Email address to send the verification code to
        """
        try:
            if not username:
                return ResultObject(msg="No username provided!", status=400)
            if not self.user_check_exists(username):
                return ResultObject(msg="The provided user does not exist!", status=404)
            user_info = self.user_internal_get_information(username).getBody()
            email = user_info["email"]
            
            # Create a 10 character verification code
            code = ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits) for _ in range(10))
            expiration = (datetime.datetime.today() + datetime.timedelta(days=1)).strftime('%d/%m/%Y, %H:%M:%S')

            self.es_client.update(index=INDICES["USER"], id=username, body={"doc": {"recovery_code": code, "recovery_expiration": expiration}})
            self.es_client.indices.refresh(index=INDICES["USER"])

            subject = "MetaboSERV: Password recovery"
            body = f"""\
            Hello {username},
            a password reset was requested for your MetaboSERV account. If this was not you, you can ignore this e-mail.

            To verify this account belongs to you, please enter the following code into the prompt that should have opened on the MetaboSERV website:
            {code}

            Your code will be valid for 24 hours, or until {expiration}.
            This email is auto-generated. Feel free to respond to it regardless if you have any questions.
            """

            message = MIMEText(body, 'plain')
            message["Subject"] = subject
            message["From"] = self.smtp_config['email']
            message["To"] = email

            try:
                with smtplib.SMTP_SSL(self.smtp_config['server'], self.smtp_config['port']) as smtp_server:
                    smtp_server.login(self.smtp_config['email'], self.smtp_config['password'])
                    smtp_server.sendmail(self.smtp_config['email'], email, message.as_string())
                    logging.info(f"Successfully sent a recovery code to {email}.")
            except Exception as e:
                return ResultObject(msg=f"Sorry, the recovery email could not be sent. Error: {e}", status=500)

            return ResultObject(msg="A recovery code has been sent to the provided e-mail address.")
        except Exception as e:
            logging.error(f"Encountered an error while sending a recovery mail: {e}")
            return ResultObject(msg=f"Encountered an error while sending a recovery mail: {e}", status=500)
        
    def user_verify(self, username: str, code: str) -> ResultObject:
        """
        Attempts to verify a user account using the provided code.
        :param username: The name of the user to verify
        :param code: Verification code from the according email
        """
        try:
            if not username or not code:
                return ResultObject(msg="No username or no code provided!", status=400)
            if not self.user_check_exists(username):
                return ResultObject(msg="The provided user does not exist!", status=404)
            user_info = self.user_internal_get_information(username).getBody()
            if user_info['verified'] == 'YES':
                return ResultObject(msg="The user is already verified, there is no need to verify them again.", status=200)
            if not 'verification_code' in user_info.keys() or not 'verification_expiration' in user_info.keys():
                return ResultObject(msg="No verification code or expiration date found for the user.", status=500)
            if code != user_info['verification_code']:
                return ResultObject(msg="The verification code does not match!", status=400)
            if datetime.datetime.today() > datetime.datetime.strptime(user_info['verification_expiration'], '%d/%m/%Y, %H:%M:%S'):
                return ResultObject(msg="Sorry, this code has already expired. Please request a new verification code.", status=410)
            
            self.es_client.update(index=INDICES["USER"], id=username, body={"doc": {"verification_code": "-", "verification_expiration": "-", "verified": "YES"}})
            self.es_client.indices.refresh(index=INDICES["USER"])
            
            return ResultObject(msg="You successfully verified your account. Thank you for your interest in MetaboSERV!")
        except Exception as e:
            logging.error(f"Encountered an error during user verification: {e}")
            return ResultObject(msg=f"Encountered an error during user verification: {e}", status=500)
        
    def user_recover_password(self, username: str, code: str) -> ResultObject:
        """
        Recoves the users password if the code matches, then sends it in a mail.
        :param username: The name of the user
        :param code: Recovery code from the according email
        """
        try:
            if not username or not code:
                return ResultObject(msg="No username or no code provided!", status=400)
            if not self.user_check_exists(username):
                return ResultObject(msg="The provided user does not exist!", status=404)
            user_info = self.user_internal_get_information(username).getBody()
            if not 'recovery_code' in user_info.keys() or not 'recovery_expiration' in user_info.keys():
                return ResultObject(msg="No recovery code or expiration date found for the user.", status=500)
            if code != user_info['recovery_code']:
                return ResultObject(msg="The recovery code does not match!", status=400)
            if datetime.datetime.today() > datetime.datetime.strptime(user_info['recovery_expiration'], '%d/%m/%Y, %H:%M:%S'):
                return ResultObject(msg="Sorry, this recovery code has already expired. Please request a new recovery code.", status=410)
            
            # Create new password and salt
            salt = secrets.token_hex(64)
            new_password = secrets.token_hex(16)
            password_enc = new_password.encode()
            digest = hashlib.pbkdf2_hmac('sha256', password_enc, salt.encode(), 10000)
            

            # Send mail
            email = user_info["email"]
            subject = "MetaboSERV: New temporary password"
            body = f"""\
            Hello {username},
            the password for your account was reset to a temporary password. Make sure to change it as soon as possible.

            Your new password:
            {new_password}
           
            This email is auto-generated. Feel free to respond to it regardless if you have any questions.
            """

            message = MIMEText(body, 'plain')
            message["Subject"] = subject
            message["From"] = self.smtp_config['email']
            message["To"] = email

            try:
                with smtplib.SMTP_SSL(self.smtp_config['server'], self.smtp_config['port']) as smtp_server:
                    smtp_server.login(self.smtp_config['email'], self.smtp_config['password'])
                    smtp_server.sendmail(self.smtp_config['email'], email, message.as_string())
                    logging.info(f"Successfully sent a recovery code to {email}.")
            except Exception as e:
                return ResultObject(msg=f"Sorry, your password recovery e-mail could not be sent! Error: {e}. Your password has not been reset.")
            
            self.es_client.update(index=INDICES["USER"], id=username, body=
                {"doc": {"recovery_code": "-", "recovery_expiration": "-", "salt": salt, "password": digest.hex()}})
            self.es_client.indices.refresh(index=INDICES["USER"])
            
            return ResultObject(msg="You successfully reset your password, check your e-mails for a new temporary password. Make sure to change it!")
        except Exception as e:
            logging.error(f"Encountered an error during user verification: {e}")
            return ResultObject(msg=f"Encountered an error during user verification: {e}", status=500)
        
    def user_change_password(self, username: str, new_password: str, old_password: str) -> ResultObject:
        """
        Attempts to change the password. Also changes the salt.
        :param username - The name of the user to change the password for
        :param old_password - Old password, for verification purposes
        :param new_password - New password
        """
        try:
            if not username or not new_password or not old_password:
                return ResultObject(f"At least one parameter is missing!", status=400)
            
            # Check old password
            user = dict2obj(self.user_authenticate_and_return(username, old_password)) #Returns none if user not found or password fails
            if not user:
                return ResultObject(msg="Old password does not match!", status=401)
            
            # Set new password
            salt = secrets.token_hex(64)
            password_enc = new_password.encode()
            digest = hashlib.pbkdf2_hmac('sha256', password_enc, salt.encode(), 10000)

            self.es_client.update(index=INDICES["USER"], id=username, body={"doc": {"password": digest.hex(), "salt": salt}})
            self.es_client.indices.refresh(index=INDICES["USER"])

            return ResultObject(msg="Password changed successfully. Please log in again.")
        except Exception as e:
            logging.error(f"Encountered an error during password change: {e}")
            return ResultObject(msg=f"Encountered an error during password change: {e}", status=500)
        
    def user_track_login_attempt(self, user_info: dict, mode="inc") -> ResultObject:
        """
        Increases the amount of tracked wrong login attempts by 1.
        :param user_info - Dict containing according user data
        :param mode - If not "inc", it will clear the untracked attempts (called on login).
        """
        try:
            if mode != "inc":
                self.es_client.update(index=INDICES["USER"], id=user_info["username"], body={"doc": {"untracked": 0}})
                return ResultObject(msg="Reset tracked wrong login attempts.")
            if "untracked" not in user_info.keys():
                self.es_client.update(index=INDICES["USER"], id=user_info["username"], body={"doc": {"untracked": 1}})
            else:
                untracked = int(user_info["untracked"]) + 1
                self.es_client.update(index=INDICES["USER"], id=user_info["username"], body={"doc": {"untracked": untracked}})
                if self.smtp_config['notify_attempts'] > 0 and untracked == self.smtp_config['notify_attempts']:
                    self.user_send_sus_notification(user_info["username"], user_info["email"])
                if self.smtp_config['lock_attempts']> 0 and untracked >= self.smtp_config['lock_attempts']:
                    self.user_lock_temporarily(user_info["username"])
            self.es_client.indices.refresh(index=INDICES["USER"])
            return ResultObject(msg="Increased amount of tracked wrong login attempts by 1.")
        except Exception as e:
            logging.error(f"Encountered an error during login attempt tracking: {e}")
            return ResultObject(msg=f"Encountered an error during login attempt tracking: {e}", status=500)
        
    def user_send_sus_notification(self, username: str, email: str):
        """
        Sends a 'suspicious activity' e-mail to a user.
        This is sent after a default amount of 5 failed login attempts.
        :param username - Username of the user
        :param email - Email address to send the mail to
        """
        try:
            if not username or not email:
                return ResultObject(msg="No username or no e-mail provided!", status=400)
            if not self.user_check_exists(username):
                return ResultObject(msg="The provided user does not exist!", status=404)

            subject = "MetaboSERV: Suspicious activity on your account"
            body = f"""\
            Hello {username},
            we have detected an unusual amount of failed login attempts for your account.

            If this was you, there is no need to worry - Remember you can always use the 'Forgot password' option to set a new password for yourself.
            If this was not you, consider changing your password to something very secure, as someone else might be attempting to log in to your MetaboSERV account.

            This email is auto-generated. Feel free to respond to it regardless if you have any questions.
            """

            message = MIMEText(body, 'plain')
            message["Subject"] = subject
            message["From"] = self.smtp_config['email']
            message["To"] = email

            with smtplib.SMTP_SSL(self.smtp_config['server'], self.smtp_config['port']) as smtp_server:
                smtp_server.login(self.smtp_config['email'], self.smtp_config['password'])
                smtp_server.sendmail(self.smtp_config['email'], email, message.as_string())
                logging.info(f"Successfully sent a suspicious activity mail to {email}.")

            return ResultObject(msg="A suspicious activity mail has been sent to the provided e-mail address.")
        except Exception as e:
            logging.error(f"Encountered an error while sending a suspicious activity mail: {e}")
            return ResultObject(msg=f"Encountered an error while sending a suspicious activity mail: {e}", status=500)

    def user_lock_temporarily(self, username: str) -> ResultObject:
        """
        Temporarily locks a MetaboSERV user account, barring anyone from logging in (15 minutes by default).
        :param username - Username of the account to lock
        """
        try:
            if not username:
                return ResultObject(msg="No username provided!", status=400)
            if not self.user_check_exists(username):
                return ResultObject(msg="The provided user does not exist!", status=404)
            user_info = self.user_internal_get_information(username).getBody()

            # If the account is already locked, no need to re-lock:
            if 'locked' in user_info.keys() and user_info['locked'] != "NO":
                return ResultObject(msg=f"The account is already locked until {user_info['locked']}.")
            
            # Create a date that represents 15 minutes from now
            expiration = (datetime.datetime.today() + datetime.timedelta(minutes=15)).strftime('%d/%m/%Y, %H:%M:%S')

            self.es_client.update(index=INDICES["USER"], id=username, body={"doc": {"locked": expiration}})
            self.es_client.indices.refresh(index=INDICES["USER"])

            return ResultObject(msg=f"User account {username} has been locked for 15 minutes.")
        except Exception as e:
            logging.error(f"Encountered an error while locking an account: {e}")
            return ResultObject(msg=f"Encountered an error while locking an account: {e}", status=500)
        
    def user_clear_lock(self, username: str) -> ResultObject:
        """
        Clears the locked status from an account.
        :param username - The user to unlock
        """
        try:
            self.es_client.update(index=INDICES["USER"], id=username, body={"doc": {"locked": "NO"}}, refresh="TRUE")
            return ResultObject(msg=f"Successfully unlocked the account {username}.")
        except Exception as e:
            logging.error(f"Encountered an error while unlocking an account: {e}")
            return ResultObject(msg=f"Encountered an error while unlocking an account: {e}", status=500)

    ################### Token authorization #####################
        
    def token_create(self, study: list, expiration=0, auth_level="viewer", comment="No comment", auth_user=None) -> ResultObject:
        """
        Creates an authorization token for one or more studies using the secrets library.
        :param study - The study or list of studies to create a token for
        :param expiration - Number of days after which the token will expire
        :param auth_level - The authorization level for the token (either "viewer" or "contributor")
        :param comment - Comment associated with the token
        :param auth_user - User issuing the token
        """
        try:
            if not auth_user or not auth_user.username:
                return ResultObject(msg="You need to be logged in to perform this action!", status=401)
            if not study:
                return ResultObject(msg="No study was provided!", status=404)
            if auth_level not in ["viewer", "contributor"]:
                return ResultObject(f"Invalid token class ({auth_level}) was provided!", status=401)
            if isinstance(study, str): study = [study]
            for study_id in study:
                if not self.user_check_authorization(auth_user, study_id, required_permission="contributor"):
                    return ResultObject(msg="You are not authorized to create a token for at least one provided study!", status=401)
            token_id = secrets.token_hex(12)
            if not expiration or not isinstance(expiration, int) or expiration <=0:
                expiration_date = "NEVER"
            else:
                expiration_date = (datetime.datetime.today() + datetime.timedelta(days=expiration)).strftime('%d/%m/%y')
            token_data = {
                "token_id": token_id,
                "creator": auth_user.username,
                "date_creation": datetime.datetime.today().strftime('%d/%m/%y'),
                "date_expiration": expiration_date,
                "studies": study,
                "auth_level": auth_level,
                "comment": comment,
            }

            self.es_client.create(index=INDICES["TOKEN"], id=token_id, body=token_data, refresh='wait_for')
            for study_id in study:
                self.token_add_to_study(study_id, token_data)
            return ResultObject(body=token_data, msg=f"Successfully created authorization token {token_id}.")
        except Exception as e:
            logging.error(f"Encountered an error during token creation: {e}")
            return ResultObject(msg=f"Encountered an error during token creation: {e}", status=500)
            
    def token_check_access(self, token_id: str, auth_user=None) -> object:
        """
        Checks whether a user has the necessary permissions to update a token.
        This means that the user either has to be uploader of the according study,
        or they have to have created the token.
        Also returns the token if successful.
        :param token_id - The token in question
        :param auth_user - The user wanting to retrieve information on, or update the token
        """
        try:
            if not auth_user or not auth_user.username or not token_id: return None
            token_data = self.token_get_internal(token_id).getBody()
            if not token_data: return None
            token_studies = token_data['studies']
            if not token_studies: return None
            if not isinstance(token_studies, list): token_studies = [token_studies]
            for study in token_studies:
                if not self.user_check_authorization(auth_user, study, required_permission="uploader") and not token_data["creator"] == auth_user.username:
                    return None
            return token_data
        except NotFoundError:
            logging.error(f"Token access check was performed for a nonexistant token.")
            return None
        except Exception as e:
            logging.error(f"Encountered a general error during token access check: {e}.")
            return None

    def token_get(self, token_id: str, auth_user=None) -> ResultObject:
        """
        Retrieves information about a token.
        :param token_id - ID of the token in question
        :param auth_user - The user trying to retrieve token information
        """ 
        try:
            if not auth_user or not auth_user.username:
                return ResultObject(msg="You must be logged in to retrieve token information!", status=401)
            potential_token = self.token_check_access(token_id, auth_user)
            if not potential_token:
                return ResultObject(msg="You lack the necessary permissions to retrieve this token!")
            return ResultObject(body=potential_token)
        except NotFoundError:
            return ResultObject(msg="The token in question does not exist!", status=404)
        except Exception as e:
            logging.error(f"Encountered an error while retrieving a token: {e}")
            return ResultObject(msg=f"Encountered an error while retrieving a token: {e}", status=500)

    def token_get_internal(self, token_id: str) -> ResultObject:
        """
        Retrieves information about a token. Internal method that is not open to any API calls.
        :param token_id - ID of the token in question
        """ 
        try:
            return ResultObject(body=self.es_client.get(index=INDICES["TOKEN"], id=token_id)['_source'])
        except NotFoundError:
            return ResultObject(msg="The token in question does not exist!", status=404)
        except Exception as e:
            logging.error(f"Encountered an error while retrieving a token: {e}")
            return ResultObject(msg=f"Encountered an error while retrieving a token: {e}", status=500)

    def token_revoke(self, token_id: str, auth_user=None) -> ResultObject:
        """
        Revokes an authorization token.
        :param token_id - ID of the token to revoke
        :param auth_user - User revoking the token
        """
        try:
            if not auth_user or not auth_user.username:
                return ResultObject(msg="You need to be logged in to revoke an authorization token!", status=401)
            potential_token = self.token_check_access(token_id, auth_user)
            if not potential_token:
                return ResultObject(msg="You lack the necessary permissions to revoke this token!", status=401)
            token_data = self.token_get_internal(token_id).getBody()
            for study_id in token_data["studies"]:
                self.token_delete_from_study(study_id, token_id)
            self.es_client.delete(index=INDICES["TOKEN"], id=token_id, refresh='wait_for')
            return ResultObject(msg="Successfully revoked the token!")
        except Exception as e:
            logging.error(f"Encountered an error while revoking a token: {e}")
            return ResultObject(msg=f"Encountered an error while revoking a token: {e}", status=500)

    def token_check_authorization(self, token_id, study_id, required_permission="contributor") -> bool:
        """
        Checks whether a particular token can be used to perform an action.
        :param token_id - ID of the token being used
        :param study_id - Study to check authorization for
        :param required_permission - Permission level required for the action. Should be "uploader", "contributor" or "viewer".
            Admin actions should instead be prefixed in the API with @admin_required.
        """
        try:
            if required_permission not in ["viewer", "contributor", "uploader"]: return False
            if not study_id or not token_id: return False
            token_data = self.token_get_internal(token_id).getBody()
            if not token_data or not token_data["studies"]: return False
            if token_data['date_expiration'] != "NEVER" and datetime.datetime.today() > datetime.datetime.strptime(token_data['date_expiration'], '%d%m%y'): return False
            token_studies = token_data["studies"] if isinstance(token_data["studies"], list) else [token_data["studies"]]
            if study_id not in token_studies: return False
            if required_permission == "uploader" and token_data["auth_level"] != "uploader": return False
            if required_permission == "contributor" and token_data["auth_level"] not in ["uploader", "contributor"]: return False
            if required_permission == "viewer" and not token_data["auth_level"]: return False
            return True
        except NotFoundError:
            return False
        except Exception as e:
            logging.error(f"Encountered an error during token authorization check: {e}")
            return False

    def token_update(self, token_id: str, token_dict: dict, auth_user=None) -> ResultObject:
        """
        Updates an authorization token, e.g. to refresh expiration.
        :param token_id - ID of the token to update
        :param token_dict - Token information
        :param auth_user - The user trying to perform the token update
        """
        try:
            if not auth_user or not auth_user.username:
                return ResultObject(msg="You need to be logged in to update an authorization token!", status=401)
            if not token_dict:
                return ResultObject(msg="No token update data was supplied!", status=400)
            potential_token = self.token_check_access(token_id, auth_user)
            if not potential_token:
                return ResultObject(msg="You lack the necessary permissions to update this token!", status=401)
            self.es_client.update(index=INDICES["TOKEN"], id=token_dict, body={"doc": token_dict})
            return ResultObject(msg="Successfully updated the token!")
        except Exception as e:
            logging.error(f"Encountered an error while updating a token: {e}")
            return ResultObject(msg=f"Encountered an error while updating a token: {e}", status=500)

    def token_check_exists(self, token_id: str) -> ResultObject:
        """
        Returns a 200 if a token is recognized, otherwise a 404.
        :param token_id - The potential token in question
        """
        try:
            does_exist = self.record_exists_in_es(index=INDICES["TOKEN"], record_id=token_id)
            if (bool(does_exist)):
                return ResultObject(msg='Token validated.', status=200)
            else:
                return ResultObject(msg='Token could not be found!', status=404)
        except Exception as e:
            logging.error(f"Failed checking if a token exists with the following error: {e}")
            return ResultObject(msg=f"Encountered an exception during user check: {e}", status=500)

    def token_add_to_study(self, study_id: str, token_data: dict) -> None:
        """
        Adds token data to a study document in Elasticsearch.
        :param study_id - The study to add the token to
        :param token_data - Token data
        """
        try:
            if not study_id or not token_data: return
            study_data = self.study_get_internal(study_id).getBody()
            study_tokens = study_data['study_auth_tokens'] if 'study_auth_tokens' in study_data.keys() else {}
            if study_id in study_tokens.keys(): return
            del token_data['studies']
            study_tokens[token_data['token_id']] = token_data
            self.es_client.update(index=INDICES["STUDY"], id=study_id, body={"doc": {"study_auth_tokens": study_tokens}})
        except Exception as e:
            logging.error(f"Encountered an error while adding token to study")

    def token_delete_from_study(self, study_id: str, token_id: str) -> None:
        """
        Removes token data from a study document in Elasticsearch.
        :param study_id - The study to add the token to
        :param token_data - Token data
        """
        try:
            if not study_id or not token_id: return
            study_data = self.study_get_internal(study_id).getBody()
            study_tokens = study_data['study_auth_tokens'] if 'study_auth_tokens' in study_data.keys() else {}
            if not study_tokens or study_id not in study_tokens.keys(): return
            del study_tokens[token_id]
            self.es_client.update(index=INDICES["STUDY"], id=study_id, body={"doc": {"study_auth_tokens": study_tokens}})
        except Exception as e:
            logging.error(f"Encountered an error while adding token to study")

    ################### Verification and validation #####################
            
        
    def record_exists_in_es(self, index, record_id) -> bool:
        """
        Checks if a particular document already exists in Elasticsearch (by its ID).

        :param index = Index name
        :param record_id = ID of the record in the supposed index
        :returns True if record exists, False otherwise
        """
        return self.es_client.exists(index=index, id=record_id).body
        
    def validate_biospecimen(self, specimen):
        if not specimen: return "unknown"
        specimen = specimen.lower().strip()
        if specimen not in self.biospecimen:
            raise InvalidBiospecimenException(specimen)
        return specimen
    
    def validate_compounds(self, compounds: list):
        """
        Validates and, if necessary, adapts a list of compounds.

        :param compounds - List of compounds
        :returns Validated + potentially updated list of compounds
        """
        if not compounds: return []
        validated_compounds = []
        internal_regex = re.compile('[^a-zA-Z0-9]|_')
        for entry in sorted(set(compounds)):
            if not re.match(self.hmdb_regex, entry.strip()):
                validated_compounds.append(re.sub(internal_regex, "_", entry.strip().lower()))
            else:
                validated_compounds.append(entry.strip())
        return validated_compounds
    
    ################### Adding data after study creation #####################

    def add_metadata_from_file(self, file: FileStorage, study_id: str, filepath: str, auth_user=None, auth_token=None):
        """
        Adds metadata from a file to an existing study.
        :param file - The file in question
        :param study_id - Study identifier
        :param filepath - Path to save the file in (should correspond to sourcefolder + study_id)
        :param auth_user - The user performing the request
        :param auth_token - Optional authorization token instead of auth_user
        """
        if not auth_user and not auth_token:
            return ResultObject(msg="You must be logged in or provide an authorization token!", status=401)
        if not self.study_check_exists(study_id):
            return ResultObject(msg=f"The study you want to add data to ({study_id}) does not exist!", status=404)
        auth_check_user = self.user_check_authorization(auth_user, study_id, required_permission="contributor")
        auth_check_token = self.token_check_authorization(auth_token, study_id, required_permission="contributor")
        if not auth_check_user and not auth_check_token:
            return ResultObject(msg=f"You lack the necessary permissions to add data to study {study_id}!", status=401)
        meta_file_data = get_metadata_from_file(file, filepath)
        if meta_file_data.status != 200:
            return meta_file_data
        current_metadata = self.study_get_internal(study_id).getBody()['study_metadata']
        for k,v in meta_file_data.getBody().items():
            current_metadata[k] = v
        self.study_update_metadata(study_id, current_metadata, auth_user=auth_user, auth_token=auth_token)
        return ResultObject(msg="Successfully added metadata to the study!")
        
    def add_concentration_data_from_file(self, file: str, study_id: str, filepath: str, transpose: bool = False, enforce_hmdb: bool = False, auth_user=None, auth_token=None):
        """
        Adds concentration data from a file to an existing study.
        :param file - The file in question
        :param study_id - Study identifier
        :param filepath - Path to save the file in (should correspond to sourcefolder + study_id)
        :param transpose - Indicates whether the file is to be transposed.
        :param enforce_hmdb - Indicates whether HMDB accession are enforced
        :param auth_user - The user performing the request
        :param auth_token - Optional authorization token instead of auth_user
        """
        if not auth_user and not auth_token:
            return ResultObject(msg="You must be logged in or provide an authorization token!", status=401)
        if not self.study_check_exists(study_id):
            return ResultObject(msg=f"The study you want to add data to ({study_id}) does not exist!", status=404)
        auth_check_user = self.user_check_authorization(auth_user, study_id, required_permission="contributor")
        auth_check_token = self.token_check_authorization(auth_token, study_id, required_permission="contributor")
        if not auth_check_user and not auth_check_token:
            return ResultObject(msg=f"You lack the necessary permissions to add data to study {study_id}!", status=401)
        conc_file_data = get_conc_data_from_file(file, filepath, transpose, enforce_hmdb)
        if conc_file_data.status != 200:
            return conc_file_data
        concentration_data = conc_file_data.body[0]
        compounds = conc_file_data.body[1]
        concentration_metadata = conc_file_data.body[2]
        compound_name_map = conc_file_data.body[3]
        unit_map = conc_file_data.body[4]

        if compound_name_map and len(compound_name_map.keys()) > 0:
            self.compound_add_namemap(compound_name_map, study_id)

        compounds = self.validate_compounds(compounds)
        md_connection = self.create_mariadb_connection()
        self.study_update(study_id, {"study_compounds": compounds}, auth_user=auth_user, auth_token=auth_token)

        # Create metadata entry for each source in the study
        if concentration_metadata:
            for source_entry in concentration_metadata.keys():
                self.es_client.index(index=INDICES["SOURCE"], id=study_id + '_SOURCE_' + str(source_entry), body={
                    "source_id": str(source_entry),
                    "source_study": study_id,
                    "source_metadata": concentration_metadata[source_entry]
                })
            self.es_client.indices.refresh(index=INDICES["SOURCE"])

        # Create unit map for this study
        if unit_map:
            self.es_client.index(index=INDICES["UNIT"], id=study_id, body={
                "study_id": study_id,
                "unit_map": unit_map,
            })

        # Add concentration data to MariaDB
        cursor = md_connection.cursor()
        if concentration_data and compounds:
            self.mariadb_drop_table(study_id)
            md_connection.commit()
            query = f"CREATE TABLE IF NOT EXISTS {study_id} (source_id VARCHAR(255) PRIMARY KEY, {', '.join([f'{compound} FLOAT' for compound in compounds])})"
            cursor.execute(query)
            # Sort compounds alphabetically
            compounds.sort()
            columns = ', '.join(compounds)
            for source_entry in concentration_data.keys():
                if str(source_entry).startswith('PROCESSING') or source_entry.lower() in ['unit', 'units']: continue
                values = ', '.join([concentration_data[source_entry][k] for k in compounds])
                query = f"INSERT INTO {study_id} (source_id, %s) VALUES ('{str(source_entry)}', %s)" % (columns, values)
                cursor.execute(query)
        cursor.close()
        md_connection.commit()
        md_connection.close()
        return ResultObject(msg="Successfully added concentration data to the study!")
        
    def add_phenotype_data_from_file(self, file: str, study_id: str, filepath: str, transpose: bool = False, auth_user=None, auth_token=None):
        """
        Adds phenotype data from a file to an existing study.
        :param file - The file in question
        :param study_id - Study identifier
        :param filepath - Path to save the file in (should correspond to sourcefolder + study_id)
        :param transpose - Indicates whether the file is to be transposed.
        :param auth_user - The user performing the request
        :param auth_token - Optional authorization token instead of auth_user
        """
        if not auth_user and not auth_token:
            return ResultObject(msg="You must be logged in or provide an authorization token!", status=401)
        if not self.study_check_exists(study_id):
            return ResultObject(msg=f"The study you want to add data to ({study_id}) does not exist!", status=404)
        auth_check_user = self.user_check_authorization(auth_user, study_id, required_permission="contributor")
        auth_check_token = self.token_check_authorization(auth_token, study_id, required_permission="contributor")
        if not auth_check_user and not auth_check_token:
            return ResultObject(msg=f"You lack the necessary permissions to add data to study {study_id}!", status=401)
        pheno_file_data = get_phenotypes_from_file(file, filepath, transpose)
        if pheno_file_data.status != 200:
            return pheno_file_data
        phenotype_levels = None
        phenotypes = pheno_file_data.getBody()
        if phenotypes:
            phenotype_data, phenotype_levels, phenotype_metadata = phenotypes
            self.phenotype_create(study_id, phenotype_data, phenotype_levels, phenotype_metadata)
            if phenotype_levels:
                self.study_update(study_id, {"study_phenotypes": list(phenotype_levels.keys())}, auth_user=auth_user, auth_token=auth_token)
        return ResultObject(msg="Successfully added phenotype data to the study!")





