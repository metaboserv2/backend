import json
import secrets
import yaml
import pandas as pd
import numpy as np
import logging
import re, copy
import os
import xmltodict as xd
import magic
from collections import OrderedDict, defaultdict
from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename

AMINOACIDS = {
    'ala': 'alanine',
    'arg': 'arginine',
    'asn': 'asparagine',
    'asp': 'aspartic acid',
    'cys': 'cysteine',
    'gln': 'glutamine',
    'glu': 'glutamic acid',
    'gly': 'glycine',
    'his': 'histidine',
    'ile': 'isoleucine',
    'leu': 'leucine',
    'lys': 'lysine',
    'met': 'methionine',
    'phe': 'phenylalanine',
    'pro': 'proline',
    'ser': 'serine',
    'thr': 'threonine',
    'trp': 'tryptophan',
    'tyr': 'tyrosine',
    'val': 'valine'
}

internal_regex = re.compile('[^a-zA-Z0-9]|_')

class ResultObject(object):

    msg: str # Message describing the body (if there is any) or the process
    body: object # Can be anything, but should be convertable to JSON
    status: int # HTTP Status code to further describe the result (e.g. 200)

    def __init__(self, msg="Success", body=None, status=200):
        """
        Creates a new ResultObject. This happens whenever a function returns some output.
        """
        setattr(self, 'msg', msg)
        setattr(self, 'body', body)
        setattr(self, 'status', status)

    def __getitem__(self, item: str):
        return getattr(self, str(item))
    
    def __str__(self):
        return str(getattr(self, 'status')) + " - " + getattr(self, 'msg') + " --- " + str(getattr(self, 'body'))

    def getBody(self):
        return getattr(self, 'body')

def read_config(yml_path: str) -> dict:
    """
    Reads a yml config file. For details refer to the readme.

    :param yml_path: The path to the yml file (default is metaboserv_conf.yml)
    :returns A dictionary containing configuration details
    """
    with open(yml_path, 'r') as stream:
        config_data = yaml.safe_load(stream)
        return config_data
    
def combine_dicts(d1, d2):
    return {
        k: [(d[k] if not isinstance(d[k], list) else d[k][0]) for d in (d1, d2) if k in d]
        for k in set(d1.keys()) | set(d2.keys())
}

def dictify_vis_ranges(vis_ranges: list) -> dict:
    vr = {}
    for vr_object in vis_ranges:
        vr[vr_object['compound_id']] = vr_object
    return vr
    
def set_delimiter(del_param: str) -> str:
    """
    Returns a file delimiter char based on a string passed through a URL.

    :param del_param: The string in question
    :returns According delimiter for the del_param string
    """
    if not del_param or del_param == 'tab':
        return '\t'
    elif del_param == 'comma':
        return ','
    elif del_param == 'semicolon':
        return ';'
    elif del_param == 'dollar':
        return '$'
    return '\t'

def expand_aminoacid(short_aa: str) -> str:
    """
    Returns the longer form of an abbreviated aminoacid.
    :param short_aa - Amino-acid triplet
    """
    if not short_aa.lower() in AMINOACIDS.keys():
        return short_aa
    return AMINOACIDS[short_aa.lower()]

def adapt_compound_for_mariadb(compound: str) -> str:
    """
    Adapts a compound into the format required for MariaDB (only alphanumeric symbols + underscores are allowed).
    :param compound - The compound in question
    """
    return ('_'+re.sub(internal_regex, "_", compound.strip().lower())).strip()

    
def csv_to_dict(filename: str, headers:int, transpose=False) -> dict:
    """
    Reads a .csv or .tsv file containing concentration data and returns it as a dictionary.

    :param filename: Path to the file (by default ./uploads/file)
    :param headers: Marks the header line for pandas
    :param transpose: Dictates whether to transpose the pandas dataframe or not
    :returns Dictionary containing data from a concentration file
    """
    try:
        with open(filename, 'r') as concentration_file:
            raw_processed_file = pd.read_csv(
                concentration_file, header=headers, skip_blank_lines=True, sep=";|\t", engine='python')
            if transpose:
                raw_processed_file = raw_processed_file.transpose()
            raw_processed_file = raw_processed_file.replace(
                [np.inf, -np.inf], -1.0)
        return raw_processed_file.to_dict('index')
    except Exception as e:
        logging.error(f"Encountered an error while trying to convert a CSV file to dict: {e}")

def xlsx_to_dict(filename: str, headers: int, transpose=False) -> dict:
    """
    Reads a .xlsx file containing concentration data and returns it as a dictionary.

    :param filename: Path to the file (by default ./uploads/file)
    :param headers: Marks the header line for pandas
    :param transpose: Dictates whether to transpose the pandas dataframe or not
    :returns Dictionary containing data from a concentration file
    """
    # Has to be opened in binary mode (rb)
    try:
        with open(filename, 'rb') as concentration_file:
            raw_processed_file = pd.read_excel(concentration_file, index_col=0, usecols=lambda x: x != 0, header=headers)
            if transpose:
                raw_processed_file = raw_processed_file.transpose()
            raw_processed_file = raw_processed_file.replace(
                [np.inf, -np.inf], -1.0)
        return raw_processed_file.to_dict('index')
    except Exception as e:
        logging.error(f"Encountered an error while trying to convert an XLSX file to dict: {e}")

################### Conc/pheno/metadata additions #################

def get_conc_data_from_file(concentration_file, filepath: str, transpose: bool = False, enforce_hmdb: bool = False) -> ResultObject:
    """
    Reads in a concentration file and returns the according dicts.
    """
    if concentration_file:
        try:
            if isinstance(concentration_file, FileStorage):
                if not concentration_file.filename or concentration_file.filename == '':
                    return ResultObject(msg="Received an empty file!", status=400)
                filename = secure_filename(concentration_file.filename)
                concentration_file.save(os.path.join(filepath, filename))
                
            else:
                filename = concentration_file
            filetype_check = validate_file_type(os.path.join(filepath, filename))
            if filetype_check.status not in [200, 201]:
                return ResultObject(msg="File type validation failed!", status=400)
            conc_file_data = parse_concentration_file(os.path.join(filepath, filename), filetype_check.body, transpose, enforce_hmdb=enforce_hmdb)
            return conc_file_data
        except Exception as e:
            logging.error("Error occurred during concentration file handling!")
            logging.error(e)
            return ResultObject(msg=f"Error occurred during concentration file handling! {e}", status=500)
        
def get_metadata_from_file(metadata_file: FileStorage, filepath: str) -> ResultObject:
    """
    Reads in a metadata file and returns the according dicts.
    """
    if metadata_file:
        try:
            if isinstance(metadata_file, FileStorage):
                if not metadata_file.filename or metadata_file.filename == '':
                    return ResultObject(msg="Received an empty file!", status=400)
                filename = secure_filename(metadata_file.filename)
                filepath = os.path.join(filepath, filename)
                metadata_file.save(filepath)
            else:
                filepath = os.path.join(filepath, metadata_file)
            filetype_check = validate_file_type(filepath)
            if not filetype_check.status in [200, 201]:
                return ResultObject(msg="File type validation failed!", status=400)
            return parse_metadata_file(filepath, filetype_check.body)
        except Exception as e:
            logging.error("Error occurred during metadata file handling!")
            logging.error(e)
            return ResultObject(msg=f"Error occurred during metadata file handling! {e}", status=500)
    
def get_phenotypes_from_file(phenotype_file: FileStorage, filepath: str, transpose: bool = False) -> ResultObject:
    """
    Reads in a phenotype file and returns the according dicts.
    """
    if phenotype_file:
        print("Got following transpose value: " + str(transpose))
        try:
            if isinstance(phenotype_file, FileStorage):
                if not phenotype_file.filename or phenotype_file.filename == '':
                    return ResultObject(msg="Received an empty file!", status=400)
                filename = secure_filename(phenotype_file.filename)
                filepath = os.path.join(filepath, filename)
                phenotype_file.save(filepath)
            else:
                filepath = os.path.join(filepath, phenotype_file)
            filetype_check = validate_file_type(filepath)
            if filetype_check.status not in [200, 201]:
                return ResultObject(msg="File type validation failed!", status=400)
            return parse_phenotype_file(filepath, filetype_check.body, transpose)
        except Exception as e:
            logging.error("Error occurred during phenotype file handling!")
            logging.error(e)
            return ResultObject(msg=f"Error occurred during phenotype file handling! {e}", status=500)


################### File Parsing ###################
        


def parse_concentration_file(filepath, filetype, transpose=True, unit_parsing="column", enforce_hmdb=False) -> ResultObject:
    """
    Parses a concentration file and returns it as a dictionary if successful.
    Also returns a list of compounds.
    This function is completely study agnostic, that is handled by other functions in the database.
    :param filepath - Complete path to the file (generally upload folder + filename)
    :param filetype - Type of the file to parse
    :param transpose - Dictates whether or not to transpose the resulting dictionary
    :param unit_parsing - Dictates whether units are supplied in square brackets after metabolite names ("brackets")
        or whether an extra column called unit is passed ("column") which is the default
    :param enforce_hmdb - Dictates whether HMDB identifiers are enforced (otherwise, arbitrary metabolite names can be used)
    """
    try:
        if not os.path.isfile(filepath): return ResultObject(msg="This concentration data file does not exist!", status=404)
        if filetype == 'xlsx' or filetype =='xls':
            processed_file = xlsx_to_dict(filepath, 0, transpose)
        elif filetype == 'tsv':
            processed_file = csv_to_dict(filepath, 0, transpose)
        elif filetype == 'csv':
            processed_file = csv_to_dict(filepath, 0,  False)
        else: return ResultObject(msg="This filetype is not supported!", status=400)
        processed_file_adapted = {}
        concentration_metadata = {}
        hmdb_regex = re.compile("HMDB[0-9]{7}")
        unit_map = {}
        compound_name_map = {}
        internal_regex = re.compile('[^a-zA-Z0-9]|_')
        for source_key in processed_file.keys():
            insert_id = source_key
            if 'id' in processed_file[source_key].keys():
                insert_id = processed_file[source_key]['id']
            if 'source_id' in processed_file[source_key].keys():
                insert_id = processed_file[source_key]['source_id']
            if (insert_id.lower() == 'unit' or insert_id.lower() == 'units') and unit_parsing == 'column':
                # Unit row/column
                for key, value in processed_file[source_key].items():
                    if str(key).lower() not in ['id', 'source_id'] and not str(key).startswith('METADATA_'):
                        if re.match(hmdb_regex, str(key)):
                            unit_map[str(key)] = value
                        else:
                            if enforce_hmdb:
                                return ResultObject(msg="At least one identifier does not match the HMDB accession format, which is enforced here!", status=400)
                            else:
                                compound_internal = '_' + re.sub(internal_regex, "_", expand_aminoacid(str(key).lower()).strip().lower())
                                compound_internal = compound_internal.replace(",","_")
                                unit_map[compound_internal] = value
                continue
            processed_file_adapted[insert_id] = {}
            concentration_metadata[insert_id] = {}
            for key, value in processed_file[source_key].items():
                if str(key).lower() not in ['id', 'source_id'] and not str(key).startswith('METADATA_') and not str(key).lower() in ['unit', 'units']:
                    metabolite_raw = key
                    metabolite_cleaned = ''
                    if unit_parsing == "brackets":
                        metabolite_split = metabolite_raw.split(" [")
                        unit = 'mmol/L'
                        if len(metabolite_split) == 1:
                            # No extra unit found
                            metabolite_cleaned = metabolite_raw
                        elif len(metabolite_split) == 2:
                            # Unit found
                            metabolite_cleaned = metabolite_split[0]
                            unit = metabolite_split[1].replace("]", "")
                        else:
                            return ResultObject(msg=f"Found invalid format while parsing. Aborting", status=500)
                        if re.match(hmdb_regex, metabolite_cleaned):
                            unit_map[metabolite_cleaned] = unit
                        else:
                            if enforce_hmdb:
                                return ResultObject(msg="At least one identifier does not match the HMDB accession format, which is enforced here!", status=400)
                            else:
                                compound_internal = '_' + re.sub(internal_regex, "_", expand_aminoacid(str(metabolite_cleaned).lower()).strip().lower())
                                compound_internal = compound_internal.replace(",","_")
                                if compound_internal not in unit_map.keys():
                                    unit_map[compound_internal] = unit
                    else:
                        metabolite_cleaned = metabolite_raw

                    vti = str(value).replace('nan', 'NULL').replace('< LOD', 'NULL').replace("NA", 'NULL')
                    if vti != "NULL" and '.' in vti:
                        vti = str(round(float(vti), 3))
                    if re.match(hmdb_regex, metabolite_cleaned):
                        processed_file_adapted[insert_id][metabolite_cleaned] = str(value).replace('nan', 'NULL').replace('< LOD', 'NULL').replace("NA", 'NULL')
                    else:
                        compound_internal = '_' + re.sub(internal_regex, "_", expand_aminoacid(str(metabolite_cleaned).lower()).strip().lower())
                        compound_internal = compound_internal.replace(",","_")
                        compound_name_map[compound_internal] = metabolite_cleaned
                        processed_file_adapted[insert_id][compound_internal] = str(value).replace('nan', 'NULL').replace('< LOD', 'NULL').replace("NA", 'NULL')
                elif str(key).startswith('METADATA_'):
                    concentration_metadata[insert_id][str(key).replace("METADATA_","").lower().replace("-","_")] = str(value)
        compounds = processed_file_adapted[list(processed_file_adapted.keys())[0]].keys()
        return ResultObject(body=[processed_file_adapted, compounds, concentration_metadata, compound_name_map, unit_map])
    except Exception as e:
        logging.error(f"Encountered an error while parsing a file: {e}")
        return ResultObject(msg=f"Encountered an error while parsing a file: {e}", status=500)
    
def adapt_conc_file(filepath, filetype, adapted_metabolites: dict, save_folder: str, transpose=False) -> ResultObject:
    try:
        if not os.path.isfile(filepath): return ResultObject(msg="This conc. data file does not exist!", status=404)
        if filetype == 'xlsx' or filetype =='xls':
            processed_file = xlsx_to_dict(filepath, 0, transpose)
        elif filetype == 'tsv':
            processed_file = csv_to_dict(filepath, 0, transpose)
        elif filetype == 'csv':
            processed_file = csv_to_dict(filepath, 0,  False)
        else: return ResultObject(msg="This filetype is not supported!", status=400)
        df = pd.DataFrame.from_dict(processed_file, orient='index')
        cols = list(df.columns)
        for i, col in enumerate(df.columns):
            if col not in adapted_metabolites.keys(): continue
            cols[i] = adapted_metabolites[col]['res']
        df.columns = cols
        temp_filename = secrets.token_hex(16)
        if filetype == 'xlsx':
            temp_filename += '.xlsx'
            df.to_excel(os.path.join(save_folder, temp_filename))
        elif filetype == 'xls':
            temp_filename += '.xls'
            df.to_excel(os.path.join(save_folder, temp_filename))
        elif filetype == 'csv':
            temp_filename += '.csv'
            df.to_csv(os.path.join(save_folder, temp_filename), index=False, sep=";")
        else:
            temp_filename += '.tsv'
            df.to_csv(os.path.join(save_folder, temp_filename), index=False, sep="\t")
        return ResultObject(body=temp_filename)
    except Exception as e:
        logging.error(f"Encountered an error while parsing a file: {e}")
        return ResultObject(msg=f"Encountered an error while parsing a file: {e}", status=500)
    
def validate_file_type(filepath) -> ResultObject:
    """
    Validates the filetype indicated by a file extension using pymagic.
    :param filepath - Complete path to the file
    """
    try:
        extension = list(os.path.splitext(filepath))[-1].replace(".","")
        magic_numbers = magic.from_buffer(open(filepath, "rb").read(2048), mime=True)
        if extension in ['csv', 'tsv'] and magic_numbers not in ['text/plain', 'text/csv']:
            return ResultObject(msg=f"File Type mismatch - csv/tsv file was detected as {magic_numbers}", status=400)
        if extension in ['xlsx', 'xls'] and magic_numbers not in [
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-excel',
            'application/xls',
            'application/CDFV2'
        ]:
            return ResultObject(msg=f"File Type mismatch - xls(x) file was detected as {magic_numbers}", status=400)
        if extension == 'pdf' and magic_numbers not in ['application/pdf']:
            return ResultObject(msg=f"File Type mismatch - PDF file was detected as {magic_numbers}", status=400)
        if extension in ['7z', 'gz', 'tar', 'tar.gz', 'bz', 'bz2', 'zip', 'rar', 'lz'] and magic_numbers not in [
            'application/x-7z-compressed',
            'application/gzip',
            'application/x-bzip2',
            'application/x-tar',
            'application/x-lzip',
            'application/x-rar-compressed',
            'application/zip',
            'application/octet-stream',
        ]:
            return ResultObject(msg=f"File Type mismatch - archive file was detected as {magic_numbers}", status=400)
        return ResultObject(body=extension, msg="File validation successful", status=200)
    except Exception as e:
        logging.error(f"Encountered an error while validating filetype: {e}")
        return ResultObject(msg=f"Encountered an error while validating filetype: {e}", status=500)
    
def parse_metadata_file(filepath, filetype) -> ResultObject:
    """
    Parses a file containing metadata on a study.
    Can handle YML and JSON files.
    :param filepath - Complete path to the file
    :param filetype - Detected type of the file
    """
    try:
        if not os.path.isfile(filepath): return ResultObject(msg="This metadata file does not exist!", status=404)
        meta_dict = {}
        if filetype == 'yml' or filetype == 'yaml':
            with open(filepath, 'r') as stream:
                meta_dict = yaml.safe_load(stream)
        elif filetype == 'tsv':
            with open(filepath, 'r') as stream:
                meta_dict = json.load(stream)
        else: return ResultObject(msg="This filetype is not supported!", status=400)
        return ResultObject(body=meta_dict)
    except Exception as e:
        logging.error(f"Encountered an error while parsing metadata file: {e}")
        return ResultObject(msg=f"Encountered an error while parsing metadata file: {e}", status=500)
    
def parse_phenotype_file(filepath, filetype, transpose=False) -> ResultObject:
    """
    Parses a file containing phenotype information for a study.
    Columns that are not 'id', 'source_id' or are prefixed with 'PHENOTYPE_' are treated as metadata.
    :param filepath - Complete path to the file
    :param filetype - Detected type of the file
    :param transpose - Whether or not to transpose ("flip") the finished data
    """
    try:
        print("Parsing pheno file with the following setting: transpose = " + str(transpose))
        if not os.path.isfile(filepath): return ResultObject(msg="This phenotype file does not exist!", status=404)
        if filetype == 'xlsx':
            processed_file = xlsx_to_dict(filepath, 0, transpose)
        elif filetype == 'tsv':
            processed_file = csv_to_dict(filepath, 0, transpose)
        elif filetype == 'csv':
            processed_file = csv_to_dict(filepath, 0, transpose)
        else: return ResultObject(msg="This filetype is not supported!", status=400)
        phenotypes = {}
        phenotype_metadata = {}
        levels = {}
        for phen_key in processed_file.keys():
            phen_object = processed_file[phen_key]
            if 'id' in phen_object.keys():
                source_id = phen_object['id']
            elif 'source_id' in phen_object.keys():
                source_id = phen_object['source_id']
            else:
                source_id = phen_key
            phenotypes[source_id] = {}
            phenotype_metadata[source_id] = {}
            for column in phen_object.keys():
                if column in ['id', 'source_id']: continue
                if column.startswith('PHENOTYPE_'):
                    pheno_id = column.replace('PHENOTYPE_','')
                    phenotypes[source_id][pheno_id] = phen_object[column]
                    if pheno_id not in levels.keys():
                        levels[pheno_id] = []
                    if phen_object[column] not in levels[pheno_id]:
                        levels[pheno_id].append(phen_object[column])
                else:
                    phenotype_metadata[source_id][column] = phen_object[column]
        print(len(phenotypes.keys()))
        print(len(levels.keys()))
        print(len(phenotype_metadata.keys()))
        return ResultObject(body=[phenotypes, levels, phenotype_metadata])
    except Exception as e:
        logging.error(f"Encountered an error while parsing a file: {e}")
        return ResultObject(msg=f"Encountered an error while parsing a file: {e}", status=500)
    

################### HMDB Parsing ###################

def hmdb_extract_file(file_folder:str, file_name: str) -> ResultObject:
    """
    Parses a HMDB Metabolite data file (.xml format) and extracts the normal concentrations.
    Can then be used to create an xxx_parsed file in the data folder (api-side).

    :param file_folder: Folder the xml file is in
    :param file_name: Name of the file (e.g. serum_metabolites.xml)
    :returns Dict with data from HMDB metabolite ontology
    """
    try:
        result_dict = defaultdict(dict)
        with open(os.path.join(file_folder, file_name), 'r',  encoding="utf8") as xml_file:
            current_buffer = ""
            while True:
                next_line = xml_file.readline()
                if not next_line:
                    break
                if next_line.strip().rstrip('\n') == "<metabolite>":
                    current_buffer=next_line
                elif next_line.strip().rstrip('\n') == "</metabolite>":
                    current_buffer = current_buffer + next_line  
                    try:
                        entry = xd.parse(current_buffer)['metabolite']
                    except xd.ExpatError:
                        logging.error("ExpatError while parsing HMDB file.\n")
                    if not entry:
                        continue

                    temp_dict = defaultdict(dict)
                    metabolite_name_unaltered = entry['name']
                    metabolite_name = entry['name'].strip().lower().replace(" ", "_").replace("-", "_").replace(",","_")

                    # Basic information
                    temp_dict['compound_id'] = entry['accession'] if entry['accession'] else 'unknown' # Use HMDB IDs now!
                    temp_dict['compound_name'] = metabolite_name_unaltered
                    temp_dict['compound_internal_id'] = '_' + metabolite_name
                    temp_dict['compound_formula'] = entry['chemical_formula'] if entry['chemical_formula'] else 'unknown'
                    temp_dict['compound_mol_weight'] = entry['average_molecular_weight'] if entry['average_molecular_weight'] else 'unknown'
                    temp_dict['compound_iupac'] = entry['iupac_name'] if entry['iupac_name'] else 'unknown'

                    # Synonyms
                    if entry['synonyms']:
                        temp_dict['compound_synonyms'] = entry['synonyms']['synonym']

                    # Taxonomy information
                    if entry['taxonomy']:
                        temp_dict['taxonomy'] = {}
                        temp_dict['taxonomy']['kingdom'] = entry['taxonomy']['kingdom']
                        temp_dict['taxonomy']['superclass'] = entry['taxonomy']['super_class']
                        if entry['taxonomy']['super_class'] != entry['taxonomy']['direct_parent']:
                            temp_dict['taxonomy']['group'] = entry['taxonomy']['direct_parent']
                    
                    # Normal concentrations
                    if entry['normal_concentrations'] and entry['normal_concentrations']['concentration']:

                        temp_dict['normal_concentrations'] = []

                        # If normal concentrations get parsed as (ordered) dict
                        # This generally happens if only one normal conc. exists
                        if type(entry['normal_concentrations']['concentration']) in [dict, OrderedDict]:
                            conc = entry['normal_concentrations']['concentration']
                            if conc and conc != {} and conc['concentration_value'] and conc['concentration_units']:
                                parsed_dict = hmdb_parse_concentration_entry(conc)
                                if parsed_dict:
                                    temp_dict['normal_concentrations'].append(parsed_dict)

                        # If normal concentrations get parsed as a list
                        # This generally happens if there is more than one normal conc.
                        elif type(entry['normal_concentrations']['concentration']) is list:
                            for conc_index in range(len(entry['normal_concentrations']['concentration'])):
                                try:
                                    conc = entry['normal_concentrations']['concentration'][conc_index]
                                    if not conc or conc == {}: continue
                                    if conc['concentration_value'] and conc['concentration_units']:
                                        parsed_dict = hmdb_parse_concentration_entry(conc)
                                        if parsed_dict:
                                            temp_dict['normal_concentrations'].append(parsed_dict)
                                except KeyError:
                                    logging.error(f"Failed trying to add concentration data for metabolite {metabolite_name_unaltered}.")

                    result_dict[temp_dict['compound_id']] = copy.deepcopy(temp_dict)
                    temp_dict = {}
                    current_buffer = ""
                else:
                    current_buffer = current_buffer + next_line
        return ResultObject(body=result_dict)
    except Exception as e:
        logging.error(f"Encountered an error during HMDB file parsing: {e}")
        return ResultObject(msg=e, status=500)

def hmdb_parse_concentration_entry(conc_object: dict):
        """
        Helper method that normalizes the messy normal concentration entries as provided by the HMDB.

        :param conc_object: HMDB normal concentration entry
        :returns Normalized object
        """
        try:
            if not conc_object: return None
            if 'concentration_value' in conc_object.keys() and conc_object['concentration_value']:
                parsed_min, parsed_max = hmdb_adapt_concentration_value(conc_object['concentration_value'])
            else:
                return None
            if parsed_min == -1 or parsed_max in [-1, 0]:
                return None
            if 'biospecimen' not in conc_object.keys():
                return None
            if conc_object['biospecimen'].lower() not in ['serum', 'blood', 'plasma', 'urine', 'feces', 'csf', 'other', 'multiple', 'unknown']:
                if conc_object['biospecimen'].lower() == 'cerebrospinal fluid (csf)':
                    conc_object['biospecimen'] = 'csf'
                else:
                    return None
            unraveled = {
                'min': round(parsed_min,2),
                'max': round(parsed_max,2),
                'age': hmdb_normalize_age(conc_object['subject_age'].lower() if ('subject_age' in conc_object.keys() and conc_object['subject_age']) else "not specified"),
                'condition': conc_object['subject_condition'].lower() if ('subject_condition' in conc_object.keys() and conc_object['subject_condition']) else "not specified",
                'unit': conc_object['concentration_units'].lower() if ('concentration_units' in conc_object.keys() and conc_object['concentration_units']) else "not specified",
                'sex': conc_object['subject_sex'].lower() if ('subject_sex' in conc_object.keys() and conc_object['subject_sex']) else "not specified",
                'biospecimen': conc_object['biospecimen'].lower()
            }
            reference = 'not specified'
            if 'references' in conc_object.keys() and conc_object['references'] != None and 'reference' in conc_object['references'].keys():
                if isinstance(conc_object['references']['reference'], dict):
                    if 'pubmed_id' in conc_object['references']['reference'].keys():
                        reference = conc_object['references']['reference']['pubmed_id']
                elif isinstance(conc_object['references']['reference'], list):
                    reference = ', '.join(filter(lambda x: x != None, [conc_object['references']['reference'][n]['pubmed_id']
                        if 'pubmed_id' in conc_object['references']['reference'][n].keys() else None for n in range(len(conc_object['references']['reference']))]))
            unraveled['ref'] = reference
            if unraveled['unit'].lower() == 'um':
                unraveled['unit'] = 'mmol/L'
                unraveled['min'] = unraveled['min']/1000
                unraveled['max'] = unraveled['max']/1000
            return unraveled
        except Exception as e:
            logging.error(conc_object)           
            logging.error(e)
            return None

def hmdb_normalize_age(value_string: str):
        """
        Normalizes HMDB age annotations into the following groups:
        Newborn, infant, children, adolescent, adult
        """
        if "newborn" in value_string: return "newborn"
        if "infant" in value_string: return "infant"
        if "child" in value_string: return "children"
        if "adolescent" in value_string: return "adolescents"
        if "adult" in value_string: return "adults"
        return "not specified"

def hmdb_adapt_concentration_value(value_string: str):
        """
        Parses a normal concentration value string as provided by HMDB (they can be quite "unclean")

        :param value_string: The string to parse
        :returns min and max values for a metabolite
        """
        def isfloat(num):
            """
            Checks if a string can be cast directly to a float.

            :param num: The string in question
            :returns True if floatable, False otherwise
            """
            try:
                float(num)
                return True
            except ValueError:
                return False

        try:
            if not value_string: return -1, -1
            value_string = value_string.replace(",",".")
            if value_string.startswith("-") or value_string.startswith("–"):
                value_string=value_string[1:len(value_string)]
            if "+/-" in value_string:
                values = value_string.split("+/-")
                return max(0, float(values[0].strip()) - float(values[1].strip())), float(values[0].strip()) + float(values[1].strip())
            if "<" in value_string:
                return 0.0, float(value_string.replace("<", "").replace("=",""))
            if ">" in value_string:
                return float(value_string.replace(">", "").replace("=","")), 999999.9
            if " (" in value_string:
                value_split = value_string.split(" (")
                values = re.split('-|–', value_split[1].replace(")", ""))
                return float(values[0]), float(values[1])
            if "(" in value_string and "-" not in value_string and "–" not in value_string:
                values = value_string.split("(")
                return float(values[0]) - float(values[1]), float(values[0]) + float(values[1])
            if "(" in value_string and ("-" in value_string or "–" in value_string):
                value_split = value_string.split("(")
                values = re.split('-|–', value_split[1].replace(")", ""))
                return float(values[0].strip()), float(values[1].strip())
            if "-" in value_string:
                values = value_string.split("-")
                return float(values[0].strip()), float(values[1].strip())
            if "–" in value_string:
                values = value_string.split("–")
                return float(values[0].strip()), float(values[1].strip())
            if isfloat(value_string):
                return float(value_string), float(value_string)
            return -1, -1
        except ValueError:
            return -1, -1
        except Exception as e:
            logging.error(e)
            logging.error(f"Encountered error during conc. value parsing: {e}")
            return -1, -1
